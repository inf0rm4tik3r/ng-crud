import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudFieldLabelComponent } from './crud-field-label.component';

describe('CrudFieldLabelComponent', () => {
  let component: CrudFieldLabelComponent;
  let fixture: ComponentFixture<CrudFieldLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudFieldLabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudFieldLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
