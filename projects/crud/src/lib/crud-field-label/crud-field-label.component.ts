import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, Input} from '@angular/core';
import {CrudComponent} from '../crud-component';
import {CrudBaseFieldComponent} from '../crud-fields/crud-base-field.component';
import {CrudLoggingService} from '../crud-logging.service';

@Component({
  selector: 'crud-field-label',
  templateUrl: './crud-field-label.component.html',
  styleUrls: ['./crud-field-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudFieldLabelComponent extends CrudComponent {

  @Input()
  for: CrudBaseFieldComponent;

  constructor(protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    cdr.detach();
  }

  @HostListener('click')
  click(): void {
    this.for.performFocus();
  }

  getComponentName(): string {
    return 'field-label';
  }

}
