import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudHistoryComponent } from './crud-history.component';

describe('CrudHistoryComponent', () => {
  let component: CrudHistoryComponent;
  let fixture: ComponentFixture<CrudHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
