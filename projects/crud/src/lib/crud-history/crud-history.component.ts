import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {CrudComponent} from '../crud-component';
import {CrudLoggingService} from '../crud-logging.service';
import {CrudRouterService} from '../crud-router.service';

@Component({
  selector: 'crud-history',
  templateUrl: './crud-history.component.html',
  styleUrls: ['./crud-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudHistoryComponent extends CrudComponent {

  constructor(private crudRouterService: CrudRouterService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  back(): void {
    this.crudRouterService.openListView();
  }

  getComponentName(): string {
    return 'history';
  }

}
