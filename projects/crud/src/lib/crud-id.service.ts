import {Injectable} from '@angular/core';
import * as uuid from 'uuid';

@Injectable()
export class CrudIdService {

  public uuid(): string {
    return uuid.v4();
  }

}
