import { TestBed } from '@angular/core/testing';

import { CrudInputService } from './crud-input.service';

describe('CrudInputService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudInputService = TestBed.get(CrudInputService);
    expect(service).toBeTruthy();
  });
});
