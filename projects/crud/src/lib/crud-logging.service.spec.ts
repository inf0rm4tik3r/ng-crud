import { TestBed } from '@angular/core/testing';

import { CrudLoggingService } from './crud-logging.service';

describe('CrudLoggingService', () => {
  let service: CrudLoggingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudLoggingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
