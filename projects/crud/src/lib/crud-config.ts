import {Type} from '@angular/core';
import {CrudDataProvider} from './crud-data-provider/crud-data-provider';
import {CrudCondition} from './crud-filter/crud-condition';
import {LogLevel} from './crud-logging.service';
import {
  Entry,
  EntryKey,
  FilterMode,
  NestedInstanceConfig,
  NestedInstances,
  OrderByAllowedSet,
  OrderByTypeSet,
  RelationConfig,
  Relations
} from './types/crud-types';

export class CrudConfig {

  private _logLevel: LogLevel = LogLevel.WARN;
  private _dataProvider: CrudDataProvider | undefined = undefined;
  private _fetchMode: 'lazy' | 'prefetch' = 'lazy';
  private _labels: { [fieldName: string]: string } = {};
  private _orderByAllowedFor: OrderByAllowedSet = {};
  private _orderByColumns: OrderByTypeSet = {};
  private _keyVisualizer: ((key: EntryKey | undefined) => string) = (() => 'missing key visualizer');
  private _entryVisualizer: ((entry: Entry | undefined) => string) = (() => 'missing entry visualizer');
  private _usePagination = true;
  private _entriesPerPage = 5;
  private _fieldTypes: { [fieldName: string]: string } = {};
  private _fieldOptions: { [fieldName: string]: unknown[] } = {};
  private _fieldRenderer: { [typeName: string]: Type<unknown> } = {};
  private _inlineEdit: { [typeName: string]: boolean } = {};
  private _filterMode: FilterMode = 'normal';
  private _allowFilterDefault: boolean = true;
  private _allowFilter: { [fieldName: string]: boolean } = {};
  private _preFilter: CrudCondition = new CrudCondition();
  private _userFilter: CrudCondition = new CrudCondition();
  private _relations: Relations = {};
  private _nestedInstances: NestedInstances = {};
  private _collapsible: boolean = true;

  // TODO: Get rid of this?
  mergeWith(other: CrudConfig): CrudConfig {
    // Copy the log level.
    this.logLevel(other.getLogLevel());

    // Copy the data provider.
    this.dataProvider(other.getDataProvider());

    // Copy the fetch mode.
    this.fetchMode(other.getFetchMode());

    // Copy labels.
    for (const fieldName of Object.keys(other._labels)) {
      this.label(fieldName, other._labels[fieldName]);
    }

    for (const columnName of Object.keys(other.getAllowedOrderByColumns())) {
      this._orderByAllowedFor[columnName] = other.getAllowedOrderByColumns()[columnName];
    }

    for (const fieldName of Object.keys(other.getOrderByColumns())) {
      this.orderBy(fieldName, other.getOrderByColumns()[fieldName]);
    }

    this.keyVisualizer(other.getKeyVisualizer());
    this.entryVisualizer(other.getEntryVisualizer());

    this.usePagination(other.shouldUsePagination());
    this.entriesPerPage(other.getEntriesPerPage());

    // Add custom field types.
    for (const fieldName of Object.keys(other._fieldTypes)) {
      this.fieldType(fieldName, other._fieldTypes[fieldName]);
    }

    // Add field options.
    for (const fieldName of Object.keys(other._fieldOptions)) {
      this.fieldOptions(fieldName, other._fieldOptions[fieldName]);
    }

    // Add custom field renderer.
    for (const type of Object.keys(other._fieldRenderer)) {
      this.fieldRenderer(type, other._fieldRenderer[type]);
    }

    // Add inline edit information.
    for (const fieldName of Object.keys(other._inlineEdit)) {
      this.inlineEdit(fieldName, other._inlineEdit[fieldName]);
    }

    // Add filter configuration.
    this.filterMode(other.getFilterMode());
    this.preFilter(other.getPreFilter());
    this.userFilter(other.getUserFilter());
    this.allowFilterDefault(other.getAllowedFilterDefault());
    for (const fieldName of Object.keys(other._allowFilter)) {
      this.allowFilter(fieldName, other.isFilterAllowedFor(fieldName));
    }

    // Add relations.
    for (const fieldName of Object.keys(other._relations)) {
      const rel = other._relations[fieldName];
      this.relation(
        rel.fieldName, rel.dataProvider, rel.targetTableConnectFieldName, rel.displayPattern,
        rel.filter, rel.orderBy
      );
    }

    // Add nested instances.
    for (const fieldName of Object.keys(other._nestedInstances)) {
      const nested = other._nestedInstances[fieldName];
      this.nestedInstance(
        nested.fieldName, nested.referencedFieldName, nested.referenceFieldName, nested.config
      );
    }

    // Copy collapsible.
    this.collapsible(other.isCollapsible());

    return this;
  }

  logLevel(value: LogLevel): CrudConfig {
    this._logLevel = value;
    return this;
  }

  getLogLevel(): LogLevel {
    return this._logLevel;
  }

  dataProvider(dataProvider: CrudDataProvider): CrudConfig {
    this._dataProvider = dataProvider;
    return this;
  }

  getDataProvider(): CrudDataProvider {
    return this._dataProvider;
  }

  fetchMode(value: 'lazy' | 'prefetch'): void {
    // TODO: Implement prefetch mode!
    if (value === 'prefetch') {
      throw new Error('Prefetch mode is currently not supported.');
    }
    this._fetchMode = value;
  }

  getFetchMode(): 'lazy' | 'prefetch' {
    return this._fetchMode;
  }

  label(fieldName: string, label: string): void {
    // TODO: Error checking: fieldName existsInDataProvider?
    this._labels[fieldName] = label;
  }

  /**
   * Returns the label for the given field name, should one exists.
   * If no label was specified, the given field name is returned.
   * @param fieldName The field name for which the custom label should be returned.
   */
  getLabeled(fieldName: string): string {
    const label = this._labels[fieldName];
    return label ? label : fieldName;
  }

  allowOrderBy(columns: string | string[], allow: boolean = true): void {
    [].concat(columns).forEach(column => {
      this._orderByAllowedFor[column] = allow;
    });
  }

  isOrderByAllowed(columnName: string): boolean {
    return !!this._orderByAllowedFor[columnName];
  }

  getAllowedOrderByColumns(): OrderByAllowedSet {
    return this._orderByAllowedFor;
  }

  orderBy(columns: string | string[], orderType: 'asc' | 'desc' = 'asc'): void {
    [].concat(columns).forEach(column => {
      this._orderByColumns[column] = orderType;
    });
  }

  getOrderByColumns(): OrderByTypeSet {
    return this._orderByColumns;
  }

  keyVisualizer(value: (key: EntryKey | undefined) => string): CrudConfig {
    this._keyVisualizer = value;
    return this;
  }

  getKeyVisualizer(): (key: EntryKey | undefined) => string {
    return this._keyVisualizer;
  }

  entryVisualizer(value: (entry: Entry | undefined) => string): CrudConfig {
    this._entryVisualizer = value;
    return this;
  }

  getEntryVisualizer(): (entry: Entry | undefined) => string {
    return this._entryVisualizer;
  }

  usePagination(value: boolean): void {
    this._usePagination = value;
  }

  shouldUsePagination(): boolean {
    return this._usePagination;
  }

  entriesPerPage(value: number): void {
    this._entriesPerPage = value;
  }

  getEntriesPerPage(): number {
    return this._entriesPerPage === Number.POSITIVE_INFINITY ? Number.MAX_SAFE_INTEGER : this._entriesPerPage;
  }

  fieldType(fieldName: string, type: string): void {
    this._fieldTypes[fieldName] = type;
  }

  /**
   * Answers of what type a certain field is.
   * @param fieldName The name of the field.
   * @param fieldType The currently known type of that field, extracted from the datasource.
   */
  getFieldType(fieldName: string, fieldType?: string): string {
    const customType: string | undefined = this._fieldTypes[fieldName];
    if (customType) {
      return customType;
    }
    if (fieldType) {
      return fieldType;
    }
    return 'unknown';
  }

  fieldOptions(fieldName: string, options: unknown[]): void {
    this._fieldOptions[fieldName] = options;
  }

  getFieldOptions(fieldName: string): unknown[] {
    return this._fieldOptions[fieldName];
  }

  fieldRenderer(type: string, component: Type<unknown>): void {
    this._fieldRenderer[type] = component;
  }

  getFieldRenderer(type: string): Type<unknown> {
    return this._fieldRenderer[type];
  }

  inlineEdit(fieldNames: string | string[], allow: boolean = true): void {
    [].concat(fieldNames).forEach(fieldName => {
      this._inlineEdit[fieldName] = allow;
    });
  }

  isInlineEditAllowed(fieldName: string): boolean {
    return this._inlineEdit[fieldName];
  }

  filterMode(value: FilterMode): void {
    this._filterMode = value;
  }

  getFilterMode(): FilterMode {
    return this._filterMode;
  }

  allowFilterDefault(defaultValue: boolean): CrudConfig {
    this._allowFilterDefault = defaultValue;
    return this;
  }

  getAllowedFilterDefault(): boolean {
    return this._allowFilterDefault;
  }

  allowFilter(fieldNames: string | string[], allowed: boolean | undefined): CrudConfig {
    const fields = [].concat(fieldNames);
    for (const fieldName of fields) {
      this._allowFilter[fieldName] = allowed;
    }
    return this;
  }

  isFilterAllowedFor(fieldName: string): boolean {
    const allowed = this._allowFilter[fieldName];
    if (typeof allowed !== typeof undefined) {
      return allowed;
    }
    return this._allowFilterDefault;
  }

  preFilter(value: CrudCondition): void {
    this._preFilter = value;
  }

  getPreFilter(): CrudCondition {
    return this._preFilter;
  }

  userFilter(condition: CrudCondition): void {
    this._userFilter = condition;
  }

  getUserFilter(): CrudCondition {
    return this._userFilter;
  }

  relation(fieldName: string,
           dataProvider: CrudDataProvider,
           targetTableConnectFieldName: string,
           displayPattern: string,
           filter: CrudCondition,
           orderBy: OrderByTypeSet): void {
    this._relations[fieldName] = new RelationConfig(
      fieldName, dataProvider, targetTableConnectFieldName, displayPattern, filter, orderBy
    );
  }

  getRelations(): Relations {
    return this._relations;
  }

  getRelation(fieldName: string): RelationConfig {
    const relationConfig = this._relations[fieldName];
    if (relationConfig) {
      return relationConfig;
    }
    throw new Error('No relation config present for field "' + fieldName + '".');
  }

  // TODO: What if our key is more complex than just 'id'?
  nestedInstance(newFieldName: string,
                 referencedFieldName: string,
                 referenceFieldName: string,
                 config: CrudConfig): void {
    this._nestedInstances[newFieldName] = new NestedInstanceConfig(
      newFieldName, referencedFieldName, referenceFieldName, config
    );
  }

  getNestedInstances(): NestedInstances {
    return this._nestedInstances;
  }

  getNestedInstance(fieldName: string): NestedInstanceConfig {
    const nestedInstanceConfig = this._nestedInstances[fieldName];
    if (nestedInstanceConfig) {
      return nestedInstanceConfig;
    }
    throw new Error('No nested instance config present for field "' + fieldName + '".');
  }

  collapsible(collapsible: boolean): CrudConfig {
    this._collapsible = collapsible;
    return this;
  }

  isCollapsible(): boolean {
    return this._collapsible;
  }

}
