import {Injectable} from '@angular/core';

export enum LogLevel {
  TRACE = 1,
  DEBUG = 2,
  INFO = 3,
  WARN = 4,
  ERROR = 5,
  FATAL = 5,
}

const LOG_LEVEL_NAMES = {
  1: 'TRACE',
  2: 'DEBUG',
  3: 'INFO ',
  4: 'WARN ',
  5: 'ERROR'
};

const DO_NOTHING: () => void = () => {
  // Do nothing.
};

@Injectable()
export class CrudLoggingService {

  private name: string = 'CRUD';

  private logLevel: LogLevel | null = LogLevel.TRACE;

  public trace: (...args: unknown[]) => void = DO_NOTHING;
  public debug: (...args: unknown[]) => void = DO_NOTHING;
  public info: (...args: unknown[]) => void = DO_NOTHING;
  public warn: (...args: unknown[]) => void = DO_NOTHING;
  public error: (...args: unknown[]) => void = DO_NOTHING;
  public fatal: (...args: unknown[]) => void = DO_NOTHING;

  setName(name: string): CrudLoggingService {
    this.name = name;
    return this.init();
  }

  setLogLevel(logLevel: LogLevel | null): CrudLoggingService {
    this.logLevel = logLevel;
    return this.init();
  }

  private prefix(logLevel: LogLevel): string {
    return this.name + ': [' + LOG_LEVEL_NAMES[logLevel] + '] -';
  }

  init(name?: string, logLevel?: LogLevel): CrudLoggingService {
    if (name) {
      this.name = name;
    }
    if (logLevel) {
      this.logLevel = logLevel;
    }

    this.trace = (() => {
      if (LogLevel.TRACE >= this.logLevel) {
        return Function.prototype.bind.call(console.log, console, this.prefix(LogLevel.TRACE));
      } else {
        return DO_NOTHING;
      }
    })();

    this.debug = (() => {
      if (LogLevel.DEBUG >= this.logLevel) {
        return Function.prototype.bind.call(console.log, console, this.prefix(LogLevel.DEBUG));
      } else {
        return DO_NOTHING;
      }
    })();

    this.info = (() => {
      return LogLevel.INFO >= this.logLevel
        ? Function.prototype.bind.call(console.log, console, this.prefix(LogLevel.INFO))
        : DO_NOTHING;
    })();

    this.warn = (() => {
      if (LogLevel.WARN >= this.logLevel) {
        return Function.prototype.bind.call(console.warn, console, this.prefix(LogLevel.WARN));
      } else {
        return DO_NOTHING;
      }
    })();

    this.error = (() => {
      if (LogLevel.ERROR >= this.logLevel) {
        return Function.prototype.bind.call(console.error, console, this.prefix(LogLevel.ERROR));
      } else {
        return DO_NOTHING;
      }
    })();

    this.fatal = (() => {
      if (LogLevel.FATAL >= this.logLevel) {
        return Function.prototype.bind.call(console.error, console, this.prefix(LogLevel.FATAL));
      } else {
        return DO_NOTHING;
      }
    })();

    return this;
  }

}
