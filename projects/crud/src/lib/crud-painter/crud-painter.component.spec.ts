import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudPainterComponent} from './crud-painter.component';

describe('CrudPainterComponent', () => {
  let component: CrudPainterComponent;
  let fixture: ComponentFixture<CrudPainterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudPainterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudPainterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
