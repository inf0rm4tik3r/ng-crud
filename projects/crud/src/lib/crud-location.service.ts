import {Injectable, OnDestroy} from '@angular/core';

@Injectable()
export class CrudLocationService implements OnDestroy {

  private static onHashChange(event: HashChangeEvent): void {
    console.log('hash has changed');
    console.log(event);
  }

  constructor() {
    window.addEventListener('hashchange', CrudLocationService.onHashChange);
  }

  ngOnDestroy(): void {
    window.removeEventListener('hashchange', CrudLocationService.onHashChange);
  }

  setHash(hash: string): void {
    window.location.hash = hash;
  }

}
