import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'crud-modal-backdrop',
  templateUrl: './crud-modal-backdrop.component.html',
  styleUrls: ['./crud-modal-backdrop.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudModalBackdropComponent {
}
