import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudModalBackdropComponent} from './crud-modal-backdrop.component';

describe('CrudModalBackdropComponent', () => {
  let component: CrudModalBackdropComponent;
  let fixture: ComponentFixture<CrudModalBackdropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudModalBackdropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudModalBackdropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
