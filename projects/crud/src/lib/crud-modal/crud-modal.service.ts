import {Injectable, Type} from '@angular/core';
import {CrudInjectionService} from '../crud-injection.service';
import {CrudInstanceService} from '../crud-instance/crud-instance-service';
import {CrudModalComponent} from './crud-modal.component';

@Injectable()
export class CrudModalService {

  constructor(private crudInstanceService: CrudInstanceService,
              private crudInjectionService: CrudInjectionService) {
  }

  private modalContainer: CrudModalComponent = undefined;

  setModalContainerForInjection(modalContainer: CrudModalComponent): void {
    this.modalContainer = modalContainer;
  }

  init(component: Type<unknown>, inputs: { [key: string]: unknown }, outputs: { [key: string]: unknown }): void {
    this.crudInjectionService.appendComponentToViewContainer(
      this.modalContainer.container.viewContainerRef,
      component,
      {inputs, outputs}
    );
    this.modalContainer.show();
  }

  destroy(): void {
    this.modalContainer.container.viewContainerRef.remove(
      this.modalContainer.container.viewContainerRef.length - 1
    );
    if (this.modalContainer.container.viewContainerRef.length === 0) {
      this.modalContainer.hide();
    }
  }

  hasModals(): boolean {
    return this.modalContainer.container.viewContainerRef.length !== 0;
  }

}
