import {TestBed} from '@angular/core/testing';

import {CrudModalService} from './crud-modal.service';

describe('CrudModalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudModalService = TestBed.get(CrudModalService);
    expect(service).toBeTruthy();
  });
});
