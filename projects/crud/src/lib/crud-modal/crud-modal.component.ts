import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostBinding, OnInit, ViewChild} from '@angular/core';
import {CrudComponent} from '../crud-component';
import {CrudContainerDirective} from '../crud-container.directive';
import {CrudInstanceService} from '../crud-instance/crud-instance-service';
import {CrudLoggingService} from '../crud-logging.service';

@Component({
  selector: 'crud-modal',
  templateUrl: './crud-modal.component.html',
  styleUrls: ['./crud-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudModalComponent extends CrudComponent implements OnInit {

  @ViewChild(CrudContainerDirective)
  container: CrudContainerDirective;

  @HostBinding('class.show')
  private _show: boolean = false;

  constructor(private crudInstanceService: CrudInstanceService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    this.cdr.detach();
  }

  ngOnInit(): void {
    this.cdr.detectChanges();
  }

  show(): void {
    this._show = true;
    this.cdr.detectChanges();
    this.crudInstanceService.instance.detectChanges();
  }

  hide(): void {
    this._show = false;
    this.cdr.detectChanges();
    this.crudInstanceService.instance.detectChanges();
  }

  getComponentName(): string {
    return 'crud-modal';
  }

}
