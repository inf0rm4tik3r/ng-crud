import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {LinkedList} from '../types/linked-list';
import {CrudToast} from './crud-toast';

@Injectable({
  providedIn: 'root'
})
export class CrudToastService {

  private readonly _toasts: BehaviorSubject<LinkedList<CrudToast>>
    = new BehaviorSubject<LinkedList<CrudToast>>(new LinkedList<CrudToast>());
  public readonly $toasts: Observable<LinkedList<CrudToast>>
    = this._toasts.asObservable();

  add(toast: CrudToast): void {
    const elem = this._toasts.getValue().append(toast);
    this._toasts.next(this._toasts.getValue());
    if (toast.automaticallyClosing) {
      setTimeout(() => {
        this._toasts.getValue().removeElement(elem);
        this._toasts.next(this._toasts.getValue());
      }, toast.automaticCloseDelay);
    }
  }

}
