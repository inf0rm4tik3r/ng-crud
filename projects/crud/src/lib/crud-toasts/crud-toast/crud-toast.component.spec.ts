import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudToastComponent } from './crud-toast.component';

describe('CrudToastComponent', () => {
  let component: CrudToastComponent;
  let fixture: ComponentFixture<CrudToastComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudToastComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudToastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
