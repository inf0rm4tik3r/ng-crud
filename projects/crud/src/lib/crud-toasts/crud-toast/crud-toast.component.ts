import {Component, Input} from '@angular/core';
import {CrudToast} from '../crud-toast';

@Component({
  selector: 'crud-toast',
  templateUrl: './crud-toast.component.html',
  styleUrls: ['./crud-toast.component.scss']
})
export class CrudToastComponent {

  @Input()
  toast: CrudToast;

}
