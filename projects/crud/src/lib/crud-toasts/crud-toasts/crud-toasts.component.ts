import {ChangeDetectionStrategy, Component, HostBinding, Input} from '@angular/core';
import {CrudToastService} from '../crud-toast.service';

@Component({
  selector: 'crud-toasts',
  templateUrl: './crud-toasts.component.html',
  styleUrls: ['./crud-toasts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudToastsComponent {

  @Input()
  verticalPosition: 'top' | 'bottom' = 'bottom';

  @Input()
  horizontalPosition: 'left' | 'right' = 'right';

  constructor(public crudToastService: CrudToastService) {
  }

  @HostBinding('style.bottom')
  get attrBottom(): string | number {
    return this.verticalPosition === 'bottom' ? 0 : 'auto';
  }

  @HostBinding('style.top')
  get attrTop(): string | number {
    return this.verticalPosition === 'top' ? 0 : 'auto';
  }

  @HostBinding('style.left')
  get attrLeft(): string | number {
    return this.horizontalPosition === 'left' ? 0 : 'auto';
  }

  @HostBinding('style.right')
  get attrRight(): string | number {
    return this.horizontalPosition === 'right' ? 0 : 'auto';
  }

}
