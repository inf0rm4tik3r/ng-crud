import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudToastsComponent } from './crud-toasts.component';

describe('CrudToastsComponent', () => {
  let component: CrudToastsComponent;
  let fixture: ComponentFixture<CrudToastsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudToastsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudToastsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
