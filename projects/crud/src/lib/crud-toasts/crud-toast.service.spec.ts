import { TestBed } from '@angular/core/testing';

import { CrudToastService } from './crud-toast.service';

describe('CrudToastService', () => {
  let service: CrudToastService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudToastService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
