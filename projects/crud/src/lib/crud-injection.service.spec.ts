import { TestBed } from '@angular/core/testing';

import { CrudInjectionService } from './crud-injection.service';

describe('CrudInjectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudInjectionService = TestBed.get(CrudInjectionService);
    expect(service).toBeTruthy();
  });
});
