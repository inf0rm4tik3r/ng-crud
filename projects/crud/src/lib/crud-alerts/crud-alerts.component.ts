import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {CrudAlertService} from '../crud-alert.service';
import {CrudAlert} from '../crud-alert/crud-alert';
import {CrudComponent} from '../crud-component';
import {CrudLoggingService} from '../crud-logging.service';

@Component({
  selector: 'crud-alerts',
  templateUrl: './crud-alerts.component.html',
  styleUrls: ['./crud-alerts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudAlertsComponent extends CrudComponent implements OnInit {

  alerts: CrudAlert[];

  constructor(public crudAlertService: CrudAlertService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    cdr.detach();
  }

  ngOnInit(): void {
    this.crudAlertService.alerts$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((alerts: CrudAlert[]) => {
        this.alerts = alerts;
        this.cdr.detectChanges();
      });
  }

  getComponentName(): string {
    return 'alerts';
  }

}
