import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudAlertsComponent} from './crud-alerts.component';

describe('CrudAlertsComponent', () => {
  let component: CrudAlertsComponent;
  let fixture: ComponentFixture<CrudAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
