import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'crud-loading-indicator',
  templateUrl: './crud-loading-indicator.component.html',
  styleUrls: ['./crud-loading-indicator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudLoadingIndicatorComponent {

  @Input()
  fontSize = '1.25em';

}
