import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudLoadingIndicatorComponent } from './crud-loading-indicator.component';

describe('LoadingIndicatorComponent', () => {
  let component: CrudLoadingIndicatorComponent;
  let fixture: ComponentFixture<CrudLoadingIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudLoadingIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudLoadingIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
