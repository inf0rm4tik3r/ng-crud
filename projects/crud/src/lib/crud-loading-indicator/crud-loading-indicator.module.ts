import {NgModule} from '@angular/core';
import {CrudLoadingIndicatorComponent} from './crud-loading-indicator.component';

@NgModule({
  imports: [],
  declarations: [
    CrudLoadingIndicatorComponent
  ],
  exports: [
    CrudLoadingIndicatorComponent
  ]
})
export class CrudLoadingIndicatorModule {
}
