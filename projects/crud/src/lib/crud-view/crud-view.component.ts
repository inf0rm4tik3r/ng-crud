import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {switchMap, takeUntil, tap} from 'rxjs/operators';
import {CrudComponent} from '../crud-component';
import {CrudDataProviderService} from '../crud-data-provider/crud-data-provider.service';
import {CrudEventService} from '../crud-event.service';
import {CrudLoggingService} from '../crud-logging.service';
import {CrudModalService} from '../crud-modal/crud-modal.service';
import {CrudRouterService} from '../crud-router.service';
import {CrudStateService} from '../crud-state.service';
import {Entry, EntryKey} from '../types/crud-types';

@Component({
  selector: 'crud-view',
  templateUrl: './crud-view.component.html',
  styleUrls: ['./crud-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudViewComponent extends CrudComponent implements OnInit, OnDestroy {

  triggerReload$: Subject<EntryKey> = new Subject<EntryKey>();
  loading: boolean = false;
  entry: Entry | undefined = undefined;

  constructor(private crudEventService: CrudEventService,
              private crudStateService: CrudStateService,
              private crudModalService: CrudModalService,
              private crudDataProviderService: CrudDataProviderService,
              private crudRouterService: CrudRouterService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  getComponentName(): string {
    return 'view';
  }

  ngOnInit(): void {
    this.triggerReload$
      .pipe(
        takeUntil(this.unsubscribe$),
        tap((entryKey: EntryKey) => {
          this.logger.trace('loading entry', entryKey);
          this.loading = true;
          this.cdr.detectChanges();
        }),
        switchMap((entryKey: EntryKey) =>
          this.crudDataProviderService.getOne(entryKey)
        )
      ).subscribe(
      (entry: Entry) => {
        this.logger.trace('Loaded entry', entry);
        this.entry = entry;
        this.loading = false;
        this.cdr.detectChanges();
      },
      () => {
        this.loading = false;
        this.cdr.detectChanges();
      },
      () => {
        this.loading = false;
        this.cdr.detectChanges();
      }
    );

    this.crudStateService.activeEntryKey$
      .pipe(
        takeUntil(this.unsubscribe$),
        tap((activeEntryKey: EntryKey | undefined) => {
          if (activeEntryKey) {
            this.triggerReload$.next(activeEntryKey);
          } else {
            this.entry = undefined;
          }
        })
      )
      .subscribe();

    this.crudEventService.keydownInActiveInstance$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event: KeyboardEvent) => this.handleKeyDown(event));
  }

  private handleKeyDown(event: KeyboardEvent) {
    if (event.key === 'l' && !this.crudModalService.hasModals()) {
      this.back();
    }
  }

  edit(): void {
    this.crudRouterService.openEditView();
  }

  back(): void {
    this.crudStateService.unsetActiveEntry();
    this.crudRouterService.openListView();
  }

}
