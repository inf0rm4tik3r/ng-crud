import {Injectable} from '@angular/core';
import {CrudInputService} from '../crud-input.service';

const TRANSLATIONS = {
  /* SELECT */
  'select.placeholder': {
    de: 'Bitte auswählen...',
    en: 'Please choose...'
  },
  'select.searchPlaceholder': {
    de: 'Suchen...',
    en: 'Search...'
  },
  'select.noSearchResults': {
    de: 'Keine Optionen gefunden.',
    en: 'No options found.'
  },
  'selectItem.selected': {
    de: 'gewählt',
    en: 'selected'
  },

  /* LICENSING */
  'licensing.not-licensed-warning': {
    de: 'Das Produkt ist nicht lizensiert!',
    en: 'The product is not licensed!'
  }
};

@Injectable({
  providedIn: 'root'
})
export class CrudTranslatorService {

  private readonly baseLanguage = 'en';
  private readonly baseGender = 'diverse';

  constructor(private crudInputService: CrudInputService) {
  }

  public translate(key: string, language?: string, gender?: string): string {
    // Get the translation in the locale of the currently logged in user if not otherwise specified.
    if (!language) {
      language = this.crudInputService.currentUser.locale;
    }
    language = language.toLowerCase();

    // Get the gender if not otherwise specified.
    if (!gender) {
      gender = this.crudInputService.currentUser.gender;
    }
    gender = gender.toLowerCase();

    const translations = TRANSLATIONS[key];
    if (translations) {
      let translationIntoLanguage;

      if (translations[language]) {
        // Use the translation into the requested language if possible.
        translationIntoLanguage = translations[language];
      }
      else if (translations[this.baseLanguage]) {
        // Fall back to the translation into the base language
        // if a translation into the requested language is not available.
        translationIntoLanguage = translations[this.baseLanguage];
      }

      if (translationIntoLanguage) {
        if (typeof translationIntoLanguage === 'string') {
          return translationIntoLanguage;
        }
        else if (translationIntoLanguage[gender]) {
          return translationIntoLanguage[gender];
        }
        else if (translationIntoLanguage[this.baseGender]) {
          return translationIntoLanguage[this.baseGender];
        }
      }
    }

    // Either the key did not match any translations or neither a translation into the requested language
    // nor the base language has benn specified for the given key. Or, if a gender based translation is requested,
    // neither a translation into the requested gender nor a translation into the base gender has been specified.
    return '{{' + key + '[' + language + '][' + gender + ']}}';
  }
}
