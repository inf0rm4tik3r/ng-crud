import {TestBed} from '@angular/core/testing';

import {CrudTranslatorService} from './crud-translator.service';

describe('CrudTranslatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudTranslatorService = TestBed.get(CrudTranslatorService);
    expect(service).toBeTruthy();
  });
});
