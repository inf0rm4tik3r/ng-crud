export function range(start: number, end: number, step: number = 1): ReadonlyArray<number> {
  const result = [];
  let i;
  for (i = start; i < end; i += step) {
    result.push(i);
  }
  if (i === end) {
    result.push(end);
  }
  return result;
}

export function timestamp(): number {
  return Date.now().valueOf();
  // return window.performance && window.performance.now ? window.performance.now() : Date.now().valueOf();
}
