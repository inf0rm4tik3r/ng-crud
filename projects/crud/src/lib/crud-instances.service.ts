import {Injectable} from '@angular/core';
import {CrudInstanceComponent} from './crud-instance/crud-instance.component';
import {CrudLoggingService, LogLevel} from './crud-logging.service';

@Injectable({
  providedIn: 'root'
})
export class CrudInstancesService {

  private logger: CrudLoggingService = new CrudLoggingService()
    .init('INSTANCES')
    .setLogLevel(LogLevel.WARN);

  private instances: CrudInstanceComponent[] = [];

  register(instance: CrudInstanceComponent): void {
    this.instances.push(instance);
    this.logger.debug('Registered CRUD instance', instance);
  }

  unregister(instance: CrudInstanceComponent): void {
    const index = this.instances.indexOf(instance);
    if (index > -1) {
      this.instances.splice(index, 1);
    }
    this.logger.debug('Unregistered CRUD instance', instance);
  }

  get(): CrudInstanceComponent[] {
    return this.instances;
  }

}
