import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Type,
  ViewChild
} from '@angular/core';
import {ReplaySubject} from 'rxjs';
import {CrudConfigService} from '../crud-config.service';
import {CrudContainerDirective} from '../crud-container.directive';
import {CrudBaseFieldComponent} from '../crud-fields/crud-base-field.component';
import {CrudColorFieldComponent} from '../crud-fields/crud-color-field/crud-color-field.component';
import {CrudGeneralFieldComponent} from '../crud-fields/crud-general-field/crud-general-field.component';
import {CrudMultiselectFieldComponent} from '../crud-fields/crud-multiselect-field/crud-multiselect-field.component';
import {CrudNestedInstanceFieldComponent} from '../crud-fields/crud-nested-instance-field/crud-nested-instance-field.component';
import {CrudNumberFieldComponent} from '../crud-fields/crud-number-field/crud-number-field.component';
import {CrudRelationFieldComponent} from '../crud-fields/crud-relation-field/crud-relation-field.component';
import {CrudSelectFieldComponent} from '../crud-fields/crud-select-field/crud-select-field.component';
import {CrudStringFieldComponent} from '../crud-fields/crud-string-field/crud-string-field.component';
import {CrudInjectionService} from '../crud-injection.service';
import {CrudLoggingService} from '../crud-logging.service';
import {DataTypeDefinition, Entry, FieldModeOptions} from '../types/crud-types';

@Component({
  selector: 'crud-field',
  template: '<ng-template crudComponentContainer></ng-template>',
  styleUrls: ['./crud-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudFieldComponent implements OnInit, AfterViewInit {

  /**
   * The injection point for the dynamic field components.
   */
  @ViewChild(CrudContainerDirective)
  container: CrudContainerDirective;

  /**
   * The entry of which a specific field should be shown.
   * Might be undefined, if no entry exists yet. For example when creating a new record.
   */
  @Input()
  entry: Entry | undefined;

  /**
   * The type definition for this field.
   * Contains the name of the field with which data for the field can be extracted from the given entry.
   */
  @Input()
  typeDef: DataTypeDefinition;

  /**
   * The mode of a field specifies how the field must be rendered.
   * Viable options are denoted by the type.
   *
   * 'value' means: Just display the raw value without a wrapping input element.
   */
  @Input()
  mode: FieldModeOptions = 'value';

  /**
   * Emits the new value of this field every time it changes.
   */
  @Output()
  valueChange: EventEmitter<unknown> = new EventEmitter<unknown>();

  /**
   * Stores the final type of this field.
   * Used to determine the renderer for this field.
   */
  type: string;

  /**
   * Stores a reference to the injected component
   * so that an interaction with the injected component is possible.
   */
  componentRef: ComponentRef<unknown>;

  /**
   * Subject to forward new values into the injected component.
   */
  value$: ReplaySubject<unknown | undefined> = new ReplaySubject<unknown | undefined>(1);

  /**
   * Subject to forward new typeDef values into the injected component.
   */
  typeDef$: ReplaySubject<DataTypeDefinition> = new ReplaySubject<DataTypeDefinition>(1);

  /**
   * Subject to forward new mode values into the injected component.
   */
  mode$: ReplaySubject<FieldModeOptions> = new ReplaySubject<FieldModeOptions>(1);

  constructor(
    private elementRef: ElementRef,
    private crudConfigService: CrudConfigService,
    private crudInjectionService: CrudInjectionService,
    private cdr: ChangeDetectorRef,
    private logger: CrudLoggingService) {
  }

  ngOnInit(): void {
    // The type given by the typeDef input might not be the final type,
    // because a custom type might have been specified by the user.
    this.type = this.crudConfigService.config.getFieldType(this.typeDef.name, this.typeDef.type);
    this.cdr.detectChanges();
  }

  ngAfterViewInit(): void {
    // Inject a dynamic field component based on this fields final type.
    switch (this.type) {
      case 'int':
        this.inject(this.customRenderer('int') || CrudNumberFieldComponent);
        break;
      case 'float':
        this.inject(this.customRenderer('float') || CrudNumberFieldComponent);
        break;
      case 'number':
        this.inject(this.customRenderer('number') || CrudNumberFieldComponent);
        break;
      case 'string':
        this.inject(this.customRenderer('string') || CrudStringFieldComponent);
        break;
      case 'select':
        this.inject(this.customRenderer('select') || CrudSelectFieldComponent);
        break;
      case 'multiselect':
        this.inject(this.customRenderer('multiselect') || CrudMultiselectFieldComponent);
        break;
      case 'color':
        this.inject(this.customRenderer('color') || CrudColorFieldComponent);
        break;
      case 'relation':
        this.inject(this.customRenderer('relation') || CrudRelationFieldComponent);
        break;
      case 'nested-instance':
        this.inject(this.customRenderer('nested-instance') || CrudNestedInstanceFieldComponent);
        break;
      default:
        this.logger.warn(this.customRenderer(this.type) || CrudNumberFieldComponent);
        this.inject(this.customRenderer(this.type) || CrudGeneralFieldComponent);
        break;
    }
  }

  private customRenderer(type: string): Type<unknown> | undefined {
    return this.crudConfigService.config.getFieldRenderer(type);
  }

  /**
   * Dynamically inject the specified componentClass into the view of this component.
   * @param componentClass The class of the component to inject.
   */
  private inject(componentClass: Type<unknown>): void {
    // The inputs for the injected component.
    const inputs = {
      typeDef$: this.typeDef$,
      value$: this.value$,
      mode$: this.mode$
    };
    // The output receivers for the injected component.
    const outputs = {
      valueChange: (($event) => this.valueChange.emit($event))
    };
    this.componentRef = this.crudInjectionService.appendComponentToViewContainer(
      this.container.viewContainerRef,
      componentClass,
      {inputs, outputs}
    );
    // Provide initial values for the component.
    this.updateInjectedComponent();
    // Change detection of the injected component mus be triggered once!
    // Otherwise an "ExpressionChangedAfterItHasBeenCheckedError" occurs.
    if (this.componentRef) {
      this.componentRef.changeDetectorRef.detectChanges();
    }
  }

  /**
   * Forwards the current values for this field into the injected component
   * and triggers the components change detection cycle.
   */
  updateInjectedComponent(): void {
    this.typeDef$.next(this.typeDef);
    this.value$.next(this.entry ? this.entry[this.typeDef.name] : undefined);
    this.mode$.next(this.mode);
  }

  getInjectedComponent(): CrudBaseFieldComponent {
    return this.componentRef.instance as CrudBaseFieldComponent;
  }

  focus(): void {
    this.getInjectedComponent().focus();
  }

  isFocused(): boolean {
    return this.getInjectedComponent().isFocused();
  }

}
