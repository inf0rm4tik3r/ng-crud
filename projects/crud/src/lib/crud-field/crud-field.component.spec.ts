import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudFieldComponent} from './crud-field.component';

describe('CrudFieldComponent', () => {
  let component: CrudFieldComponent;
  let fixture: ComponentFixture<CrudFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
