import {LinkedList} from './linked-list';

describe('LinkedList', () => {

  let list: LinkedList<string>;

  beforeEach(() => {
    list = new LinkedList<string>();
  });

  it('should create', () => {
    expect(list).toBeTruthy();
  });

  it('initial size is zero', () => {
    expect(list.size).toBe(0);
  });

  it('to string', () => {
    list.append('a');
    list.append('b');
    list.append('c');
    expect(list.toString()).toBe('tail > (- "a" "b") > ("a" "b" "c") > ("b" "c" -) > head');
  });

  it('append to empty list', () => {
    list.append('a');
    expect(list.size).toBe(1);
    expect(list.toString()).toBe('tail > (- "a" -) > head');
  });

  it('append to non empty list', () => {
    list.append('a');
    list.append('b');
    expect(list.size).toBe(2);
    expect(list.toString()).toBe('tail > (- "a" "b") > ("a" "b" -) > head');
  });

  it('prepend to empty list', () => {
    list.prepend('a');
    expect(list.size).toBe(1);
    expect(list.toString()).toBe('tail > (- "a" -) > head');
  });

  it('prepend to non empty list', () => {
    list.prepend('a');
    list.prepend('b');
    expect(list.size).toBe(2);
    expect(list.toString()).toBe('tail > (- "b" "a") > ("b" "a" -) > head');
  });

  it('remove tail removes last element and returns its data', () => {
    list.prepend('a');
    list.prepend('b');
    const tailData = list.removeTail();
    expect(list.size).toBe(1);
    expect(tailData).toBe('b');
    expect(list.toString()).toBe('tail > (- "a" -) > head');
  });

  it('remove head removes first element and returns its data', () => {
    list.append('a');
    list.append('b');
    const headData = list.removeHead();
    expect(list.size).toBe(1);
    expect(headData).toBe('b');
    expect(list.toString()).toBe('tail > (- "a" -) > head');
  });

  it('iterator', () => {
    list.append('a');
    list.append('b');
    list.append('c');
    let result = '';
    for (const elem of list) {
      result += elem;
    }
    expect(result).toBe('abc');
  });

});
