import {CrudConfig} from '../crud-config';
import {CrudDataProvider} from '../crud-data-provider/crud-data-provider';
import {CrudCondition} from '../crud-filter/crud-condition';

export interface CrudInstance {
  id: string;
  active: boolean;
}

export interface CrudInstances {
  [id: string]: CrudInstance;
}

export interface CrudDataTypeConstraints {
  [key: string]: unknown;
}

export interface DataTypeDefinition {
  type: string;
  name: string;
  nullable: boolean;
  key: boolean;
  autoIncrement: boolean;
  constraints?: CrudDataTypeConstraints | undefined;
}

export type DataStructure = DataTypeDefinition[];

export interface EntryKey {
  [key: string]: unknown;
}

export interface Entry {
  [key: string]: unknown;
}

export interface EntryUpdate {
  [key: string]: unknown;
}

export type Entries = Entry[];

export interface OrderByTypeSet {
  [key: string]: 'asc' | 'desc';
}

export interface OrderByAllowedSet {
  [key: string]: boolean;
}

export type FilterMode = 'simple' | 'normal' | 'expert';

export class RelationConfig {
  readonly fieldName: string;
  readonly dataProvider: CrudDataProvider;
  readonly targetTableConnectFieldName: string;
  readonly displayPattern: string;
  readonly filter: CrudCondition;
  readonly orderBy: OrderByTypeSet;

  constructor(fieldName: string,
              dataProvider: CrudDataProvider,
              targetTableConnectFieldName: string,
              displayPattern: string,
              filter: CrudCondition,
              orderBy: OrderByTypeSet) {
    this.fieldName = fieldName;
    this.dataProvider = dataProvider;
    this.targetTableConnectFieldName = targetTableConnectFieldName;
    this.displayPattern = displayPattern;
    this.filter = filter;
    this.orderBy = orderBy;
  }
}

export interface Relations {
  [fieldName: string]: RelationConfig;
}


export class NestedInstanceConfig {
  readonly fieldName: string;
  readonly referencedFieldName: string;
  readonly referenceFieldName: string;
  readonly config: CrudConfig;

  constructor(fieldName: string,
              referencedFieldName: string,
              referenceFieldName: string,
              config: CrudConfig) {
    this.fieldName = fieldName;
    this.referencedFieldName = referencedFieldName;
    this.referenceFieldName = referenceFieldName;
    this.config = config;
  }
}

export interface NestedInstances {
  [fieldName: string]: NestedInstanceConfig;
}

export type FieldModeOptions = 'value' | 'view' | 'edit' | 'create';

