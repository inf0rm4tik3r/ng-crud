import {CrudView} from './crud-view';
import {LinkedList} from './linked-list';

export class FixedSizedStack {

  private history: LinkedList<CrudView>;
  private maxSize: number;

  constructor(maxSize: number = 2) {
    this.history = new LinkedList<CrudView>();
    this.maxSize = maxSize;
  }

  push(view: CrudView): void {
    this.history.append(view);
    if (this.history.size > this.maxSize) {
      this.history.removeTail();
    }
  }

  peek(): CrudView {
    return this.history.head.data;
  }

  pop(): CrudView {
    return this.history.removeHead();
  }


}
