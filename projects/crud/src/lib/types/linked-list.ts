export class LinkedList<T> implements Iterable<T> {

  head: LinkedListElement<T> | null = null;
  tail: LinkedListElement<T> | null = null;
  private _size: number = 0;

  toString(): string {
    let res = 'tail';
    let elem: LinkedListElement<T> | null = this.tail;
    while (elem != null) {
      res += ' > ('
        + (elem.prev !== null ? '"' + elem.prev.data + '"' : '-')
        + ' "' + elem.data + '"'
        + ' ' + (elem.next !== null ? '"' + elem.next.data + '"' : '-')
        + ')';
      elem = elem.next;
    }
    res += ' > head';
    return res;
  }

  /**
   * Add a new element to the tail of the list.
   */
  prepend(data: T): void {
    const elem = new LinkedListElement<T>(data);
    if (this.tail) {
      this.tail.prev = elem;
      elem.next = this.tail;
      this.tail = elem;
    } else {
      this.head = elem;
      this.tail = elem;
    }
    this._size++;
  }

  /**
   * Add a new element to the head of the list.
   */
  append(data: T): LinkedListElement<T> {
    const elem = new LinkedListElement<T>(data);
    if (this.head) {
      this.head.next = elem;
      elem.prev = this.head;
      this.head = elem;
    } else {
      this.head = elem;
      this.tail = elem;
    }
    this._size++;
    return elem;
  }

  removeHead(): T {
    if (this.head === null) {
      throw new Error('List has no head to remove!');
    }
    const oldHead = this.head;
    this.head = oldHead.prev;
    this.head.next = null;
    // The old head should not have a next element. But we want to be safe to not create any dangling reference!
    oldHead.next = null;
    oldHead.prev = null;
    this._size--;
    return oldHead.data;
  }

  removeTail(): T {
    if (this.tail === null) {
      throw new Error('List has no tail to remove!');
    }
    const oldTail = this.tail;
    this.tail = oldTail.next;
    this.tail.prev = null;
    // The old tail should not have a previous element. But we want to be safe to not create any dangling reference!
    oldTail.prev = null;
    oldTail.next = null;
    this._size--;
    return oldTail.data;
  }

  // TODO: Test
  find(data: T): LinkedListElement<T> | undefined {
    let pointer: LinkedListElement<T> = this.head;
    while (pointer !== null) {
      if (pointer.data === data) {
        return pointer;
      }
      pointer = pointer.prev;
    }
    return undefined;
  }

  // TODO: Test
  remove(data: T): boolean {
    const element: LinkedListElement<T> | undefined = this.find(data);
    if (element) {
      this.removeElement(element);
      return true;
    }
    console.warn('Unable to find element with data', data, 'in', this);
    return false;
  }

  // TODO: Test
  removeElement(elem: LinkedListElement<T>): void {
    if (elem.prev !== null) {
      elem.prev.next = elem.next;
    }
    if (elem.next !== null) {
      elem.next.prev = elem.prev;
    }
    if (elem === this.head) {
      this.head = null;
    }
    if (elem === this.tail) {
      this.tail = null;
    }
    elem.prev = null;
    elem.next = null;
    elem.data = null;
    this._size--;
  }

  // TODO: Test
  clear(): void {
    this.head = null;
    this.tail = null;
    this._size = 0;
  }

  get size(): number {
    return this._size;
  }

  [Symbol.iterator](): Iterator<T, { done: boolean, value: T }> {
    let pointer: LinkedListElement<T> = this.head;
    return {
      next(): IteratorResult<T> {
        if (pointer !== null) {
          const data: T = pointer.data;
          pointer = pointer.prev;
          return {
            done: false,
            value: data
          };
        } else {
          return {
            done: true,
            value: null
          };
        }
      }
    };
  }

}

export class LinkedListElement<T> {

  data: T;

  prev: LinkedListElement<T> | null = null;
  next: LinkedListElement<T> | null = null;

  constructor(data: T) {
    this.data = data;
  }

}
