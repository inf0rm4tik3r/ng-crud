export enum CrudView {
  LIST,
  CREATE,
  EDIT,
  VIEW,
  HISTORY
}
