/**
 * Focusable components must implement this interface
 * - to show intent
 * - to be referencable as something focusable and
 * - to have implementation guidance
 *
 * A general way to implement this would be:
 * - injecting the ElementRef through "private elementRef: ElementRef"
 * - injecting the Document through "@Inject(DOCUMENT) private document: HTMLDocument"
 * - marking the component with '/** @dynamic \*\/' if necessary
 * - implementing the methods of this interface using elementRef.nativeElement and document.activeElement
 */
export interface CrudFocusable {

  focus(): void;

  blur(): void;

  isFocused(): boolean;

  getNativeElement(): Element;

}
