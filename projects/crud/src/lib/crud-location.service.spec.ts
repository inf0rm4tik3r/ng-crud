import { TestBed } from '@angular/core/testing';

import { CrudLocationService } from './crud-location.service';

describe('CrudLocationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudLocationService = TestBed.get(CrudLocationService);
    expect(service).toBeTruthy();
  });
});
