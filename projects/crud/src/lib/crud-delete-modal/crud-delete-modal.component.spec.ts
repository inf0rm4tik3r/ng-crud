import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudDeleteModalComponent} from './crud-delete-modal.component';

describe('CrudDeleteModalComponent', () => {
  let component: CrudDeleteModalComponent;
  let fixture: ComponentFixture<CrudDeleteModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudDeleteModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudDeleteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
