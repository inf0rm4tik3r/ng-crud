import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {CrudComponent} from '../crud-component';
import {CrudConfigService} from '../crud-config.service';
import {CrudLoggingService} from '../crud-logging.service';
import {Entry} from '../types/crud-types';

@Component({
  selector: 'crud-delete-modal',
  templateUrl: './crud-delete-modal.component.html',
  styleUrls: ['./crud-delete-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudDeleteModalComponent extends CrudComponent {

  @Input()
  entry: Entry;

  @Output()
  cancelClicked: EventEmitter<void> = new EventEmitter();

  @Output()
  deleteClicked: EventEmitter<void> = new EventEmitter();

  constructor(public crudConfigService: CrudConfigService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  getComponentName(): string {
    return 'delete-modal';
  }

}
