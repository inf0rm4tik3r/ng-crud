import { TestBed } from '@angular/core/testing';

import { CrudConfigService } from './crud-config.service';

describe('CrudConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudConfigService = TestBed.get(CrudConfigService);
    expect(service).toBeTruthy();
  });
});
