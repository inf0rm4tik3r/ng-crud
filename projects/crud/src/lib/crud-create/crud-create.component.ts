import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {first, takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../crud-component';
import {CrudDataProviderService} from '../crud-data-provider/crud-data-provider.service';
import {CrudEventService} from '../crud-event.service';
import {CrudFieldsComponent} from '../crud-fields/crud-fields.component';
import {CrudLoggingService} from '../crud-logging.service';
import {CrudModalService} from '../crud-modal/crud-modal.service';
import {CrudModifiedModalComponent} from '../crud-modified-modal/crud-modified-modal.component';
import {CrudRouterService} from '../crud-router.service';
import {CrudStateService} from '../crud-state.service';
import {Entry} from '../types/crud-types';

@Component({
  selector: 'crud-create',
  templateUrl: './crud-create.component.html',
  styleUrls: ['./crud-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudCreateComponent extends CrudComponent implements OnInit {

  @ViewChild(CrudFieldsComponent)
  fields: CrudFieldsComponent;

  constructor(private crudEventService: CrudEventService,
              private crudStateService: CrudStateService,
              private crudDataProviderService: CrudDataProviderService,
              private crudRouterService: CrudRouterService,
              private crudModalService: CrudModalService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  ngOnInit(): void {
    this.crudEventService.keydownInActiveInstance$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event: KeyboardEvent) => this.handleKeyDown(event));
  }

  handleKeyDown(event: KeyboardEvent): void {
    if (event.key === 'l' && !this.crudModalService.hasModals()) {
      this.toListView();
    }
  }

  save(): void {
    const entry: Entry = this.fields.getInput();
    this.logger.trace('save new entry', entry);

    this.crudDataProviderService.add(entry)
      .pipe(first())
      .subscribe(([createdKey, createdEntry]) => {
        this.logger.trace('New entry saved. Key is', createdKey, createdEntry);
        this.fields.unsavedChanges = false;
        this.cdr.detectChanges();
        this.crudStateService.setActiveEntry([createdEntry, createdKey]);
        this.crudRouterService.openEditView();
      });
  }

  saveAndReturn(): void {
    const input: Entry = this.fields.getInput();
    this.logger.trace('save new entry and return', input);

    this.crudDataProviderService.add(input)
      .pipe(first())
      .subscribe((_) => {
        this.fields.unsavedChanges = false;
        this.cdr.detectChanges();
        this.crudStateService.unsetActiveEntry();
        this.crudRouterService.openListView();
      });
  }

  saveAndNew(): void {
    const input: Entry = this.fields.getInput();
    this.logger.trace('save new entry and create next', input);

    this.crudDataProviderService.add(input)
      .pipe(first())
      .subscribe((_) => {
        this.fields.reset();
        this.cdr.detectChanges();
        this.crudRouterService.openCreateView();
      });
  }

  toListView(): void {
    if (this.fields.unsavedChanges) {
      this.crudModalService.init(CrudModifiedModalComponent, {}, {
        cancelClicked: () => {
          this.crudModalService.destroy();
        },
        leaveClicked: () => {
          this.crudStateService.unsetActiveEntry();
          this.crudRouterService.openListView();
          this.crudModalService.destroy();
        }
      });
    } else {
      this.crudStateService.unsetActiveEntry();
      this.crudRouterService.openListView();
    }
  }

  getComponentName(): string {
    return 'create';
  }

}
