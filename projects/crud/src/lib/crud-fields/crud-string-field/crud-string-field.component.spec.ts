import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudStringFieldComponent} from './crud-string-field.component';

describe('CrudStringFieldComponent', () => {
  let component: CrudStringFieldComponent;
  let fixture: ComponentFixture<CrudStringFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudStringFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudStringFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
