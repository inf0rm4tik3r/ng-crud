import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudFieldsComponent} from './crud-fields.component';

describe('CrudFieldsComponent', () => {
  let component: CrudFieldsComponent;
  let fixture: ComponentFixture<CrudFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
