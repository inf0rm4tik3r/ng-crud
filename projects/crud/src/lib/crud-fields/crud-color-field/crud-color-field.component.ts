import {DOCUMENT} from '@angular/common';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Inject} from '@angular/core';
import {CrudConfigService} from '../../crud-config.service';
import {CrudEventService} from '../../crud-event.service';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudModalService} from '../../crud-modal/crud-modal.service';
import {CrudStateService} from '../../crud-state.service';
import {Color} from '../../helper/color';
import {CrudBaseFieldComponent} from '../crud-base-field.component';

/** @dynamic */
@Component({
  selector: 'app-crud-color-field',
  templateUrl: './crud-color-field.component.html',
  styleUrls: ['./crud-color-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudColorFieldComponent extends CrudBaseFieldComponent {

  color = Color;

  constructor(protected crudEventService: CrudEventService,
              public crudConfigService: CrudConfigService,
              protected crudStateService: CrudStateService,
              protected crudModalService: CrudModalService,
              protected logger: CrudLoggingService,
              protected cdr: ChangeDetectorRef,
              @Inject(DOCUMENT) protected document: HTMLDocument) {
    super(crudEventService, crudConfigService, crudStateService, crudModalService, logger, cdr);
  }

  performIsFocused(): boolean {
    return (this.input as ElementRef<HTMLInputElement>).nativeElement === this.document.activeElement;
  }

  performFocus(): void {
    (this.input as ElementRef<HTMLInputElement>).nativeElement.focus();
  }

  performUnfocus(): void {
    (this.input as ElementRef<HTMLInputElement>).nativeElement.blur();
  }

  update(newValue: string): void {
    this.cdr.detectChanges();
    this.valueChange.emit(newValue);
  }

}
