import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudColorFieldComponent} from './crud-color-field.component';

describe('CrudColorFieldComponent', () => {
  let component: CrudColorFieldComponent;
  let fixture: ComponentFixture<CrudColorFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudColorFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudColorFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
