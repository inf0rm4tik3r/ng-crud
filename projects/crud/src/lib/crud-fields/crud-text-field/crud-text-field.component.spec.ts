import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudTextFieldComponent} from './crud-text-field.component';

describe('CrudTextFieldComponent', () => {
  let component: CrudTextFieldComponent;
  let fixture: ComponentFixture<CrudTextFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudTextFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudTextFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
