import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudMultiselectFieldComponent} from './crud-multiselect-field.component';

describe('CrudMultiselectFieldComponent', () => {
  let component: CrudMultiselectFieldComponent;
  let fixture: ComponentFixture<CrudMultiselectFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudMultiselectFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudMultiselectFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
