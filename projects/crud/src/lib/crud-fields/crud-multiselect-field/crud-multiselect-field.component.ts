import {DOCUMENT} from '@angular/common';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Inject} from '@angular/core';
import {CrudConfigService} from '../../crud-config.service';
import {CrudEventService} from '../../crud-event.service';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudModalService} from '../../crud-modal/crud-modal.service';
import {CrudSelectComponent} from '../../crud-select/crud-select.component';
import {CrudStateService} from '../../crud-state.service';
import {CrudBaseFieldComponent} from '../crud-base-field.component';

/** @dynamic */
@Component({
  selector: 'crud-multiselect-field',
  templateUrl: './crud-multiselect-field.component.html',
  styleUrls: ['./crud-multiselect-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudMultiselectFieldComponent extends CrudBaseFieldComponent {

  constructor(protected crudEventService: CrudEventService,
              public crudConfigService: CrudConfigService,
              protected crudStateService: CrudStateService,
              protected crudModalService: CrudModalService,
              protected logger: CrudLoggingService,
              protected cdr: ChangeDetectorRef,
              @Inject(DOCUMENT) protected document: HTMLDocument) {
    super(crudEventService, crudConfigService, crudStateService, crudModalService, logger, cdr);
  }

  performIsFocused(): boolean {
    return (this.input as ElementRef<unknown>).nativeElement === this.document.activeElement;
  }

  performFocus(): void {
    ((this.input as ElementRef<unknown>).nativeElement as CrudSelectComponent).openMenu(); //TODO: call focus() instead
  }

  performUnfocus(): void {
    ((this.input as ElementRef<unknown>).nativeElement as CrudSelectComponent).closeMenu(); //TODO: call unfocus() instead
  }

}
