import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudFileFieldComponent} from './crud-file-field.component';

describe('CrudFileFieldComponent', () => {
  let component: CrudFileFieldComponent;
  let fixture: ComponentFixture<CrudFileFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudFileFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudFileFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
