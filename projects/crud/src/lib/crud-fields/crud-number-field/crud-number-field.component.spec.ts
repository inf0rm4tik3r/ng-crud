import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudNumberFieldComponent} from './crud-number-field.component';

describe('CrudNumberFieldComponent', () => {
  let component: CrudNumberFieldComponent;
  let fixture: ComponentFixture<CrudNumberFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudNumberFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudNumberFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
