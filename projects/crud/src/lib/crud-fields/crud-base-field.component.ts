import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../crud-component';
import {CrudConfigService} from '../crud-config.service';
import {CrudEventService} from '../crud-event.service';
import {CrudLoggingService} from '../crud-logging.service';
import {CrudModalService} from '../crud-modal/crud-modal.service';
import {CrudStateService} from '../crud-state.service';
import {DataTypeDefinition, FieldModeOptions} from '../types/crud-types';

@Component({
  template: ''
})
export abstract class CrudBaseFieldComponent extends CrudComponent implements OnInit {

  @Input()
  typeDef$: Observable<DataTypeDefinition>;

  @Input()
  value$: Observable<unknown | undefined>;

  @Input()
  mode$: Observable<FieldModeOptions>;

  @Output()
  valueChange: EventEmitter<unknown> = new EventEmitter<unknown>();

  @ViewChild('input')
  input: unknown;

  protected constructor(protected crudEventService: CrudEventService,
                        public crudConfigService: CrudConfigService,
                        protected crudStateService: CrudStateService,
                        protected crudModalService: CrudModalService,
                        protected logger: CrudLoggingService,
                        protected cdr: ChangeDetectorRef) {
    super(cdr, logger);
  }

  ngOnInit(): void {
    this.crudEventService.keydownInActiveInstance$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event: KeyboardEvent) => this.handleKeyDown(event));
  }

  private handleKeyDown(event: KeyboardEvent): void {
    if (event.key === 'Escape' && !this.crudModalService.hasModals() && this.isFocused()) {
      this.unfocus();
      event.stopPropagation();
    }
  }

  abstract performIsFocused(): boolean;

  isFocused(): boolean {
    if (this.input) {
      return this.performIsFocused();
    } else {
      return false;
    }
  }

  abstract performFocus(): void;

  focus(): void {
    if (this.input) {
      this.performFocus();
      this.cdr.detectChanges();
    }
  }

  abstract performUnfocus(): void;

  unfocus(): void {
    if (this.input) {
      this.performUnfocus();
      this.cdr.detectChanges();
    }
  }

  onValueChange(newValue: unknown | undefined): void {
    this.valueChange.emit(newValue);
  }

  getComponentName(): string {
    return 'base-field';
  }

}
