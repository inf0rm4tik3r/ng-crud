import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudPasswordFieldComponent} from './crud-password-field.component';

describe('CrudPasswordFieldComponent', () => {
  let component: CrudPasswordFieldComponent;
  let fixture: ComponentFixture<CrudPasswordFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudPasswordFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudPasswordFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
