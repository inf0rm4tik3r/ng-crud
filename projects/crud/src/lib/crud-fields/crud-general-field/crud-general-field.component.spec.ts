import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudGeneralFieldComponent} from './crud-general-field.component';

describe('CrudGeneralFieldComponent', () => {
  let component: CrudGeneralFieldComponent;
  let fixture: ComponentFixture<CrudGeneralFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudGeneralFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudGeneralFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
