import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {CrudConfigService} from '../../crud-config.service';
import {CrudEventService} from '../../crud-event.service';
import {CrudInstanceService} from '../../crud-instance/crud-instance-service';
import {CrudInstanceComponent} from '../../crud-instance/crud-instance.component';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudModalService} from '../../crud-modal/crud-modal.service';
import {CrudStateService} from '../../crud-state.service';
import {NestedInstanceConfig} from '../../types/crud-types';
import {CrudBaseFieldComponent} from '../crud-base-field.component';

@Component({
  selector: 'crud-nested-instance-field',
  templateUrl: './crud-nested-instance-field.component.html',
  styleUrls: ['./crud-nested-instance-field.component.scss']
})
export class CrudNestedInstanceFieldComponent extends CrudBaseFieldComponent implements OnInit, OnDestroy {

  nestedInstanceConfig: NestedInstanceConfig;

  constructor(protected crudEventService: CrudEventService,
              public crudConfigService: CrudConfigService,
              protected crudStateService: CrudStateService,
              protected crudModalService: CrudModalService,
              protected logger: CrudLoggingService,
              protected cdr: ChangeDetectorRef,
              public crudInstanceService: CrudInstanceService) {
    super(crudEventService, crudConfigService, crudStateService, crudModalService, logger, cdr);
  }

  ngOnInit(): void {
    this.typeDef$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((typeDef) => {
        this.nestedInstanceConfig = this.crudConfigService.config.getNestedInstance(typeDef.name);
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  performIsFocused(): boolean {
    return this.input ? (this.input as CrudInstanceComponent).isFocused() : false;
  }

  performFocus(): void {
    if (this.input) {
      (this.input as CrudInstanceComponent).focus();
    }
  }

  performUnfocus(): void {
    if (this.input) {
      (this.input as CrudInstanceComponent).blur();
    }
  }

  getComponentName(): string {
    return 'nested-instance-field';
  }

}
