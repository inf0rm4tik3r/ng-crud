import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudNestedInstanceFieldComponent} from './crud-nested-instance-field.component';

describe('CrudNestedInstanceFieldComponent', () => {
  let component: CrudNestedInstanceFieldComponent;
  let fixture: ComponentFixture<CrudNestedInstanceFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudNestedInstanceFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudNestedInstanceFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
