import {Component, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {first} from 'rxjs/operators';
import {CrudConfigService} from '../crud-config.service';
import {CrudDataProviderService} from '../crud-data-provider/crud-data-provider.service';
import {CrudFieldComponent} from '../crud-field/crud-field.component';
import {DataStructure, DataTypeDefinition, Entry, FieldModeOptions, NestedInstances} from '../types/crud-types';

@Component({
  selector: 'crud-fields',
  templateUrl: './crud-fields.component.html',
  styleUrls: ['./crud-fields.component.scss']
})
export class CrudFieldsComponent implements OnInit {

  @ViewChildren(CrudFieldComponent)
  fields: QueryList<CrudFieldComponent>;

  @Input()
  mode: FieldModeOptions = 'value';

  dataStructure: DataStructure = undefined;
  nestedInstances: NestedInstances = {};

  private _entry: Entry;

  input: Entry = {};

  // TODO: Store change info.
  unsavedChanges: boolean = false;

  constructor(private crudConfigService: CrudConfigService,
              private crudDataProviderService: CrudDataProviderService) {
  }

  ngOnInit(): void {
    this.reloadStructure();
  }

  reset(): void {
    this.reloadStructure();
    this.entry = undefined;
    this.input = {};
    this.unsavedChanges = false;
  }

  reloadStructure(): void {
    this.crudDataProviderService.getStructure()
      .pipe(first())
      .subscribe((dataStructure: DataStructure) => {
        this.dataStructure = dataStructure;
        this.initInput();
      });

    this.nestedInstances = this.crudConfigService.config.getNestedInstances();
  }

  @Input()
  set entry(entry: Entry) {
    this._entry = entry;
    this.initInput();
  }

  get entry(): Entry {
    return this._entry;
  }

  // TODO: Init input by calling an init method on each injected field.
  initInput(): void {
    if (!!this._entry && !!this.dataStructure) {
      this.dataStructure.forEach((typeDef: DataTypeDefinition) => {
        this.input[typeDef.name] = this._entry[typeDef.name];
      });
      this.unsavedChanges = false; // TODO: Better alternative?
    }
  }

  updateInput(newValue: unknown, typeDefName: string): void {
    this.input[typeDefName] = newValue;
    this.unsavedChanges = true; // TODO: Compare with initial values.
  }

  // TODO: Retrieve input by calling a method on each injected field.
  getInput(): Entry {
    return Object.assign({}, this.input);
  }

  isAnyFieldFocused(): boolean {
    for (const field of this.fields) {
      if (field.isFocused()) {
        return true;
      }
    }
    return false;
  }

}
