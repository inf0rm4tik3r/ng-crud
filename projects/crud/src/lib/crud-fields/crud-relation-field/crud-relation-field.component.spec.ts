import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudRelationFieldComponent} from './crud-relation-field.component';

describe('CrudRelationFieldComponent', () => {
  let component: CrudRelationFieldComponent;
  let fixture: ComponentFixture<CrudRelationFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudRelationFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudRelationFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
