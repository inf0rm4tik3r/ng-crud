import {DOCUMENT} from '@angular/common';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {combineLatest, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {CrudConfigService} from '../../crud-config.service';
import {CrudDataProviderService} from '../../crud-data-provider/crud-data-provider.service';
import {CrudEventService} from '../../crud-event.service';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudModalService} from '../../crud-modal/crud-modal.service';
import {CrudSelectComponent} from '../../crud-select/crud-select.component';
import {Choice} from '../../crud-select/state/choices-types';
import {CrudStateService} from '../../crud-state.service';
import {Entries} from '../../types/crud-types';
import {CrudBaseFieldComponent} from '../crud-base-field.component';

/** @dynamic */
@Component({
  selector: 'crud-relation-field',
  templateUrl: './crud-relation-field.component.html',
  styleUrls: ['./crud-relation-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudRelationFieldComponent extends CrudBaseFieldComponent implements OnInit {

  optionsSubscription: Subscription;
  options: Entries;

  /**
   * The choices for the select field.
   * UNDEFINED if no choices are available jet.
   */
  choices: Choice[] | undefined = undefined;

  /**
   * The currently selected choice.
   * NULL if no choice is selected.
   * UNDEFINED if no choices are available jet.
   */
  selectedChoice: Choice | null | undefined = undefined;

  constructor(protected crudEventService: CrudEventService,
              public crudConfigService: CrudConfigService,
              protected crudStateService: CrudStateService,
              protected crudModalService: CrudModalService,
              protected logger: CrudLoggingService,
              protected cdr: ChangeDetectorRef,
              @Inject(DOCUMENT) protected document: HTMLDocument,
              private crudDataProviderService: CrudDataProviderService) {
    super(crudEventService, crudConfigService, crudStateService, crudModalService, logger, cdr);
  }

  ngOnInit(): void {
    combineLatest([this.typeDef$, this.value$])
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(([typeDef, value]) => {
        // this.logger.debug('typedef', typeDef);
        if (this.optionsSubscription) {
          this.optionsSubscription.unsubscribe();
        }
        const relationConfig = this.crudConfigService.config.getRelation(typeDef.name);
        this.optionsSubscription = this.crudDataProviderService.relation(typeDef.name)
          .getAll(
            relationConfig.orderBy,
            relationConfig.filter,
            5000 // Cache duration in milliseconds. TODO: as configuration.
          )
          .subscribe((entries: Entries) => {
            // this.logger.debug('entries', entries);
            this.options = entries;

            // Map entries to choices. Could be costly...
            this.choices = [];
            this.selectedChoice = null;
            for (const entry of entries) {
              this.choices.push({
                value: entry.id as string,
                label: entry.name as string,
                selected: entry.id === value
              } as Choice);
            }
            for (const choice of this.choices) {
              if (choice.selected) {
                this.selectedChoice = choice;
              }
            }

            this.cdr.detectChanges();
          });
      });
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    if (this.optionsSubscription) {
      this.optionsSubscription.unsubscribe();
    }
  }

  performIsFocused(): boolean {
    return this.input === this.document.activeElement;
  }

  performFocus(): void {
    (this.input as CrudSelectComponent).focus();
    this.cdr.detectChanges(); // TODO: necessary?
  }

  performUnfocus(): void {
    (this.input as CrudSelectComponent).blur();
    // this.cdr.detectChanges();
  }

  onValueChange(selection: Choice | Choice[] | undefined): void {
    if (Array.isArray(selection)) {
      this.logger.error('Not expecting multiple selections!');
      return;
    }
    super.onValueChange(selection ? selection.value : undefined);
  }

  getComponentName(): string {
    return 'relation-field';
  }

}
