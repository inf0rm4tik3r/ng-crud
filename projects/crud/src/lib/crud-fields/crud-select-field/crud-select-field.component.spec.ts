import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudSelectFieldComponent} from './crud-select-field.component';

describe('CrudSelectFieldComponent', () => {
  let component: CrudSelectFieldComponent;
  let fixture: ComponentFixture<CrudSelectFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudSelectFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudSelectFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
