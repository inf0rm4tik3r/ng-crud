import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudColorPickerComponent} from './crud-color-picker.component';

describe('CrudColorPickerComponent', () => {
  let component: CrudColorPickerComponent;
  let fixture: ComponentFixture<CrudColorPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudColorPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
