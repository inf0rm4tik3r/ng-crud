import {Injectable} from '@angular/core';
import {CrudConfig} from './crud-config';
import {CrudStateService} from './crud-state.service';

@Injectable()
export class CrudConfigService {

  private readonly _config: CrudConfig;

  constructor(private crudStateService: CrudStateService) {
    this._config = this.buildDefaultConfig();
  }

  buildDefaultConfig(): CrudConfig {
    return new CrudConfig();
  }

  initializeFrom(userConfig: CrudConfig | undefined): void {
    // The userConfig might not be present if no CrudConfig input was provided to the instance.
    if (userConfig) {
      this._config.mergeWith(userConfig);
    }

    // Initialize the state from the merged configuration.
    // For example: This copies the preconfigured orderBy configuration to the current state.
    this.crudStateService.initializeFrom(this._config);
  }

  get config(): CrudConfig {
    return this._config;
  }

}
