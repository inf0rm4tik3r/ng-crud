export abstract class NumberConversion {

  static hexToDecimal(hex: string): number {
    let i, j;
    const digits = [0];
    let carry;
    for (i = 0; i < hex.length; i += 1) {
      carry = parseInt(hex.charAt(i), 16);
      for (j = 0; j < digits.length; j += 1) {
        digits[j] = digits[j] * 16 + carry;
        carry = digits[j] / 10 | 0;
        digits[j] %= 10;
      }
      while (carry > 0) {
        digits.push(carry % 10);
        carry = carry / 10 | 0;
      }
    }
    return Number.parseFloat(digits.reverse().join(''));
  }

}
