import {NumberConversion} from './number-conversion';

export class Color {

  static hexToRGB(color: string, convertToZeroOne: boolean = false): [number, number, number] | undefined {
    if (color.startsWith('#')) {
      color = color.substr(1);
    }
    if (color.length !== 6) {
      return undefined;
    }
    const r = color.substr(0, 2);
    const g = color.substr(2, 2);
    const b = color.substr(4, 2);
    const rDec = NumberConversion.hexToDecimal(r);
    const gDec = NumberConversion.hexToDecimal(g);
    const bDec = NumberConversion.hexToDecimal(b);
    if (convertToZeroOne) {
      return [rDec / 255, gDec / 255, bDec / 255];
    }
    return [rDec, gDec, bDec];
  }

  static rgbToLuminance([r, g, b]: [number, number, number]): number {
    return 0.2126 * r + 0.7152 * g + 0.0722 * b; // a) Photometric/digital ITU BT.709
    // return 0.33 * r + 0.5 * g + 0.16 * b; // a) slow approximation
    // return (r + r + b + g + g + g) / 6; // a) fast approximation
    // return 0.299 * r + 0.587 * g + 0.114 * b; // b) Digital ITU BT.601 (gives more weight to the R and B components).
    // return 0.375 * r + 0.5 * g + 0.125 * b; // b) slow approximation.
    // return (r + r + r + b + g + g + g + g) >> 3; // b) fast approximation
  }

  static hexToLuminance(hex: string | undefined): number {
    if (!hex) {
      return -1;
    }
    const rgb: [number, number, number] | undefined = Color.hexToRGB(hex, true);
    return rgb ? Color.rgbToLuminance(rgb) : -1;
  }

  static asHexString(string: string | undefined): string | undefined {
    if (!string) {
      return undefined;
    }
    if (!string.startsWith('#')) {
      return '#' + string;
    }
    return string;
  }

}
