import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[ngVar]'
})
export class NgVarDirective {

  @Input()
  set ngVar(context: unknown) {
    this.context['$implicit'] = context;
    this.context['ngVar'] = context;
    this.updateView();
  }

  private context: unknown = {};

  constructor(private viewContainerRef: ViewContainerRef,
              private templateRef: TemplateRef<unknown>) {
  }

  updateView(): void {
    this.viewContainerRef.clear();
    this.viewContainerRef.createEmbeddedView(this.templateRef, this.context);
  }

}
