import { TestBed } from '@angular/core/testing';

import { CrudStateService } from './crud-state.service';

describe('CrudStateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudStateService = TestBed.get(CrudStateService);
    expect(service).toBeTruthy();
  });
});
