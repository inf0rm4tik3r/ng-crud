import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {concatMap, skipWhile} from 'rxjs/operators';
import {CrudConfig} from './crud-config';
import {CrudConfigService} from './crud-config.service';
import {CrudCondition} from './crud-filter/crud-condition';
import {CrudLoggingService} from './crud-logging.service';
import {Entry, EntryKey, OrderByTypeSet} from './types/crud-types';
import {CrudView} from './types/crud-view';
import {LinkedList} from './types/linked-list';

@Injectable()
export class CrudStateService {

  private crudConfigService: CrudConfigService;

  private readonly oderByOptions: ['asc', 'desc'] = ['asc', 'desc'];

  private readonly _initialized: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public readonly initialized$: Observable<boolean> = this._initialized.asObservable();

  private readonly _active: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public readonly active$: Observable<boolean> = this.awaitInitialization(this._active.asObservable());

  private readonly _fullscreen: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public readonly fullscreen$: Observable<boolean> = this.awaitInitialization(this._fullscreen.asObservable());

  private readonly _view: BehaviorSubject<CrudView> = new BehaviorSubject<CrudView>(CrudView.LIST);
  public readonly view$: Observable<CrudView> = this.awaitInitialization(this._view.asObservable());

  private readonly _activeEntryKey: BehaviorSubject<EntryKey | undefined> = new BehaviorSubject<EntryKey | undefined>(undefined);
  public readonly activeEntryKey$: Observable<EntryKey | undefined> = this.awaitInitialization(this._activeEntryKey.asObservable());

  private readonly _activeEntryKeyVisualization: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public readonly activeEntryKeyVisualization$: Observable<string>
    = this.awaitInitialization(this._activeEntryKeyVisualization.asObservable());

  private readonly _activeEntryVisualization: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public readonly activeEntryVisualization$: Observable<string> = this.awaitInitialization(this._activeEntryVisualization.asObservable());

  private readonly _orderedBy: BehaviorSubject<OrderByTypeSet> = new BehaviorSubject<OrderByTypeSet>({});
  public readonly orderedBy$: Observable<OrderByTypeSet> = this.awaitInitialization(this._orderedBy.asObservable());

  private readonly _userFilter: BehaviorSubject<CrudCondition> = new BehaviorSubject<CrudCondition>(new CrudCondition());
  public readonly userFilter$: Observable<CrudCondition> = this.awaitInitialization(this._userFilter.asObservable());

  private readonly _usePagination: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public readonly usePagination$: Observable<boolean> = this.awaitInitialization(this._usePagination.asObservable());

  private readonly _entriesPerPage: BehaviorSubject<number> = new BehaviorSubject<number>(5);
  public readonly entriesPerPage$: Observable<number> = this.awaitInitialization(this._entriesPerPage.asObservable());

  private readonly _currentPageInList: BehaviorSubject<number> = new BehaviorSubject<number>(1);
  public readonly currentPageInList$: Observable<number> = this.awaitInitialization(this._currentPageInList.asObservable());

  private readonly _recentlyCreated: BehaviorSubject<LinkedList<Entry>> = new BehaviorSubject<LinkedList<Entry>>(new LinkedList<Entry>());
  public readonly recentlyCreatedEntries$: Observable<LinkedList<Entry>> = this.awaitInitialization(this._recentlyCreated.asObservable());

  private readonly _recentlyUpdated: BehaviorSubject<LinkedList<Entry>> = new BehaviorSubject<LinkedList<Entry>>(new LinkedList<Entry>());
  public readonly recentlyUpdatedEntries$: Observable<LinkedList<Entry>> = this.awaitInitialization(this._recentlyUpdated.asObservable());

  private readonly _recentlyActivated: BehaviorSubject<LinkedList<Entry>> = new BehaviorSubject<LinkedList<Entry>>(new LinkedList<Entry>());
  public readonly recentlyActivatedEntries$: Observable<LinkedList<Entry>>
    = this.awaitInitialization(this._recentlyActivated.asObservable());

  constructor(private logger: CrudLoggingService) {
  }

  private awaitInitialization<T>(observable: Observable<T>): Observable<T> {
    return this.initialized$.pipe(
      skipWhile((initialized) => initialized === false),
      concatMap(() => observable)
    );
  }

  initializeFrom(crudConfig: CrudConfig): void {
    // Copy the initial usePagination configuration.
    this.setUsePagination(crudConfig.shouldUsePagination());

    // Copy the initial entriesPerPage configuration.
    this.setEntriesPerPage(crudConfig.getEntriesPerPage());

    // Copy the initial orderBy configuration.
    for (const fieldName of Object.keys(crudConfig.getOrderByColumns())) {
      this.setOrderedBy(fieldName, crudConfig.getOrderByColumns()[fieldName]);
    }

    // Copy the initial userFilter.
    this.setUserFilter(crudConfig.getUserFilter());

    this._initialized.next(true);
  }

  registerConfigService(crudConfigService: CrudConfigService): void {
    this.crudConfigService = crudConfigService;
  }

  setActive(active: boolean): void {
    if (this._active.getValue() !== active) {
      this._active.next(active);
    }
  }

  toggleFullscreen(): void {
    this._fullscreen.next(!this._fullscreen.getValue());
  }

  setFullscreen(value: boolean): void {
    this._fullscreen.next(value);
  }

  hasActiveEntry(): boolean {
    return typeof this._activeEntryKey.getValue() !== typeof undefined;
  }

  getActiveEntryKeyValue(): EntryKey | undefined {
    return this._activeEntryKey.getValue();
  }

  unsetActiveEntry(): void {
    this.setActiveEntry([undefined, undefined]);
  }

  setActiveEntry([entry, entryKey]: [Entry | undefined, EntryKey | undefined]): void {
    this.logger.trace('Activating entry', entry);
    this._activeEntryKey.next(entryKey);
    this._activeEntryKeyVisualization.next(
      this.crudConfigService.config.getKeyVisualizer().apply(this, [entryKey])
    );
    this._activeEntryVisualization.next(
      this.crudConfigService.config.getKeyVisualizer().apply(this, [entry])
    );
    if (entry) {
      this.addRecentlyActivatedEntry(entry);
    }
  }

  setView(view: CrudView): void {
    this._view.next(view);
  }

  private setOrderedBy(fieldName: string, orderType: 'asc' | 'desc', publishChange: boolean = true): void {
    this._orderedBy.getValue()[fieldName] = orderType;
    if (publishChange) {
      this._orderedBy.next(this._orderedBy.getValue());
    }
  }

  private unsetOrderedBy(fieldName: string, publishChange: boolean = true): void {
    delete this._orderedBy.getValue()[fieldName];
    if (publishChange) {
      this._orderedBy.next(this._orderedBy.getValue());
    }
  }

  private resetOrderedByExceptForThis(fieldName: string, publishChange: boolean = true) {
    let updated = false;
    for (const property of Object.keys(this._orderedBy.getValue())) {
      if (property !== fieldName) {
        this.unsetOrderedBy(property, false);
        updated = true;
      }
    }
    // Publish all changes at once, but only next a new value if the data was actually modified!
    if (updated && publishChange) {
      this._orderedBy.next(this._orderedBy.getValue());
    }
  }

  touchOrderedBy(fieldName: string, keepExisting: boolean = false): void {
    if (!keepExisting) {
      this.resetOrderedByExceptForThis(fieldName, false);
    }

    if (!this._orderedBy.getValue()[fieldName]) {
      // Set using first option.
      this.setOrderedBy(fieldName, this.oderByOptions[0], false);
    } else if (this._orderedBy.getValue()[fieldName] === this.oderByOptions[this.oderByOptions.length - 1]) {
      // Unset when order by last option.
      this.unsetOrderedBy(fieldName, false);
    } else {
      // Advance through options.
      for (let i = 0; i < this.oderByOptions.length - 1; i++) {
        if (this._orderedBy.getValue()[fieldName] === this.oderByOptions[i]) {
          this.setOrderedBy(fieldName, this.oderByOptions[i + 1], false);
        }
      }
    }
    // Publish all changes at once!
    this._orderedBy.next(this._orderedBy.getValue());
  }

  setUserFilter(userFilter: CrudCondition): void {
    this._userFilter.next(userFilter);
  }

  setUsePagination(usePagination: boolean): void {
    this._usePagination.next(usePagination);
  }

  setEntriesPerPage(entriesPerPage: number): void {
    this._entriesPerPage.next(entriesPerPage);
  }

  setCurrentPageInList(currentPageInList: number): void {
    this._currentPageInList.next(currentPageInList);
  }

  addRecentlyCreatedEntry(entry: Entry, keep: number = 1000): void {
    this._recentlyCreated.getValue().append(entry);
    this._recentlyCreated.next(this._recentlyCreated.getValue());
    setTimeout(() => {
      if (!this._recentlyCreated.getValue().remove(entry)) {
        this.logger.error('Could not find recently created entry ', entry, 'in LinkedList.');
      }
      this._recentlyCreated.next(this._recentlyCreated.getValue());
    }, keep);
  }

  // The CrudTableBody highlight recently updated rows.
  // The "keep" duration should be 2 times the transition speed of the background-color defined on the updated row!
  addRecentlyUpdatedEntry(entry: Entry, keep: number = 1000): void {
    this._recentlyUpdated.getValue().append(entry);
    this._recentlyUpdated.next(this._recentlyUpdated.getValue());
    setTimeout(() => {
      if (!this._recentlyUpdated.getValue().remove(entry)) {
        this.logger.debug('Could not find recently update entry ', entry, 'in LinkedList. Probably happened due to fast savings.');
      }
      this._recentlyUpdated.next(this._recentlyUpdated.getValue());
    }, keep);
  }

  addRecentlyActivatedEntry(entry: Entry, keep: number = 1000): void {
    this._recentlyActivated.getValue().append(entry);
    this._recentlyActivated.next(this._recentlyActivated.getValue());
    setTimeout(() => {
      if (!this._recentlyActivated.getValue().remove(entry)) {
        this.logger.error('Could not find recently activated entry ', entry, 'in LinkedList.');
      }
      this._recentlyActivated.next(this._recentlyActivated.getValue());
    }, keep);
  }

}
