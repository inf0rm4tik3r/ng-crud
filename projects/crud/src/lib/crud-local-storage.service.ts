import {CrudInstance, CrudInstances} from './types/crud-types';
import {Injectable} from '@angular/core';

export const CRUD_INSTANCES = 'crud-instances';

@Injectable()
export class CrudLocalStorageService {

  private static fromLocalStorage(content: string): CrudInstances {
    return JSON.parse(content);
  }

  private static toLocalStorage(instances: CrudInstances): string {
    return JSON.stringify(instances);
  }

  update(instance: CrudInstance): void {
    const instances: CrudInstances = this.load();
    instances[instance.id] = instance;
    this.store(instances);
  }

  updatePartial(id: string, update: (instance: CrudInstance) => void): void {
    const instances: CrudInstances = this.load();
    const instance = instances[id];
    if (instance) {
      update(instance);
      instances[id] = instance;
    }
    else {
      const newInstance = {
        id,
        active: false
      };
      update(newInstance);
      instances[id] = newInstance;
    }
    this.store(instances);
  }

  load(noRetry: boolean = false): CrudInstances | undefined {
    /*
     * "crud-instances": ["1": {active: true}, "2": {active: false}]
     */

    const data: string | null = localStorage.getItem(CRUD_INSTANCES);

    // If "crud-instances" data is available then return it.
    if (data != null) {
      return CrudLocalStorageService.fromLocalStorage(data);
    }

    if (noRetry === true) {
      console.error('Could not load "crud-instances" from local storage. Storage might not be accessible.');
      return undefined;
    }

    // Otherwise store an initial empty array and try again.
    localStorage.setItem(CRUD_INSTANCES, CrudLocalStorageService.toLocalStorage({}));

    return this.load(true);
  }

  store(instances: CrudInstances): void {
    // console.log('Storing:', instances);
    localStorage.setItem(CRUD_INSTANCES, CrudLocalStorageService.toLocalStorage(instances));
  }

}
