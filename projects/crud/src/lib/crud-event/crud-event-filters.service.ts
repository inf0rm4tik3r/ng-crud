import {ElementRef, Injectable} from '@angular/core';
import {CrudElementEventFiltersService} from './crud-element-event-filters.service';
import {CrudEventFilters} from './crud-event-filters';

@Injectable()
export class CrudEventFiltersService {

  constructor(private elementRef: ElementRef,
              private crudElementEventFiltersService: CrudElementEventFiltersService) {
  }

  ngOnDestroy(): void {
    this.crudElementEventFiltersService.unregister(this.elementRef.nativeElement);
  }

  register(filters: CrudEventFilters): void {
    this.crudElementEventFiltersService.register(this.elementRef.nativeElement, filters);
  }

}
