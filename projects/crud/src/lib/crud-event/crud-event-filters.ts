import {CrudEventFilter} from './crud-event-filter';

// Mapping event names to filters.
export type CrudEventFilters = { [key: string]: CrudEventFilter };
