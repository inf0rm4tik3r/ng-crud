import {NgModule} from '@angular/core';
import {EVENT_MANAGER_PLUGINS} from '@angular/platform-browser';
import {CrudElementEventFiltersService} from './crud-element-event-filters.service';
import {CrudEventFiltersDirective} from './crud-event-filters.directive';
import {CrudEventFiltersService} from './crud-event-filters.service';
import {CrudFilteredEventPlugin} from './crud-filtered-event.plugin';

@NgModule({
  imports: [],
  declarations: [
    CrudEventFiltersDirective
  ],
  exports: [
    CrudEventFiltersDirective
  ],
  providers: [
    CrudElementEventFiltersService,
    CrudEventFiltersService,
    {
      provide: EVENT_MANAGER_PLUGINS,
      useClass: CrudFilteredEventPlugin,
      multi: true
    }
  ]
})
export class CrudEventModule {
}
