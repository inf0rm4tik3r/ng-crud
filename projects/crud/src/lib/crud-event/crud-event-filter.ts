export type CrudEventFilter = (event: Event) => boolean;
