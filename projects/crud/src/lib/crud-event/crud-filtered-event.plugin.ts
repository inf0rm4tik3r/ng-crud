/* eslint-disable @typescript-eslint/ban-types */
import {Injectable} from '@angular/core';
import {EventManager} from '@angular/platform-browser';
import {CrudElementEventFiltersService} from './crud-element-event-filters.service';
import {CrudEventFilter} from './crud-event-filter';
// This is not a public API, so we cannot extend it...
// import {EventManagerPlugin} from '@angular/platform-browser/src/dom/events/event_manager';

export const FILTERED = 'filter';
export const PREVENT = 'prevent';
export const STOP = 'stop';
export const SUPPORTED_ZONES = [];
export const SUPPORTED_OPTIONS = [FILTERED, PREVENT, STOP];
export const GLOBAL_NOT_SUPPORTED = 'Filtering global events is not supported, delegated handling to default plugin.';

interface EventParts {
  zone: string[], // [document], [window]
  event: string, // keydown
  options: string[], // [enter], [enter, filter, stop, prevent]
}

// NFO: A subject to change: https://github.com/angular/angular/issues/3929
// noinspection JSUnusedGlobalSymbols
@Injectable()
export class CrudFilteredEventPlugin { // extends EventManagerPlugin {

  manager: EventManager;

  constructor(private filteredEventService: CrudElementEventFiltersService) {
  }

  /*
   * For Example: A typical name could be: 'window:keydown.enter.filter.stop.prevent'
   */
  private static eventNameToParts(eventName: string): EventParts {
    const splitAt: number = eventName.lastIndexOf(':');
    const of: string = eventName.substr(0, splitAt);
    const eventWithOptions: string[] = eventName.substr(splitAt + 1).split('.');
    return {
      zone: of.split(':'),
      event: eventWithOptions[0],
      options: eventWithOptions.slice(1)
    };
  }

  private static eventPartsToActualEventName(eventParts: EventParts): string {
    const zone = eventParts.zone
      .filter(modifier => SUPPORTED_ZONES.indexOf(modifier) === -1)
      .join(':');
    const options = eventParts.options
      .filter(modifier => SUPPORTED_OPTIONS.indexOf(modifier) === -1)
      .join('.');
    return zone + (zone.length > 0 ? ':' : '') + eventParts.event + (options.length > 0 ? '.' : '') + options;
  }

  supports(eventName: string): boolean {
    const eventParts: EventParts = CrudFilteredEventPlugin.eventNameToParts(eventName);
    return eventParts.options.indexOf(FILTERED) !== -1 ||
      eventParts.options.indexOf(PREVENT) !== -1 ||
      eventParts.options.indexOf(STOP) !== -1;
  }

  addEventListener(element: HTMLElement, eventName: string, handler: Function): Function {
    const {manager} = this;
    const zone = manager.getZone();

    const eventParts: EventParts = CrudFilteredEventPlugin.eventNameToParts(eventName);
    const actualEventName: string = CrudFilteredEventPlugin.eventPartsToActualEventName(eventParts);
    //console.log('eventName', eventName);
    //console.log('actualEventName', actualEventName);

    const filtered = (event: Event) => {
      const filter: CrudEventFilter = this.filteredEventService.getFilter(element, actualEventName);
      if (eventParts.options.indexOf(FILTERED) === -1 || !filter || filter(event)) {
        if (eventParts.options.indexOf(PREVENT) !== -1) {
          event.preventDefault();
        }
        if (eventParts.options.indexOf(STOP) !== -1) {
          event.stopPropagation();
        }
        zone.run(() => handler(event));
      }
    };
    return zone.runOutsideAngular(() => manager.addEventListener(element, actualEventName, filtered));
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  addGlobalEventListener(element: string, eventName: string, handler: Function): Function {
    console.warn(GLOBAL_NOT_SUPPORTED);
    const eventParts: EventParts = CrudFilteredEventPlugin.eventNameToParts(eventName);
    const actualEventName: string = CrudFilteredEventPlugin.eventPartsToActualEventName(eventParts);
    return this.manager.addGlobalEventListener(element, actualEventName, handler);
  }

}
