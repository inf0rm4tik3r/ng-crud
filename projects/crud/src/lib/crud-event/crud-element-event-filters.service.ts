import {Injectable} from '@angular/core';
import {CrudEventFilter} from './crud-event-filter';
import {CrudEventFilters} from './crud-event-filters';

@Injectable()
export class CrudElementEventFiltersService {

  private elementFilters: Map<Element, CrudEventFilters> = new Map();

  register(element: Element, filters: CrudEventFilters): void {
    this.elementFilters.set(element, filters);
  }

  unregister(element: Element): void {
    this.elementFilters.delete(element);
  }

  getFilter(element: Element, event: string): CrudEventFilter | null {
    const filters: CrudEventFilters = this.elementFilters.get(element);
    return filters ? filters[event] || null : null;
  }

}
