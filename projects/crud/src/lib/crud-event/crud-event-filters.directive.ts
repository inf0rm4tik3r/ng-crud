import {Directive, ElementRef, Input, OnDestroy, Optional, Self} from '@angular/core';
import {CrudEventFilters} from './crud-event-filters';
import {CrudEventFiltersService} from './crud-event-filters.service';
import {CrudElementEventFiltersService} from './crud-element-event-filters.service';

const ALREADY_APPLIED = `
    Component you are trying to use eventFilters on already has filters applied inside,
    to avoid conflicts you cannot use directive directly on this component's host. You
    need to wrap it in another HTML element and apply directive on the wrapper.
`;

@Directive({
  selector: '[eventFilters]'
})
export class CrudEventFiltersDirective implements OnDestroy {

  @Input()
  set eventFilters(filters: CrudEventFilters) {
    if (this.crudEventFiltersService) {
      console.warn(ALREADY_APPLIED);
      return;
    }
    this.crudFilteredEventService.register(this.elementRef.nativeElement, filters);
  }

  constructor(private elementRef: ElementRef,
              private crudFilteredEventService: CrudElementEventFiltersService,
              @Optional()
              @Self()
              private crudEventFiltersService: CrudEventFiltersService | null) {
  }

  ngOnDestroy(): void {
    this.crudFilteredEventService.unregister(this.elementRef.nativeElement);
  }

}
