import { TestBed } from '@angular/core/testing';

import { CrudAlertService } from './crud-alert.service';

describe('CrudAlertService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudAlertService = TestBed.get(CrudAlertService);
    expect(service).toBeTruthy();
  });
});
