import {CrudDataStructure} from '../crud-data/crud-data-structure';
import {CrudDataType} from '../crud-data/crud-data-type';
import {DataStructure, Entries} from '../types/crud-types';

export class CrudTestData {

  static readonly newTestStructure: CrudDataStructure = new CrudDataStructure(
    {
      name: 'id',
      type: CrudDataType.int,
      nullable: false,
      defaultValue: undefined,
      key: true,
      autoIncrement: true
    }
  );

  static readonly testDataStructure: DataStructure = [
    {
      name: 'id',
      type: 'int',
      nullable: false,
      key: true,
      autoIncrement: true
    },
    {
      name: 'name',
      type: 'string',
      nullable: false,
      key: false,
      autoIncrement: false,
      constraints: {
        maxLength: 100
      }
    },
    {
      name: 'meta',
      type: 'string',
      nullable: false,
      key: false,
      autoIncrement: false,
      constraints: {
        maxLength: 100
      }
    },
    {
      name: 'value',
      type: 'float',
      nullable: false,
      key: false,
      autoIncrement: false
    },
    {
      name: 'color',
      type: 'color',
      nullable: false,
      key: false,
      autoIncrement: false
    },
    {
      name: 'relation_id',
      type: 'int',
      nullable: true,
      key: false,
      autoIncrement: false
    }
  ];

  static readonly testData: Entries = [
    {id: 1, name: 'Lorem', meta: 'm', value: 1.23, color: '#f14c4c', relation_id: 2},
    {id: 2, name: 'ipsum', meta: 'm', value: 1.124, color: 'wlkfe', relation_id: 1},
    {id: 3, name: 'dolor', meta: 'm', value: 31.2, color: '#e0b101', relation_id: null},
    {id: 4, name: 'sit', meta: 'm', value: -31.42, color: '#8c8484', relation_id: 1},
    {id: 5, name: 'xylophon', meta: 'm', value: 74.6, color: '#2b0a0a', relation_id: null},
    {id: 6, name: 'foo', meta: 'm', value: 7.28, color: '#ff00d5', relation_id: 3},
    {id: 7, name: 'zebra', meta: 'm', value: 5.91, color: '#ffae97', relation_id: 2},
    {id: 8, name: 'amet', meta: 'm', value: 41.6, color: '#48dbd2', relation_id: null},
    {id: 9, name: 'foo', meta: 'm', value: 7.28, color: '#ffef12', relation_id: null},
    {id: 10, name: 'bar', meta: 'm', value: 5.91, color: '#165f09', relation_id: 1},
    {id: 11, name: 'rino', meta: 'm', value: 99.6, color: '#ff0000', relation_id: 3},
    {id: 12, name: 'foo', meta: 'm', value: 7.28, color: '#000000', relation_id: 2},
    {id: 13, name: 'bar', meta: 'm', value: 5.91, color: '#ffffff', relation_id: null}
  ];

  static readonly relationDataStructure: DataStructure = [
    {
      name: 'id',
      type: 'int',
      nullable: false,
      key: true,
      autoIncrement: true
    },
    {
      name: 'name',
      type: 'string',
      nullable: false,
      key: false,
      autoIncrement: false,
      constraints: {
        maxLength: 100
      }
    }
  ];

  static readonly relationData: Entries = [
    {id: 1, name: 'A'},
    {id: 2, name: 'B'},
    {id: 3, name: 'C'}
  ];


  static readonly nestedDataStructure: DataStructure = [
    {
      name: 'id',
      type: 'int',
      nullable: false,
      key: true,
      autoIncrement: true
    },
    {
      name: 'ref_id',
      type: 'int',
      nullable: false,
      key: false,
      autoIncrement: false
    },
    {
      name: 'comment',
      type: 'string',
      nullable: false,
      key: false,
      autoIncrement: false,
      constraints: {
        maxLength: 1024
      }
    }
  ];

  static readonly nestedData: Entries = [
    {id: 1, ref_id: 1, comment: 'Hallo'},
    {id: 2, ref_id: 1, comment: 'Hallo'},
    {id: 3, ref_id: 2, comment: 'FOOBAR'}
  ];

}
