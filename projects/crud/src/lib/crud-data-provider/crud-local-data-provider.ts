import {Observable} from 'rxjs';
import {CrudDataStore} from '../crud-data-store/crud-data-store';
import {CrudDataStoreEntry} from '../crud-data-store/crud-data-store-entry';
import {CrudCondition} from '../crud-filter/crud-condition';
import {CrudConditionClause} from '../crud-filter/crud-condition-clause';
import {CrudConditionOperator} from '../crud-filter/crud-condition-operator';
import {CrudIdService} from '../crud-id.service';
import {CrudLoggingService, LogLevel} from '../crud-logging.service';
import {DataStructure, Entries, Entry, EntryKey, EntryUpdate, OrderByTypeSet} from '../types/crud-types';
import {CrudDataProvider} from './crud-data-provider';

export class CrudLocalDataProvider implements CrudDataProvider {

  private readonly dataStructureProvider: () => DataStructure;
  private readonly dataProvider: () => Entries;
  private readonly delay: number;
  private dataStructure: DataStructure;
  private store: CrudDataStore;

  private idService: CrudIdService;
  private logger: CrudLoggingService;

  constructor(dataStructureProvider: () => DataStructure,
              dataProvider: () => Entries,
              name: string = 'local-data-provider',
              delay: number = 100) {
    this.logger = new CrudLoggingService().init(name, LogLevel.WARN);
    this.idService = new CrudIdService();
    this.dataStructureProvider = dataStructureProvider;
    this.dataProvider = dataProvider;
    this.delay = delay;
    this.restore();
  }

  private static hasKey(entry: Entry, key: EntryKey) {
    for (const k of Object.keys(key)) {
      if (entry[k] !== key[k]) {
        return false;
      }
    }
    return true;
  }

  getLogger(): CrudLoggingService {
    return this.logger;
  }

  setLogger(logger: CrudLoggingService | undefined): CrudLocalDataProvider {
    this.logger = logger;
    return this;
  }

  setLogName(name: string): CrudLocalDataProvider {
    this.logger.setName(name);
    return this;
  }

  setLogLevel(logLevel: LogLevel): CrudLocalDataProvider {
    this.logger.setLogLevel(logLevel);
    return this;
  }

  restore(): void {
    this.dataStructure = JSON.parse(JSON.stringify(this.dataStructureProvider()));

    // Create the store.
    this.store = new CrudDataStore(this.idService.uuid(), this.dataStructureProvider());

    const entries: Entries = JSON.parse(JSON.stringify(this.dataProvider()));
    // And fill it with data.
    for (const entry of entries) {
      this.store.data.insert({data: entry, $loki: undefined, meta: undefined});
    }
  }

  getStructure(): Observable<DataStructure> {
    return new Observable<DataStructure>(observer => {
      setTimeout(() => {
        this.logger.trace('getStructure', this.dataStructure);
        observer.next(Object.assign([], this.dataStructure));
        observer.complete();
      }, this.delay);
    });
  }

  private getFilteredAndOrdered(condition: CrudCondition | undefined,
                                orderedBy: OrderByTypeSet): Entries {
    return this.getOrdered(this.getFiltered(condition), orderedBy);
  }

  private getFiltered(condition: CrudCondition | undefined): Entries {
    if (condition) {
      return this.store.data
        .where((dsEntry: CrudDataStoreEntry) => this.applyCondition(dsEntry.data, condition))
        .map((dsEntry) => dsEntry.data);
    } else {
      return this.store.data.where(() => true)
        .map((dsEntry) => dsEntry.data);
    }
  }

  /**
   * Tests if the entry meets the criteria in the given condition.
   */
  private applyCondition(entry: Entry, condition: CrudCondition): boolean {
    let keep = true;
    let lastOp: string | undefined = CrudConditionOperator.AND;
    for (const queryElem of condition.getQuery()) {
      if (queryElem instanceof CrudConditionClause) {
        keep = keep && this.applyClause(entry, queryElem);
      } else if (queryElem instanceof CrudConditionOperator) {
        if (queryElem.operator === CrudConditionOperator.AND) {
          lastOp = CrudConditionOperator.AND;
        } else if (queryElem.operator === CrudConditionOperator.OR) {
          lastOp = CrudConditionOperator.OR;
        } else {
          throw new Error('Unknown CrudConditionOperator: ' + queryElem.operator);
        }
      } else if (queryElem instanceof CrudCondition) {
        if (lastOp === CrudConditionOperator.AND) {
          keep = keep && this.applyCondition(entry, queryElem);
        } else if (lastOp === CrudConditionOperator.OR) {
          keep = keep || this.applyCondition(entry, queryElem);
        } else {
          throw new Error('Unexpected lastOp: ' + lastOp);
        }
      } else {
        this.logger.error('queryElem:', queryElem);
        throw new Error('Unexpected query element: ' + typeof queryElem);
      }
    }
    return keep;
  }

  /**
   * @return True if the entry meets the condition clause.
   */
  private applyClause(entry: Entry, clause: CrudConditionClause): boolean {
    const value = entry[clause.columnName];
    if (!value) {
      this.logger.warn('Operating on:', value);
      return true;
    }
    // TODO: add more operators.
    switch (clause.operator) {
      case '=':
        return value == clause.value;
      case '!=':
        return value != clause.value;
      case '<':
        return value < clause.value;
      case '<=':
        return value <= clause.value;
      case '>':
        return value > clause.value;
      case '>=':
        return value >= clause.value;
      case 'CONTAINS':
        return (value as string).includes(clause.value as string);
      case 'STARTS_WITH':
        return (value as string).startsWith(clause.value as string);
      case 'ENDS_WITH':
        return (value as string).endsWith(clause.value as string);
    }
    this.logger.error('Operator ' + clause.operator + ' needs implementation!');
    return true;
  }

  private getOrdered(data: Entries, orderedBy: OrderByTypeSet): Entries {
    return [].concat(data).sort((a, b) => {
      return CrudLocalDataProvider.compareAll(a, b, orderedBy);
    });
  }

  private static compareAll(a: Entry, b: Entry, orderedBy: OrderByTypeSet): number {
    // Order the data in order of appearance of the field names.
    let comp = 0;
    for (const fieldName of Object.keys(orderedBy)) {
      const singleCompare = CrudLocalDataProvider.compare(a, b, fieldName);
      if (comp === 0) {
        comp = orderedBy[fieldName] === 'asc' ? (comp || singleCompare) : (comp || -singleCompare);
      }
    }
    return comp;
  }

  // TODO: receive data structure to access type information.
  private static compare(a: Entry, b: Entry, fieldName: string): number {
    const aVal: unknown = a[fieldName];
    const bVal: unknown = b[fieldName];
    const type: unknown = typeof aVal !== typeof undefined ? typeof aVal : typeof bVal;

    switch (type) {
      case 'number':
        return (aVal as number) - (bVal as number);
      case 'string':
        return (aVal as string).localeCompare((bVal as string));

      // TODO: Add cases..
      default:
        return (aVal as number) - (bVal as number);
    }
  }

  // TODO: Allow arbitrary keys!
  add(newEntry: Entry): Observable<[EntryKey, Entry]> {
    return new Observable<[EntryKey, Entry]>(observer => {
      setTimeout(() => {
        this.logger.trace('add', newEntry);

        // Build up the key of the new entry so that it can be returned to the caller.
        const newKey: EntryKey = {};

        // "Key" fields must either have a value or must be declared as autoincrement!
        for (const typeDef of this.dataStructure) {
          if (typeDef.key === true) {

            // Non autoincrement key field without a value -> warn.
            if (typeof newEntry[typeDef.name] === typeof undefined
              && !typeDef.autoIncrement === true) {
              const error = [typeDef, 'is a key field but no data was given.'];
              this.logger.warn(...error);
            }

            // Key and autoincrement -> generate new value and overwrite given data.
            if (typeDef.autoIncrement === true) {
              // Assert that autoincrement is only used on number fields.
              newEntry[typeDef.name] = this.store.data.max('data.' + typeDef.name) + 1;
              this.logger.trace(
                '"' + typeDef.name, '" with autoincrement = true was deduced to be', newEntry[typeDef.name]);
            }

            // Store key value.
            newKey[typeDef.name] = newEntry[typeDef.name];
          }
        }

        this.logger.trace('add', newEntry);
        this.store.data.insert({data: newEntry, $loki: undefined, meta: undefined});
        observer.next([newKey, newEntry]);
        observer.complete();
      }, this.delay);
    });
  }

  delete(key: EntryKey): Observable<void> {
    return new Observable<void>(observer => {
      setTimeout(() => {
        this.store.data.removeWhere((entry: CrudDataStoreEntry) => CrudLocalDataProvider.hasKey(entry.data, key));
        observer.next();
        observer.complete();
      }, this.delay);
    });
  }

  get(limit: number,
      skip: number,
      orderedBy: OrderByTypeSet,
      condition: CrudCondition | undefined): Observable<Entries> {
    return new Observable<Entries>(observer => {
      setTimeout(() => {
        const data = this.getFilteredAndOrdered(condition, orderedBy);
        const result: Entries = [];
        // TODO: Performance: Use limit and skip when accessing the store!
        for (let i = skip; i < Math.min(data.length, skip + limit); i++) {
          result.push(data[i]);
        }
        observer.next(result);
        observer.complete();
      }, this.delay);
    });
  }

  getAll(orderedBy: OrderByTypeSet,
         condition: CrudCondition | undefined): Observable<Entries> {
    return new Observable<Entries>(observer => {
      setTimeout(() => {
        observer.next(this.getFilteredAndOrdered(condition, orderedBy));
        observer.complete();
      }, this.delay);
    });
  }

  getCount(condition: CrudCondition | undefined): Observable<number> {
    return new Observable<number>(observer => {
      setTimeout(() => {
        observer.next(this.getFiltered(condition).length);
        observer.complete();
      }, this.delay);
    });
  }

  getOne(key: EntryKey): Observable<Entry | undefined> {
    return new Observable<Entry>(observer => {
      setTimeout(() => {
        this.logger.trace('getOne', key);
        const foundEntries: Entry[] = this.store.data
          .where((entry: CrudDataStoreEntry) => CrudLocalDataProvider.hasKey(entry.data, key))
          .map((dsEntry: CrudDataStoreEntry) => dsEntry.data);
        this.logger.trace('getOne found', foundEntries, 'returning first');
        if (foundEntries.length !== 0) {
          observer.next(foundEntries[0]);
        } else {
          const error = ['getOne found nothing for key: ', key];
          this.logger.warn(...error);
          observer.next(undefined);
        }
        observer.complete();
      }, this.delay);
    });
  }

  updateOne(key: EntryKey, update: EntryUpdate): Observable<Entry | undefined> {
    return new Observable<Entry>(observer => {
      setTimeout(() => {
        let updated: Entry = undefined;
        this.store.data.updateWhere(
          (entry: CrudDataStoreEntry) => CrudLocalDataProvider.hasKey(entry.data, key),
          (entry: CrudDataStoreEntry) => {
            // Update each necessary field.
            for (const k of Object.keys(update)) {
              entry.data[k] = update[k];
            }
            updated = entry.data;
            return entry;
          }
        );
        observer.next(updated);
        observer.complete();
      }, this.delay);
    });
  }

}
