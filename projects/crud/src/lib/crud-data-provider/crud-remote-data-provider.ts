/* eslint-disable */
import {Observable} from 'rxjs';
import {CrudLoggingService} from '../crud-logging.service';
import {DataStructure, Entries, Entry, EntryKey} from '../types/crud-types';
import {CrudDataProvider} from './crud-data-provider';

export class CrudRemoteDataProvider implements CrudDataProvider {

  private _apiUrl: string;
  private logger: CrudLoggingService;

  constructor(apiUrl: string) {
    this._apiUrl = apiUrl;
  }

  setLogger(logger: CrudLoggingService | undefined): void {
    this.logger = logger;
  }

  add(newEntry: Entry): Observable<[EntryKey, Entry]> {
    throw new Error('Not jet implemented.');
  }

  delete(key: EntryKey): Observable<void> {
    throw new Error('Not jet implemented.');
  }

  get(limit: number, skip: number): Observable<Entries> {
    throw new Error('Not jet implemented.');
  }

  getAll(): Observable<Entries> {
    throw new Error('Not jet implemented.');
  }

  getCount(): Observable<number> {
    throw new Error('Not jet implemented.');
  }

  getOne(key: EntryKey): Observable<Entry> {
    throw new Error('Not jet implemented.');
  }

  getStructure(): Observable<DataStructure> {
    throw new Error('Not jet implemented.');
  }

  restore(): void {
    throw new Error('Not jet implemented.');
  }

  updateOne(key: EntryKey, newData: Entry): Observable<Entry | undefined> {
    throw new Error('Not jet implemented.');
  }

}
