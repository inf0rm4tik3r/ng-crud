import {BehaviorSubject, Observable, of} from 'rxjs';
import {concatMap, skipWhile, take, tap} from 'rxjs/operators';
import {CrudDataStore} from '../crud-data-store/crud-data-store';
import {CrudDataStoreEntry} from '../crud-data-store/crud-data-store-entry';
import {CrudCondition} from '../crud-filter/crud-condition';
import {CrudLoggingService} from '../crud-logging.service';
import {timestamp} from '../crud-util';
import {DataStructure, Entries, Entry, EntryKey, OrderByTypeSet} from '../types/crud-types';
import {CrudDataProvider} from './crud-data-provider';

/**
 * This class acts as a facade for a data provider.
 * Contains a store which is able to cache the data given by the data provider.
 * This allows the application to access data rapidly without having to request the data from the actual data provider.
 */
export class CrudDataProviderFacade implements CrudDataProvider {

  // TODO: Add logic to filter, sort and select locally if necessary.

  private readonly id: string;
  private readonly dataProvider: CrudDataProvider;

  init$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  structure$: BehaviorSubject<DataStructure> = new BehaviorSubject<DataStructure>(undefined);

  store: CrudDataStore | undefined = undefined;

  lastGetAllCall: number = 0;
  lastGetAllCallFinished$: BehaviorSubject<boolean> = new BehaviorSubject(true);

  logger: CrudLoggingService | undefined = undefined;

  constructor(id: string,
              dataProvider: CrudDataProvider) {
    this.id = id;
    this.dataProvider = dataProvider;
    this.dataProvider.getStructure().pipe(take(1)).subscribe((structure) => {
      // setTimeout(() => {

      // We store the structure of the data for later use without the need to call the actual data provider again.
      // -> The structure should not change over the lifespan of a provider facade.
      this.structure$.next(structure);

      // A store is created in which data from the data provider can be cached.
      this.store = new CrudDataStore(this.id, structure);

      // Pushing a true value to the init$ subject sets this provider facet to be "initialized".
      // Calls to other methods will no longer be halted.
      this.init$.next(true);

      // }, 0); // Artificial store construction delay.
    });
  }

  private awaitInitialization<T>(observable: Observable<T>): Observable<T> {
    return this.init$.pipe(
      skipWhile((initialized) => initialized === false),
      concatMap(() => observable)
    );
  }

  setLogger(logger: CrudLoggingService | undefined): void {
    this.logger = logger;
  }

  getStructure(): Observable<DataStructure> {
    return this.awaitInitialization(
      this.structure$.asObservable()
    );
  }

  getCount(condition: CrudCondition | undefined): Observable<number> {
    return this.awaitInitialization(
      this.dataProvider.getCount(condition)
    );
  }

  private awaitLastGetAll<T>(observableProvider: () => Observable<T>): Observable<T> {
    return this.lastGetAllCallFinished$.pipe(
      skipWhile((finished) => finished === false),
      concatMap(() => observableProvider())
    );
  }

  getAll(orderedBy: OrderByTypeSet, condition: CrudCondition | undefined, cache: number = 0): Observable<Entries> {
    const now = timestamp();

    // Reload if the cached data is too old and the last request finished.
    // Last part of the condition is important. If multiple fast calls to this method are made,
    // only the first call should trigger the actual request. Later calls should 'wait' for a cached result.
    if (now > this.lastGetAllCall + cache && this.lastGetAllCallFinished$.getValue()) {
      // console.log('get');
      this.lastGetAllCall = now;
      this.lastGetAllCallFinished$.next(false);

      // Always wait for the provider to be fully initialized before issuing a call.
      // Load the data and cache it.
      const req = this.dataProvider.getAll(orderedBy, condition)
        .pipe(
          tap((all: Entry[]) => {
            this.store.data.clear();
            for (const entry of all) {
              this.store.data.insert({data: entry, $loki: undefined, meta: undefined});
            }
          })
        );
      req.pipe(take(1)).subscribe(() => {
          this.lastGetAllCallFinished$.next(true);
        }, () => {
          this.lastGetAllCallFinished$.next(true);
        }, () => {
          this.lastGetAllCallFinished$.next(true);
        }
      );
      return this.awaitInitialization(req);
    }

    // Wait for an ongoing request to finish. Then send the cached data.
    // console.log('cache', now > this.lastGetAllCall + cache, this.lastGetAllCallFinished$.getValue());
    return this.awaitLastGetAll(
      () => of(this.store.data
        .where((_: CrudDataStoreEntry) => true)
        .map((dsEntry: CrudDataStoreEntry) => dsEntry.data))
    );
  }

  get(limit: number,
      skip: number,
      orderedBy: OrderByTypeSet,
      condition: CrudCondition | undefined): Observable<Entries> {
    return this.awaitInitialization(
      this.dataProvider.get(limit, skip, orderedBy, condition)
    );
  }

  getOne(key: EntryKey): Observable<Entry | undefined> {
    return this.awaitInitialization(
      this.dataProvider.getOne(key)
    );
  }

  add(newEntry: Entry): Observable<[EntryKey, Entry]> {
    return this.awaitInitialization(
      this.dataProvider.add(newEntry)
    );
  }

  delete(key: EntryKey): Observable<void> {
    return this.awaitInitialization(
      this.dataProvider.delete(key)
    );
  }

  updateOne(key: EntryKey, newData: Entry): Observable<Entry> {
    return this.awaitInitialization(
      this.dataProvider.updateOne(key, newData)
    );
  }

  restore(): void {
    this.dataProvider.restore();
  }

}
