import {Observable} from 'rxjs';
import {CrudCondition} from '../crud-filter/crud-condition';
import {CrudLoggingService} from '../crud-logging.service';
import {DataStructure, Entries, Entry, EntryKey, OrderByTypeSet} from '../types/crud-types';

export interface CrudDataProvider {

  setLogger(logger: CrudLoggingService | undefined): void;

  getStructure(): Observable<DataStructure>;

  getCount(
    condition: CrudCondition | undefined
  ): Observable<number>;

  getAll(
    orderedBy: OrderByTypeSet,
    condition: CrudCondition | undefined
  ): Observable<Entries>;

  get(
    limit: number,
    skip: number,
    orderedBy: OrderByTypeSet,
    condition: CrudCondition | undefined
  ): Observable<Entries>;

  // TODO: Add filter
  getOne(key: EntryKey): Observable<Entry | undefined>;

  add(newEntry: Entry): Observable<[EntryKey, Entry]>;

  // TODO: Add filter
  delete(key: EntryKey): Observable<void>;

  // TODO: Add filter
  updateOne(key: EntryKey, newData: Entry): Observable<Entry | undefined>;

  restore(): void;

}
