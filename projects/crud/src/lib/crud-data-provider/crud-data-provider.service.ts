import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {CrudConfigService} from '../crud-config.service';
import {CrudCondition} from '../crud-filter/crud-condition';
import {CrudInstanceService} from '../crud-instance/crud-instance-service';
import {CrudLoggingService} from '../crud-logging.service';
import {CrudStateService} from '../crud-state.service';
import {DataStructure, Entries, Entry, EntryKey, OrderByTypeSet} from '../types/crud-types';
import {CrudDataProvider} from './crud-data-provider';
import {CrudDataProviderFacade} from './crud-data-provider-facade';

/**
 * This service is used whenever we want to work with data.
 * // TODO: Rename to CrudDataService? We do not really care, that it uses the CrudDataProvider through a facade in the background..
 */
@Injectable()
export class CrudDataProviderService implements CrudDataProvider {

  private facade: CrudDataProviderFacade;

  private relationFacades: { [fieldName: string]: CrudDataProviderFacade } = {};

  constructor(private crudInstanceService: CrudInstanceService,
              private crudConfigService: CrudConfigService,
              private crudStateService: CrudStateService,
              private logger: CrudLoggingService) {
  }

  initFacades(): void {
    this.facade = new CrudDataProviderFacade(
      this.crudInstanceService.id,
      this.crudConfigService.config.getDataProvider()
    );

    for (const fieldName of Object.keys(this.crudConfigService.config.getRelations())) {
      const relation = this.crudConfigService.config.getRelations()[fieldName];
      this.relationFacades[fieldName] = new CrudDataProviderFacade(
        this.crudInstanceService.id + fieldName,
        relation.dataProvider
      );
    }
  }

  relation(fieldName: string): CrudDataProviderFacade {
    const relationFacade = this.relationFacades[fieldName];
    if (relationFacade) {
      return relationFacade;
    }
    throw new Error('No data provider facade exists for relation field "' + fieldName + '".');
  }

  /**
   * Extract the key data from an entry.
   * @param entry The entry from which the key data should be returned.
   */
  getKey(entry: Entry): EntryKey {
    const key = {};
    for (const typeDef of this.facade.structure$.getValue()) {
      if (typeDef.key === true) {
        key[typeDef.name] = entry[typeDef.name];
      }
    }
    return key;
  }

  setLogger(_: CrudLoggingService | undefined): void {
    // Intentionally do nothing.
    this.logger.error('Method should not have been called.');
  }

  getStructure(): Observable<DataStructure> {
    return this.facade.getStructure();
  }

  getCount(condition: CrudCondition | undefined): Observable<number> {
    return this.facade.getCount(condition);
  }

  private computeParentConstraint(): CrudCondition | undefined {
    const parent = this.crudInstanceService.instance.crudInputService.parent;
    if (!!parent && parent.crudStateService.hasActiveEntry()) {
      // Getting the value is ok, a changing key will be caught later up the stack.
      const parentActiveEntryKey = parent.crudStateService.getActiveEntryKeyValue();
      const nestedInstanceConfig = this.crudInstanceService.instance.crudInputService.nestedInstanceConfig;
      const referencedFieldName = nestedInstanceConfig.referencedFieldName;
      const referenceFieldName = nestedInstanceConfig.referenceFieldName;
      return new CrudCondition().where(
        referenceFieldName,
        '=',
        parentActiveEntryKey[referencedFieldName]
        // 'parent-constraint'
      );
    }
    return undefined;
  }

  getAll(orderedBy: OrderByTypeSet, condition: CrudCondition | undefined): Observable<Entries> {
    const parentConstraint = this.computeParentConstraint();
    if (parentConstraint) {
      this.logger.trace('getAll with parent constraint', ...parentConstraint.getQuery());
      condition = condition ? CrudCondition.combine(condition, parentConstraint) : parentConstraint;
    }
    return this.facade.getAll(orderedBy, condition);
  }

  get(limit: number,
      skip: number,
      orderedBy: OrderByTypeSet,
      condition: CrudCondition | undefined): Observable<Entries> {
    const parentConstraint = this.computeParentConstraint();
    if (parentConstraint) {
      this.logger.trace('get with parent constraint', ...parentConstraint.getQuery());
      condition = condition ? CrudCondition.combine(condition, parentConstraint) : parentConstraint;
      this.logger.trace('final condition', condition);
    }
    return this.facade.get(limit, skip, orderedBy, condition);
  }

  // TODO: Add condition? Entry may not be found then.
  getOne(key: EntryKey): Observable<Entry | undefined> {
    return this.facade.getOne(key);
  }

  add(newEntry: Entry): Observable<[EntryKey, Entry]> {
    /*
     * Ensure that parent constraints are met!
     * If an entry in a nested instance is added, we should have a parent instance with an active entry.
     * The new entry must reference the parents active entry!
     */
    const parent = this.crudInstanceService.instance.crudInputService.parent;
    if (!!parent && parent.crudStateService.hasActiveEntry()) {
      // Getting the value is ok, a changing key will be caught later up the stack.
      const parentActiveEntryKey = parent.crudStateService.getActiveEntryKeyValue();
      const nestedInstanceConfig = this.crudInstanceService.instance.crudInputService.nestedInstanceConfig;
      const referencedFieldName = nestedInstanceConfig.referencedFieldName;
      const referenceFieldName = nestedInstanceConfig.referenceFieldName;
      newEntry[referenceFieldName] = parentActiveEntryKey[referencedFieldName];
      this.logger.trace(
        'add nested entry with reference field "' + referenceFieldName + '" set to',
        parentActiveEntryKey[referencedFieldName]
      );
    }

    return this.facade.add(newEntry)
      .pipe(tap(([_, createdEntry]) => {
        this.crudStateService.addRecentlyCreatedEntry(createdEntry);
      }));
  }

  delete(key: EntryKey): Observable<void> {
    return this.facade.delete(key);
  }

  updateOne(key: EntryKey, newData: Entry): Observable<Entry | undefined> {
    return this.facade.updateOne(key, newData)
      .pipe(tap((updatedEntry: Entry | undefined) => {
        if (updatedEntry) {
          this.crudStateService.addRecentlyUpdatedEntry(updatedEntry);
        } else {
          this.logger.error('Unable to update entry with key', key, 'with new values', newData);
        }
      }));
  }

  restore(): void {
    this.facade.restore();
  }

}
