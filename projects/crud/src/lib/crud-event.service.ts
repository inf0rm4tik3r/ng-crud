import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {filter, map, withLatestFrom} from 'rxjs/operators';
import {CrudStateService} from './crud-state.service';

@Injectable()
export class CrudEventService {

  private readonly _instanceClick: Subject<MouseEvent> = new Subject<MouseEvent>();
  public readonly instanceClick$: Observable<MouseEvent> = this._instanceClick.asObservable();

  private readonly _documentClick: Subject<MouseEvent> = new Subject<MouseEvent>();
  public readonly documentClick$: Observable<MouseEvent> = this._documentClick.asObservable();

  private readonly _keydown: Subject<KeyboardEvent> = new Subject<KeyboardEvent>();
  public readonly keydown$: Observable<KeyboardEvent> = this._keydown.asObservable();
  public readonly keydownInActiveInstance$: Observable<KeyboardEvent>;

  constructor(private crudStateService: CrudStateService) {
    this.keydownInActiveInstance$ = this.keydown$
      .pipe(
        withLatestFrom(this.crudStateService.active$),
        filter(([_, active]) => active),
        map(([event, _]) => event)
      );
  }

  handleInstanceClick(event: MouseEvent): void {
    this._instanceClick.next(event);
  }

  handleDocumentClick(event: MouseEvent): void {
    this._documentClick.next(event);
  }

  handleKeydown(event: KeyboardEvent): void {
    this._keydown.next(event);
  }

}
