import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {CrudAlertComponent} from './crud-alert/crud-alert.component';
import {CrudAlertsComponent} from './crud-alerts/crud-alerts.component';
import {CrudCollapsibleModule} from './crud-collapsible/crud-collapsible.module';
import {CrudColorPickerComponent} from './crud-color-picker/crud-color-picker.component';
import {CrudContainerDirective} from './crud-container.directive';
import {CrudCreateComponent} from './crud-create/crud-create.component';
import {CrudDeleteModalComponent} from './crud-delete-modal/crud-delete-modal.component';
import {CrudEditComponent} from './crud-edit/crud-edit.component';
import {CrudEventModule} from './crud-event/crud-event.module';
import {CrudFieldLabelComponent} from './crud-field-label/crud-field-label.component';
import {CrudFieldComponent} from './crud-field/crud-field.component';
import {CrudColorFieldComponent} from './crud-fields/crud-color-field/crud-color-field.component';
import {CrudFieldsComponent} from './crud-fields/crud-fields.component';
import {CrudFileFieldComponent} from './crud-fields/crud-file-field/crud-file-field.component';
import {CrudGeneralFieldComponent} from './crud-fields/crud-general-field/crud-general-field.component';
import {CrudMultiselectFieldComponent} from './crud-fields/crud-multiselect-field/crud-multiselect-field.component';
import {CrudNestedInstanceFieldComponent} from './crud-fields/crud-nested-instance-field/crud-nested-instance-field.component';
import {CrudNumberFieldComponent} from './crud-fields/crud-number-field/crud-number-field.component';
import {CrudPasswordFieldComponent} from './crud-fields/crud-password-field/crud-password-field.component';
import {CrudRelationFieldComponent} from './crud-fields/crud-relation-field/crud-relation-field.component';
import {CrudSelectFieldComponent} from './crud-fields/crud-select-field/crud-select-field.component';
import {CrudStringFieldComponent} from './crud-fields/crud-string-field/crud-string-field.component';
import {CrudTextFieldComponent} from './crud-fields/crud-text-field/crud-text-field.component';
import {CrudConditionClauseEditorComponent} from './crud-filter/crud-condition-clause-editor/crud-condition-clause-editor.component';
import {CrudConditionClauseComponent} from './crud-filter/crud-condition-clause/crud-condition-clause.component';
import {CrudConditionOperatorEditorComponent} from './crud-filter/crud-condition-operator-editor/crud-condition-operator-editor.component';
import {CrudConditionOperatorComponent} from './crud-filter/crud-condition-operator/crud-condition-operator.component';
import {CrudConditionComponent} from './crud-filter/crud-condition/crud-condition.component';
import {CrudFilterComponent} from './crud-filter/crud-filter.component';
import {CrudHistoryComponent} from './crud-history/crud-history.component';
import {CrudInlineeditModalComponent} from './crud-inlineedit-modal/crud-inlineedit-modal.component';
import {CrudInstanceComponent} from './crud-instance/crud-instance.component';
import {NotLicensedWarningComponent} from './crud-licensing/not-licensed-warning/not-licensed-warning.component';
import {CrudListComponent} from './crud-list/crud-list.component';
import {CrudPaginationComponent} from './crud-list/crud-pagination/crud-pagination.component';
import {CrudLoadingIndicatorModule} from './crud-loading-indicator/crud-loading-indicator.module';
import {CrudModalBackdropComponent} from './crud-modal/crud-modal-backdrop/crud-modal-backdrop.component';
import {CrudModalComponent} from './crud-modal/crud-modal.component';
import {CrudModifiedModalComponent} from './crud-modified-modal/crud-modified-modal.component';
import {CrudPainterComponent} from './crud-painter/crud-painter.component';
import {RemoveWrapperDirective} from './crud-remove-wrapper.directive';
import {CrudSelectModule} from './crud-select/crud-select.module';
import {CrudTableBodyComponent} from './crud-table/crud-table-body/crud-table-body.component';
import {CrudTableCellComponent} from './crud-table/crud-table-cell/crud-table-cell.component';
import {CrudTableFooterComponent} from './crud-table/crud-table-footer/crud-table-footer.component';
import {CrudTableHeaderComponent} from './crud-table/crud-table-header/crud-table-header.component';
import {CrudTableComponent} from './crud-table/crud-table.component';
import {CrudToastComponent} from './crud-toasts/crud-toast/crud-toast.component';
import {CrudToastsComponent} from './crud-toasts/crud-toasts/crud-toasts.component';
import {CrudBtnGroupComponent} from './crud-ui/crud-btn-group/crud-btn-group.component';
import {CrudBtnWrapperComponent} from './crud-ui/crud-btn-wrapper/crud-btn-wrapper.component';
import {CrudBtnComponent} from './crud-ui/crud-btn/crud-btn.component';
import {CrudViewComponent} from './crud-view/crud-view.component';
import {NgVarDirective} from './helper/ng-var.directive';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    CrudLoadingIndicatorModule,
    FormsModule,
    ReactiveFormsModule,
    CrudSelectModule,
    CrudCollapsibleModule,
    FontAwesomeModule,
    CrudEventModule
  ],
  declarations: [
    CrudContainerDirective,
    CrudInstanceComponent,
    CrudListComponent,
    CrudEditComponent,
    CrudViewComponent,
    CrudFieldsComponent,
    CrudTableComponent,
    CrudTableHeaderComponent,
    CrudTableBodyComponent,
    CrudTableFooterComponent,
    RemoveWrapperDirective,
    CrudCreateComponent,
    CrudPaginationComponent,
    CrudModalComponent,
    CrudModalBackdropComponent,
    CrudDeleteModalComponent,
    CrudModifiedModalComponent,
    CrudAlertComponent,
    CrudAlertsComponent,
    NotLicensedWarningComponent,
    CrudNumberFieldComponent,
    CrudTextFieldComponent,
    CrudPasswordFieldComponent,
    CrudFileFieldComponent,
    CrudSelectFieldComponent,
    CrudMultiselectFieldComponent,
    CrudGeneralFieldComponent,
    CrudFieldComponent,
    CrudStringFieldComponent,
    CrudColorFieldComponent,
    CrudColorPickerComponent,
    CrudPainterComponent,
    CrudInlineeditModalComponent,
    CrudFilterComponent,
    CrudConditionClauseComponent,
    CrudConditionOperatorComponent,
    CrudConditionComponent,
    CrudRelationFieldComponent,
    CrudNestedInstanceFieldComponent,
    CrudConditionClauseEditorComponent,
    CrudConditionOperatorEditorComponent,
    CrudTableCellComponent,
    CrudHistoryComponent,
    CrudBtnComponent,
    CrudBtnGroupComponent,
    CrudBtnWrapperComponent,
    CrudFieldLabelComponent,
    NgVarDirective,
    CrudToastComponent,
    CrudToastsComponent
  ],
  entryComponents: [
    CrudDeleteModalComponent,
    CrudModifiedModalComponent
  ],
  exports: [
    CrudInstanceComponent,
    CrudToastsComponent
  ]
})
export class CrudModule {
}
