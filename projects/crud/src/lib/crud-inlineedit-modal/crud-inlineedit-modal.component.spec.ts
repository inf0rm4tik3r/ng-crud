import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudInlineeditModalComponent } from './crud-inlineedit-modal.component';

describe('CrudInlineeditModalComponent', () => {
  let component: CrudInlineeditModalComponent;
  let fixture: ComponentFixture<CrudInlineeditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudInlineeditModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudInlineeditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
