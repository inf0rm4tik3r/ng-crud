import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import {CrudComponent} from '../crud-component';
import {CrudConfigService} from '../crud-config.service';
import {CrudDataProviderService} from '../crud-data-provider/crud-data-provider.service';
import {CrudFieldComponent} from '../crud-field/crud-field.component';
import {CrudInstanceService} from '../crud-instance/crud-instance-service';
import {CrudLoggingService} from '../crud-logging.service';
import {DataTypeDefinition, Entry} from '../types/crud-types';

@Component({
  selector: 'crud-inlineedit-modal',
  templateUrl: './crud-inlineedit-modal.component.html',
  styleUrls: ['./crud-inlineedit-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudInlineeditModalComponent extends CrudComponent implements AfterViewInit {

  @ViewChild(CrudFieldComponent)
  field: CrudFieldComponent;

  @Input()
  entry: Entry;

  @Input()
  typeDef: DataTypeDefinition;

  @Output()
  cancelClicked: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  saveClicked: EventEmitter<unknown> = new EventEmitter<unknown>();

  value: unknown | undefined;

  constructor(public crudConfigService: CrudConfigService,
              public crudDataProviderService: CrudDataProviderService,
              private crudInstanceService: CrudInstanceService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.field.focus();
      this.cdr.detectChanges();
    }, 0);
  }

  @HostListener('window:keydown.esc', ['$event'])
  private onEscape(event: KeyboardEvent): void {
    if (this.crudInstanceService.instance.active) {
      this.logger.warn('inlineedit - esc');
      this.cancel();
      event.stopPropagation();
    }
  }

  @HostListener('window:keydown.enter', ['$event'])
  private onEnter(event: KeyboardEvent): void {
    if (this.crudInstanceService.instance.active) {
      this.save();
      event.stopPropagation();
    }
  }

  cancel(): void {
    this.cancelClicked.emit();
  }

  save(): void {
    this.saveClicked.emit(this.value ? this.value : this.entry[this.typeDef.name]);
  }

  getComponentName(): string {
    return 'inlineedit-modal';
  }

}
