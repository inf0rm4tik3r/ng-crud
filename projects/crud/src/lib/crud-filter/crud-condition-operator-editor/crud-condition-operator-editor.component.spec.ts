import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudConditionOperatorEditorComponent} from './crud-condition-operator-editor.component';

describe('CrudConditionClauseOpEditorComponent', () => {
  let component: CrudConditionOperatorEditorComponent;
  let fixture: ComponentFixture<CrudConditionOperatorEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudConditionOperatorEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudConditionOperatorEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
