import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {CrudComponent} from '../../crud-component';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudConditionOperator} from '../crud-condition-operator';

@Component({
  selector: 'crud-condition-operator-editor',
  templateUrl: './crud-condition-operator-editor.component.html',
  styleUrls: ['./crud-condition-operator-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudConditionOperatorEditorComponent extends CrudComponent {

  @Input()
  op: CrudConditionOperator;

  @Output()
  opChange: EventEmitter<void> = new EventEmitter<void>();

  and = CrudConditionOperator.AND;
  or = CrudConditionOperator.OR;

  constructor(protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  setAnd(): void {
    this.op.operator = this.and;
    this.opChange.emit();
    this.cdr.detectChanges();
  }

  setOr(): void {
    this.op.operator = this.or;
    this.opChange.emit();
    this.cdr.detectChanges();
  }

  getComponentName(): string {
    return 'condition-operator-editor';
  }

}
