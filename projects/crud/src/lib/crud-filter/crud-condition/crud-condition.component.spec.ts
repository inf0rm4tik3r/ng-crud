import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudConditionComponent} from './crud-condition.component';

describe('CrudConditionComponent', () => {
  let component: CrudConditionComponent;
  let fixture: ComponentFixture<CrudConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudConditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
