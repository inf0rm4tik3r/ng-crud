import {Component, EventEmitter, HostBinding, Input, Output} from '@angular/core';
import {CrudCondition, CrudConditionQueryElements} from '../crud-condition';
import {CrudConditionClause} from '../crud-condition-clause';

@Component({
  selector: 'crud-condition',
  templateUrl: './crud-condition.component.html',
  styleUrls: ['./crud-condition.component.scss']
})
export class CrudConditionComponent {

  @Input()
  condition: CrudCondition;

  @HostBinding('class.highlightParentheses')
  highlightParentheses: boolean = false;

  @Output()
  selected: EventEmitter<CrudConditionQueryElements> = new EventEmitter<CrudConditionQueryElements>();

  isClauseOp(part: unknown): boolean {
    return typeof part === 'string';
  }

  isClause(part: unknown): boolean {
    return part instanceof CrudConditionClause;
  }

  isCondition(part: unknown): boolean {
    return part instanceof CrudCondition;
  }

}
