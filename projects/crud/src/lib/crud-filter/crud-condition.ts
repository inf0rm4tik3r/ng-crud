import {Observable, Subject} from 'rxjs';
import {CrudConditionClause} from './crud-condition-clause';
import {CrudConditionOperator, CrudConditionOperatorType} from './crud-condition-operator';

export type CrudConditionQueryElements = (CrudCondition | CrudConditionClause | CrudConditionOperator);
export type CrudConditionQuery = CrudConditionQueryElements[];

export class CrudCondition {

  /**
   * Reference to the parent Condition or null if the current object is not a sub-query.
   * @var Condition | null
   */
  private parent: CrudCondition | undefined = undefined;

  private query: CrudConditionQuery = [];
  private pos: number = 0;

  private _changes: Subject<CrudCondition> = new Subject<CrudCondition>();

  constructor(parent?: CrudCondition) {
    this.parent = parent;
  }

  /**
   * A conditions without any clauses that will not filter out any entries.
   */
  public static acceptAll(): CrudCondition {
    return new CrudCondition();
  }

  static combine(...conditions: CrudCondition[]): CrudCondition {
    const combined = new CrudCondition();
    for (const condition of conditions) {
      combined.concat(condition, CrudConditionOperator.AND, false);
    }
    return combined;
  }

  debug(): CrudCondition {
    console.log(this);
    console.log(this.toJson());
    return this;
  }

  toString(): string {
    let out = '';

    for (const queryElem of this.query) {
      if (queryElem instanceof CrudConditionOperator) {
        switch (queryElem.operator) {
          case CrudConditionOperator.AND:
            out += ' && ';
            break;
          case CrudConditionOperator.OR:
            out += ' || ';
            break;
        }
      } else if (queryElem instanceof CrudConditionClause) {
        out += '(' + queryElem.toString() + ')';
      } else if (queryElem instanceof CrudCondition) {
        out += '(' + queryElem.toString() + ')';
      }
    }

    return out;
  }

  toJson(): string {
    /*
    let json = '[';
    for (const queryElement of this.query) {

      json += queryElement.toJson();
    }
    json += ']';
    */
    return JSON.stringify(this, (key: string, value: unknown) => {
      if (key === 'parent' || key === 'pos' || key === '_changes') {
        return;
      }
      return value;
    });
  }

  /*
  combine(other: CrudCondition): CrudCondition {
    const combined = new CrudCondition();



    return combined;
  }
  */

  reset(): CrudCondition {
    this.query = [];
    this.pos = 0;
    this.promoteChanges();
    return this;
  }

  contains(columnName: string): boolean {
    for (const queryElem of this.query) {
      if (queryElem instanceof CrudConditionClause && (queryElem as CrudConditionClause).columnName === columnName) {
        return true;
      }
      if (queryElem instanceof CrudCondition && queryElem.contains(columnName)) {
        return true;
      }
    }
    return false;
  }

  where(columnName: string,
        operator: string,
        value: unknown,
        identifier?: string,
        promoteChanges: boolean = true): CrudCondition {
    // Check if the columnName parameter is correctly specified.
    if (columnName.length === 0) {
      console.error('Column name must not be empty!');
      return this;
    }

    // Check if the specified column really exists.
    /*
    if ( !this.assert.column_exists(this.crud.get_table_set().get_main_table(), __FUNCTION__, column_name, args) ) {
      return this;
    }
    */

    // Check if the operator parameter is correctly specified.
    if (operator.length === 0) {
      throw new Error('Operator must not be empty!');
    }

    // Make the operator uppercase. This allows the usage of 'in' instead of 'IN'
    operator = operator.toUpperCase();

    // Check if the specified operator is supported.
    if (!CrudConditionClause.CLAUSE_OPERATORS.includes(operator)) {
      throw new Error('Operator "' + operator + '" is not supported! Use one of: '
        + JSON.stringify(CrudConditionClause.CLAUSE_OPERATORS));
    }

    // Check if the value parameter is correctly specified.
    if (typeof value === typeof undefined) {
      console.error('The value must not be undefined. Null is permitted with some operators.');
      return this;
    }
    if (value === null) {
      // Passing 'null' to the value parameter is allowed, if the operator is set to '=' or '!='.
      // The column is then checked to be null or not null.
      if (operator === '=' || operator === '!=') {
        // do nothing...
      } else {
        console.error('A non null value must be specified when using operator "' + operator + '".');
        return this;
      }
    }

    // Check if the specified value matches the operators conditions.
    switch (operator) {
      // Value must be specified as an array.
      case 'IN':
      case '!IN':
        if (!Array.isArray(value)) {
          console.error('The operators "IN" and "!IN" require the value to be specified as an array!');
          return this;
        }
        break;
      // Value must be specified as an array of size 2.
      case '><':
      case '!><':
      case 'BETWEEN':
      case '!BETWEEN':
        if (!(Array.isArray(value) && value.length === 2)) {
          console.error(
            'The operators "><", "!><", "BETWEEN" and "!BETWEEN" ' +
            'require the value to be specified as an array of size 2!'
          );
          return this;
        }
        break;
    }

    // Check if the identifier was correctly specified and is not already in use.
    if (identifier) {
      if (identifier.length === 0) {
        console.error('Identifier must not be an empty string!');
        return this;
      }
      if (this.findRef(identifier)) {
        console.error(
          'The specified identifier was already used for another where clause. Identifiers must be unique!'
        );
        return this;
      }
    }

    // MAYBE: TODO: Check if value is valid for operator and field type.

    // Add an 'AND' connector if this where() call immediately followed the previous call to this function.
    // Happens if the 'OR' connector was not specified.
    if ((this.pos - 1) >= 0 && this.query[this.pos - 1] instanceof CrudConditionClause) {
      // TODO: Or instanceof Crudcondition?
      this.and(false); // Do not promote this change as it is only part of a bigger change.
    }

    // Add the where clause to the query.
    const ccc = new CrudConditionClause();
    ccc.columnName = columnName;
    ccc.operator = operator;
    ccc.value = value;
    ccc.identifier = identifier;
    this.query[this.pos++] = ccc;

    if (promoteChanges === true) {
      this.promoteChanges();
    }

    // Allows method chaining.
    return this;
  }

  updateValue(identifier: string, value: unknown, promoteChanges: boolean = true): void {
    if (identifier.length === 0) {
      console.error('The identifier must not be empty.');
      return;
    }

    // Retrieve the corresponding where clause.
    const clause: CrudConditionClause = this.findRef(identifier);

    // Check if the identifier corresponded to a where clause.
    if (!clause) {
      console.error('The identifier "' + identifier + '" does not correspond to any where clause!');
      return;
    }

    // Check if the new value fits the operator of the referenced clause.
    if (typeof value === typeof undefined) {
      console.error('The value must not be undefined. Null is permitted with some operators.');
      return;
    }
    if (value === null) {
      // Passing 'null' to the value parameter is allowed, if the operator is set to '=' or '!='.
      // The column is then checked to be null or not null.
      if (clause.operator === '=' || clause.operator === '!=') {
        // do nothing...
      } else {
        console.error('A non null value must be specified when using operator "' + clause.operator + '".');
        return;
      }
    }

    // Set the new value.
    clause.value = value;

    if (promoteChanges === true) {
      this.promoteChanges();
    }
  }

  find(identifier: string): CrudConditionClause | null {
    return this.findRef(identifier);
  }

  private findRef(identifier: string): CrudConditionClause | null {
    // Try to find the identifier on the current level.
    for (const queryElement of this.query) {
      if (queryElement instanceof CrudConditionClause) {
        if (queryElement.identifier === identifier) {
          return queryElement;
        }
      } else if (queryElement instanceof CrudCondition) {
        // Try to find the identifier in a sub level.
        const found = queryElement.findRef(identifier);
        if (found !== null) {
          return found;
        }
      }
      // otherwise continue.
    }
    return null;
  }

  concat(condition: CrudCondition,
         connectingOperatorType: CrudConditionOperatorType = CrudConditionOperator.AND,
         promoteChanges: boolean = true): CrudCondition {
    if (this.isNotEmpty() && !(this.query[this.pos - 1] instanceof CrudConditionOperator)) {
      if (connectingOperatorType === CrudConditionOperator.AND) {
        this.and(false);
      } else if (connectingOperatorType === CrudConditionOperator.OR) {
        this.or(false);
      } else {
        throw new Error('Unexpected condition operator: ' + connectingOperatorType);
      }
    }
    this.query[this.pos++] = condition;
    if (promoteChanges === true) {
      this.promoteChanges();
    }
    return this;
  }

  and(promoteChanges: boolean = true): CrudCondition {
    // Overwrite any previous connector.
    if ((this.pos - 1) >= 0 && this.query[this.pos - 1] instanceof CrudConditionOperator) {
      this.query[this.pos - 1] = CrudConditionOperator.and();
      if (promoteChanges === true) {
        this.promoteChanges();
      }
      return this;
    }

    this.query[this.pos++] = CrudConditionOperator.and();
    if (promoteChanges === true) {
      this.promoteChanges();
    }
    return this;
  }

  or(promoteChanges: boolean = true): CrudCondition {
    // Overwrite any previous connector.
    if ((this.pos - 1) >= 0 && this.query[this.pos - 1] instanceof CrudConditionOperator) {
      this.query[this.pos - 1] = CrudConditionOperator.or();
      if (promoteChanges === true) {
        this.promoteChanges();
      }
      return this;
    }

    this.query[this.pos++] = CrudConditionOperator.or();
    if (promoteChanges === true) {
      this.promoteChanges();
    }
    return this;
  }

  open(promoteChanges: boolean = true): CrudCondition {
    // Add the AND operator if there are already some query element and no connector is present as the last element.
    if ((this.pos - 1) >= 0 && this.query[this.pos - 1] instanceof CrudConditionClause) {
      this.and(false);
    }

    const subQuery = new CrudCondition(this);
    this.query[this.pos++] = subQuery;
    if (promoteChanges === true) {
      this.promoteChanges();
    }
    return subQuery;
  }

  close(): CrudCondition {
    return this.getParent();
  }

  getParent(): CrudCondition {
    return this.parent;
  }

  // TODO: As getter
  getQuery(): CrudConditionQuery {
    return this.query;
  }

  promoteChanges(): void {
    this._changes.next(this);
  }

  get changes(): Observable<CrudCondition> {
    return this._changes.asObservable();
  }

  isEmpty(): boolean {
    return this.query.length === 0;
  }

  isNotEmpty(): boolean {
    return this.query.length !== 0;
  }

}
