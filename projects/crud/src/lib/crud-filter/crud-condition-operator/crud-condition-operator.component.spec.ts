import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudConditionOperatorComponent} from './crud-condition-operator.component';

describe('CrudConditionClauseOpComponent', () => {
  let component: CrudConditionOperatorComponent;
  let fixture: ComponentFixture<CrudConditionOperatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudConditionOperatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudConditionOperatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
