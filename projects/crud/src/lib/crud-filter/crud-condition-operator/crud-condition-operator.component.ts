import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {CrudConditionOperator} from '../crud-condition-operator';

@Component({
  selector: 'crud-condition-operator',
  templateUrl: './crud-condition-operator.component.html',
  styleUrls: ['./crud-condition-operator.component.scss']
})
export class CrudConditionOperatorComponent {

  @Input()
  operator: CrudConditionOperator;

  @Output()
  selected: EventEmitter<CrudConditionOperator> = new EventEmitter<CrudConditionOperator>();

  @HostListener('click')
  select(): void {
    this.selected.emit(this.operator);
  }

}
