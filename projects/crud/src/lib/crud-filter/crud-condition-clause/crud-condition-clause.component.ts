import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {CrudConditionClause} from '../crud-condition-clause';

@Component({
  selector: 'crud-condition-clause',
  templateUrl: './crud-condition-clause.component.html',
  styleUrls: ['./crud-condition-clause.component.scss']
})
export class CrudConditionClauseComponent {

  @Input()
  clause: CrudConditionClause;

  @Output()
  selected: EventEmitter<CrudConditionClause> = new EventEmitter<CrudConditionClause>();

  @HostListener('click')
  select(): void {
    this.selected.emit(this.clause);
  }

}
