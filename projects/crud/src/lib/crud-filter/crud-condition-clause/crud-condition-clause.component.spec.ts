import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudConditionClauseComponent} from './crud-condition-clause.component';

describe('CrudConditionClauseComponent', () => {
  let component: CrudConditionClauseComponent;
  let fixture: ComponentFixture<CrudConditionClauseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudConditionClauseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudConditionClauseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
