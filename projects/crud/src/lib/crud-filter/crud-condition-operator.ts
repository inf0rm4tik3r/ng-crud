export type CrudConditionOperatorType = 'AND' | 'OR';

export class CrudConditionOperator {

  public static readonly AND: CrudConditionOperatorType = 'AND';
  public static readonly OR: CrudConditionOperatorType = 'OR';

  private _operator: CrudConditionOperatorType;

  constructor(operator: CrudConditionOperatorType) {
    this._operator = operator;
  }

  static and(): CrudConditionOperator {
    return new CrudConditionOperator(CrudConditionOperator.AND);
  }

  static or(): CrudConditionOperator {
    return new CrudConditionOperator(CrudConditionOperator.OR);
  }

  get operator(): CrudConditionOperatorType {
    return this._operator;
  }

  set operator(operator: CrudConditionOperatorType) {
    this._operator = operator;
  }

}
