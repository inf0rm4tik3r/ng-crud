import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Choice} from '../../crud-select/state/choices-types';
import {CrudConditionClause} from '../crud-condition-clause';

@Component({
  selector: 'crud-condition-clause-editor',
  templateUrl: './crud-condition-clause-editor.component.html',
  styleUrls: ['./crud-condition-clause-editor.component.scss']
})
export class CrudConditionClauseEditorComponent implements OnChanges {

  @Input()
  clause: CrudConditionClause;

  @Output()
  clauseChange: EventEmitter<void> = new EventEmitter<void>();

  opChoices: Choice[];

  ngOnChanges(_: SimpleChanges): void {
    this.opChoices = [];
    for (const clauseOp of CrudConditionClause.CLAUSE_OPERATORS) {
      this.opChoices.push({
        value: clauseOp,
        label: clauseOp,
        selected: clauseOp === this.clause.operator
      });
    }
  }

}
