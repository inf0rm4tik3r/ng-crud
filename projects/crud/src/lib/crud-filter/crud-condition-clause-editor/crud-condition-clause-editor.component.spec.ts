import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudConditionClauseEditorComponent} from './crud-condition-clause-editor.component';

describe('CrudConditionClauseEditorComponent', () => {
  let component: CrudConditionClauseEditorComponent;
  let fixture: ComponentFixture<CrudConditionClauseEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudConditionClauseEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudConditionClauseEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
