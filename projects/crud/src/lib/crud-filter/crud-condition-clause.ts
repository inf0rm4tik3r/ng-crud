export class CrudConditionClause {

  /**
   * List of supported "where" operators.
   */
  public static readonly CLAUSE_OPERATORS = [
    // Basic
    '=',
    '!=', '<>',
    '<',
    '<=',
    '>',
    '>=',

    // Like
    '*%', 'STARTS_WITH',
    '%*', 'ENDS_WITH',
    '%*%', 'CONTAINS',
    '*', 'EQUALS',

    // IN
    'IN',
    '!IN',

    // Between
    'BETWEEN', '><',
    '!BETWEEN', '!><'
  ];

  private _columnName: string;
  private _operator: string;
  private _value: unknown;
  private _identifier: string | null;

  toString(): string {
    return JSON.stringify(this._columnName) + ' ' + this._operator + ' ' + JSON.stringify(this._value as string);
  }

  toJson(): string {
    return '{\n'
      + '"fieldName": "' + this.columnName + '"},\n'
      + '"operator": "' + this.operator + '"},\n'
      + '"value": "' + this.value + '"},\n'
      + '}';
  }

  get columnName(): string {
    return this._columnName;
  }

  set columnName(value: string) {
    this._columnName = value;
  }

  get operator(): string {
    return this._operator;
  }

  set operator(value: string) {
    this._operator = value;
  }

  get value(): unknown {
    return this._value;
  }

  set value(value: unknown) {
    this._value = value;
  }

  get identifier(): string | null {
    return this._identifier;
  }

  set identifier(value: string | null) {
    this._identifier = value;
  }

}
