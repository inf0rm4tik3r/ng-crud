import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {Subscription} from 'rxjs';
import {CrudComponent} from '../crud-component';
import {CrudLoggingService} from '../crud-logging.service';
import {DataStructure} from '../types/crud-types';
import {CrudCondition, CrudConditionQueryElements} from './crud-condition';
import {CrudConditionClause} from './crud-condition-clause';
import {CrudConditionOperator} from './crud-condition-operator';

@Component({
  selector: 'crud-filter',
  templateUrl: './crud-filter.component.html',
  styleUrls: ['./crud-filter.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudFilterComponent extends CrudComponent implements OnInit, OnChanges {

  @Input()
  dataStructure: DataStructure;

  @Input()
  condition: CrudCondition | undefined = undefined;

  @Output()
  conditionChange: EventEmitter<void> = new EventEmitter<void>();

  conditionChangSubscription: Subscription | undefined = undefined;

  selected: CrudConditionQueryElements;

  createMode: boolean = false;

  constructor(protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  ngOnInit(): void {
    this.subscribeForChanges();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!!changes.condition && changes.condition.currentValue !== changes.condition.previousValue) {
      this.logger.debug('new condition input');
      this.subscribeForChanges();
    }
  }

  private subscribeForChanges(): void {
    if (this.conditionChangSubscription) {
      this.conditionChangSubscription.unsubscribe();
    }

    if (this.condition) {
      this.conditionChangSubscription = this.condition.changes.subscribe(() => {
        this.logger.debug('condition changed', this.condition.toString());
        this.conditionChange.emit();
      });
    }
  }

  select(selected: CrudConditionQueryElements): void {
    this.selected = selected;
    this.createMode = false;
    this.cdr.detectChanges();
  }

  create(): void {
    this.selected = undefined;
    this.createMode = true;
    this.cdr.detectChanges();
  }

  isClause(part: CrudConditionQueryElements): boolean {
    return part instanceof CrudConditionClause;
  }

  isCondition(part: CrudConditionQueryElements): boolean {
    return part instanceof CrudCondition;
  }

  isConditionOp(part: CrudConditionQueryElements): boolean {
    return part instanceof CrudConditionOperator;
  }

  getComponentName(): string {
    return 'filter';
  }

}
