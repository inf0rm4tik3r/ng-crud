import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudTableCellComponent} from './crud-table-cell.component';

describe('CrudTableCellComponent', () => {
  let component: CrudTableCellComponent;
  let fixture: ComponentFixture<CrudTableCellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudTableCellComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudTableCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
