import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild} from '@angular/core';
import {faEdit} from '@fortawesome/pro-regular-svg-icons';
import {first} from 'rxjs/operators';
import {CrudComponent} from '../../crud-component';
import {CrudConfigService} from '../../crud-config.service';
import {CrudDataProviderService} from '../../crud-data-provider/crud-data-provider.service';
import {CrudFieldComponent} from '../../crud-field/crud-field.component';
import {CrudInlineeditModalComponent} from '../../crud-inlineedit-modal/crud-inlineedit-modal.component';
import {CrudListService} from '../../crud-list/crud-list.service';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudModalService} from '../../crud-modal/crud-modal.service';
import {DataTypeDefinition, Entry} from '../../types/crud-types';

@Component({
  selector: 'crud-table-cell',
  templateUrl: './crud-table-cell.component.html',
  styleUrls: ['./crud-table-cell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudTableCellComponent extends CrudComponent implements OnInit {

  @ViewChild(CrudFieldComponent)
  private field: CrudFieldComponent;

  @Input()
  entry: Entry;

  @Input()
  typeDef: DataTypeDefinition;

  faEdit = faEdit;

  constructor(private crudDataProviderService: CrudDataProviderService,
              private crudListService: CrudListService,
              private crudModalService: CrudModalService,
              public crudConfigService: CrudConfigService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    cdr.detach();
  }

  ngOnInit(): void {
    this.cdr.detectChanges();
  }

  updateInjectedComponent(): void {
    this.field.updateInjectedComponent();
    this.cdr.detectChanges();
  }

  openInlineEdit(entry: Entry, typeDef: DataTypeDefinition): void {
    this.crudModalService.init(
      CrudInlineeditModalComponent,
      {
        key: this.crudDataProviderService.getKey(entry),
        entry,
        typeDef
      },
      {
        cancelClicked: () => {
          this.crudModalService.destroy();
        },
        saveClicked: (value) => {
          entry[typeDef.name] = value;
          this.crudDataProviderService.updateOne(this.crudDataProviderService.getKey(entry), entry)
            .pipe(first())
            .subscribe(() => {
              this.crudModalService.destroy();
              this.crudListService.triggerReload();
            });
        }
      });
  }

  getComponentName(): string {
    return 'table-cell';
  }

}
