import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudTableFooterComponent} from './crud-table-footer.component';

describe('CrudTableFooterComponent', () => {
  let component: CrudTableFooterComponent;
  let fixture: ComponentFixture<CrudTableFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudTableFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudTableFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
