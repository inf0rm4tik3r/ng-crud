import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {DataStructure, Entries} from '../../types/crud-types';

@Component({
  selector: 'crud-table-footer',
  templateUrl: './crud-table-footer.component.html',
  styleUrls: ['./crud-table-footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudTableFooterComponent {

  @Input()
  dataStructure: DataStructure = [];

  @Input()
  entries: Entries = [];

}
