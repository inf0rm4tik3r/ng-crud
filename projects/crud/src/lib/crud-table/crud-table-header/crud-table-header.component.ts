import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {faFilter as faFilterRegular} from '@fortawesome/pro-regular-svg-icons';
import {faFilter as faFilterSolid} from '@fortawesome/pro-solid-svg-icons';
import {takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../../crud-component';
import {CrudConfigService} from '../../crud-config.service';
import {CrudCondition} from '../../crud-filter/crud-condition';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudStateService} from '../../crud-state.service';
import {DataStructure} from '../../types/crud-types';

@Component({
  selector: 'crud-table-header',
  templateUrl: './crud-table-header.component.html',
  styleUrls: ['./crud-table-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudTableHeaderComponent extends CrudComponent implements OnInit {

  @Input()
  dataStructure: DataStructure = [];

  userFilter: CrudCondition | undefined = undefined;

  faFilterSolid = faFilterSolid;
  faFilterRegular = faFilterRegular;

  constructor(public crudConfigService: CrudConfigService,
              public crudStateService: CrudStateService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  ngOnInit(): void {
    this.crudStateService.userFilter$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(userFilter => this.userFilter = userFilter);
  }

  getComponentName(): string {
    return 'table-header';
  }

  isOrderByAllowed(columnName: string): boolean {
    return this.crudConfigService.config.isOrderByAllowed(columnName);
  }

  /**
   * Update the orderBy state of a field.
   */
  orderBy(event: MouseEvent, fieldName: string): void {
    if (this.isOrderByAllowed(fieldName)) {
      this.crudStateService.touchOrderedBy(fieldName, event.ctrlKey);
    } else {
      this.logger.warn('Ordering by field ' + fieldName
        + ' is not allowed. This function should not have been called.');
    }
  }

  isFilterByAllowed(fieldName: string): boolean {
    return this.crudConfigService.config.isFilterAllowedFor(fieldName);
  }

  isFilteredBy(fieldName: string): boolean {
    return this.userFilter ? this.userFilter.contains(fieldName) : false;
  }

  openFilterSettings(event: Event, fieldName: string): void {
    this.logger.warn('not jet implemented');
    event.stopPropagation();
    event.preventDefault();
    this.logger.trace('openFilterSettings', fieldName);
  }

}
