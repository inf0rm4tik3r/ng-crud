import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudTableHeaderComponent} from './crud-table-header.component';

describe('CrudTableHeaderComponent', () => {
  let component: CrudTableHeaderComponent;
  let fixture: ComponentFixture<CrudTableHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudTableHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudTableHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
