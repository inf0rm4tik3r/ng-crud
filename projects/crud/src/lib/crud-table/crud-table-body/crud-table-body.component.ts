import {ChangeDetectorRef, Component, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {faEye, faPencil, faTrash, faTrashAlt} from '@fortawesome/pro-regular-svg-icons';
import {first, takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../../crud-component';
import {CrudDataProviderService} from '../../crud-data-provider/crud-data-provider.service';
import {CrudDeleteModalComponent} from '../../crud-delete-modal/crud-delete-modal.component';
import {CrudListService} from '../../crud-list/crud-list.service';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudModalService} from '../../crud-modal/crud-modal.service';
import {CrudRouterService} from '../../crud-router.service';
import {CrudStateService} from '../../crud-state.service';
import {DataStructure, Entries, Entry} from '../../types/crud-types';
import {LinkedList} from '../../types/linked-list';
import {CrudTableCellComponent} from '../crud-table-cell/crud-table-cell.component';

@Component({
  selector: 'crud-table-body',
  templateUrl: './crud-table-body.component.html',
  styleUrls: ['./crud-table-body.component.scss']
})
export class CrudTableBodyComponent extends CrudComponent implements OnInit {

  @ViewChildren(CrudTableCellComponent)
  cells: QueryList<CrudTableCellComponent>;

  @Input()
  dataStructure: DataStructure = [];

  @Input()
  entries: Entries = [];

  recentlyUpdatedEntries: LinkedList<Entry>;

  faEye = faEye;
  faPencil = faPencil;
  faTrash = faTrash;
  faTrashAlt = faTrashAlt;

  constructor(private crudDataProviderService: CrudDataProviderService,
              private crudRouterService: CrudRouterService,
              private crudStateService: CrudStateService,
              private crudListService: CrudListService,
              private crudModalService: CrudModalService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  ngOnInit(): void {
    this.crudStateService.recentlyUpdatedEntries$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(recentlyUpdatedEntries => {
        this.recentlyUpdatedEntries = recentlyUpdatedEntries;
        this.cdr.detectChanges();
      });
  }

  wasRecentlyUpdated(entry: Entry): boolean {
    return !!this.recentlyUpdatedEntries.find(entry);
  }

  onViewClick(entry: Entry): void {
    this.crudStateService.setActiveEntry([entry, this.crudDataProviderService.getKey(entry)]);
    this.crudRouterService.openViewView();
  }

  onEditClick(entry: Entry): void {
    this.crudStateService.setActiveEntry([entry, this.crudDataProviderService.getKey(entry)]);
    this.crudRouterService.openEditView();
  }

  onDeleteClick(entry: Entry): void {
    this.crudModalService.init(
      CrudDeleteModalComponent,
      {
        entry
      },
      {
        cancelClicked: () => {
          this.crudModalService.destroy();
        },
        deleteClicked: () => {
          this.crudDataProviderService.delete(this.crudDataProviderService.getKey(entry))
            .pipe(first())
            .subscribe(() => {
              this.crudListService.triggerReload();
              this.crudModalService.destroy();
            });
        }
      });
  }

  getComponentName(): string {
    return 'table-body';
  }

}
