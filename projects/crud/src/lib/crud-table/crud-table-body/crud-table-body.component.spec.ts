import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudTableBodyComponent} from './crud-table-body.component';

describe('CrudTableBodyComponent', () => {
  let component: CrudTableBodyComponent;
  let fixture: ComponentFixture<CrudTableBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudTableBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudTableBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
