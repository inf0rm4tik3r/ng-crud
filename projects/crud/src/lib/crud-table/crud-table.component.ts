import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, ViewChild} from '@angular/core';
import {CrudComponent} from '../crud-component';
import {CrudLoggingService} from '../crud-logging.service';
import {DataStructure, Entries} from '../types/crud-types';
import {CrudTableBodyComponent} from './crud-table-body/crud-table-body.component';

@Component({
  selector: 'crud-table',
  templateUrl: './crud-table.component.html',
  styleUrls: ['./crud-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudTableComponent extends CrudComponent {

  @ViewChild(CrudTableBodyComponent)
  bodyComponent: CrudTableBodyComponent;

  @Input()
  dataStructure: DataStructure;

  @Input()
  data: Entries;

  constructor(protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  getComponentName(): string {
    return 'table';
  }

}
