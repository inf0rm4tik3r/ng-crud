import {Entry} from '../types/crud-types';

export interface CrudDataStoreEntry {

  data: Entry;

  $loki: unknown | undefined;
  meta: unknown | undefined;

}
