import LokiConstructor, {Collection} from 'lokijs';
import {DataStructure} from '../types/crud-types';
import {CrudDataStoreEntry} from './crud-data-store-entry';

export class CrudDataStore {

  private readonly _db: Loki;
  private readonly _data: Collection<CrudDataStoreEntry>;

  constructor(name: string, dataStructure: DataStructure) {
    // Create the database.
    this._db = new LokiConstructor(name + '.db');

    // Add a collection.
    const fieldNames = [];
    for (const typeDef of dataStructure) {
      fieldNames.push(typeDef.name);
    }
    this._data = this.db.addCollection('data', {indices: fieldNames});
  }

  get db(): Loki {
    return this._db;
  }

  get data(): Collection<CrudDataStoreEntry> {
    return this._data;
  }

}
