import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {CrudAlertService} from '../crud-alert.service';
import {CrudComponent} from '../crud-component';
import {CrudLoggingService} from '../crud-logging.service';
import {CrudAlert} from './crud-alert';

@Component({
  selector: 'crud-alert',
  templateUrl: './crud-alert.component.html',
  styleUrls: ['./crud-alert.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudAlertComponent extends CrudComponent {

  // TODO: Handle automatic closing.

  @Input()
  alert: CrudAlert;

  constructor(private crudAlertService: CrudAlertService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  close(): void {
    this.crudAlertService.removeAlert(this.alert);
  }

  getComponentName(): string {
    return 'alert';
  }

}
