export type SupportedCrudAlertTypes = 'info' | 'success' | 'warning' | 'danger';

// Immutable. Mutable alert would not go well with our manual ChangeDetection.
export class CrudAlert {

  /**
   * All automatically closing messages will close after this delay.
   * @var int The delay in milliseconds.
   */
  public static readonly AUTOMATIC_CLOSE_DELAY: number = 2500;

  private readonly _type: SupportedCrudAlertTypes = 'info';

  private readonly _heading: string = '';

  private readonly _message: string = '';

  private readonly _dismissible: boolean = true;

  private readonly _automaticallyClosing: boolean = true;

  private readonly _automaticCloseDelay: number = CrudAlert.AUTOMATIC_CLOSE_DELAY;

  constructor(type: SupportedCrudAlertTypes = 'info',
              heading: string = '',
              message: string = '',
              dismissible: boolean = true,
              automaticallyClosing: boolean = true,
              automaticCloseDelay: number = CrudAlert.AUTOMATIC_CLOSE_DELAY) {
    this._type = type;
    this._heading = heading;
    this._message = message;
    this._dismissible = dismissible;
    this._automaticallyClosing = automaticallyClosing;
    this._automaticCloseDelay = automaticCloseDelay;
  }

  get type(): SupportedCrudAlertTypes {
    return this._type;
  }

  get heading(): string {
    return this._heading;
  }

  get message(): string {
    return this._message;
  }

  get dismissible(): boolean {
    return this._dismissible;
  }

  get automaticallyClosing(): boolean {
    return this._automaticallyClosing;
  }

  get automaticCloseDelay(): number {
    return this._automaticCloseDelay;
  }

}
