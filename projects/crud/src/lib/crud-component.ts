import {ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';
import {CrudLoggingService} from './crud-logging.service';

@Component({
  template: ''
})
export abstract class CrudComponent implements OnDestroy {

  private _unsubscribe$: Subject<void> = undefined;

  protected constructor(protected cdr: ChangeDetectorRef,
                        protected logger: CrudLoggingService) {
  }

  ngOnDestroy(): void {
    if (this._unsubscribe$) {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
      // this.logger.trace('unsubscribe$ completed');
    }
  }

  checked(): void {
    // this.logger.trace(this.getComponentName() + ' checked');
  }

  get unsubscribe$(): Subject<void> {
    if (!this._unsubscribe$) {
      this._unsubscribe$ = new Subject<void>();
    }
    return this._unsubscribe$;
  }

  abstract getComponentName(): string;

}
