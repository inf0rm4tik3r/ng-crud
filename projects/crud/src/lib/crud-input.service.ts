import {Injectable} from '@angular/core';
import {CrudUser} from './crud-user/crud-user';
import {CrudConfig} from './crud-config';
import {CrudInstanceComponent} from './crud-instance/crud-instance.component';
import {NestedInstanceConfig} from './types/crud-types';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class CrudInputService {

  private _title: BehaviorSubject<string> = new BehaviorSubject<string>('CrudInstance');
  title$: Observable<string> = this._title.asObservable();

  private _theme: BehaviorSubject<string> = new BehaviorSubject<string>('light');
  theme$: Observable<string> = this._theme.asObservable();

  private _themeOverriders: string | string[] | undefined = undefined;
  private _config: CrudConfig | undefined = undefined;
  private _nestedInstanceConfig: NestedInstanceConfig | undefined = undefined;
  private _currentUser: CrudUser = CrudUser.DEFAULT_USER;
  private _parent: CrudInstanceComponent | undefined = undefined;

  setTitle(value: string): void {
    this._title.next(value);
  }

  get theme(): string {
    return this._theme.getValue();
  }

  set theme(value: string) {
    this._theme.next(value);
  }

  get themeOverriders(): string | string[] | undefined {
    return this._themeOverriders;
  }

  set themeOverriders(value: string | string[] | undefined) {
    this._themeOverriders = value;
  }

  get config(): CrudConfig | undefined {
    return this._config;
  }

  set config(config: CrudConfig | undefined) {
    this._config = config;
  }

  get nestedInstanceConfig(): NestedInstanceConfig | undefined {
    return this._nestedInstanceConfig;
  }

  set nestedInstanceConfig(value: NestedInstanceConfig | undefined) {
    this._nestedInstanceConfig = value;
  }

  get currentUser(): CrudUser {
    return this._currentUser;
  }

  set currentUser(value: CrudUser) {
    this._currentUser = value;
  }

  get parent(): CrudInstanceComponent | undefined {
    return this._parent;
  }

  set parent(value: CrudInstanceComponent | undefined) {
    this._parent = value;
  }

}
