import {Injectable} from '@angular/core';
import {CrudListComponent} from './crud-list.component';

@Injectable()
export class CrudListService {

  private _list: CrudListComponent;

  registerList(list: CrudListComponent): void {
    this._list = list;
  }

  triggerReload(): void {
    this._list.triggerReload$.next(); // Inherently reloads the pagination as the list updates the entry count variable!
  }

}
