import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../../crud-component';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudStateService} from '../../crud-state.service';
import {range} from '../../crud-util';
import {CrudListService} from '../crud-list.service';

@Component({
  selector: 'crud-pagination',
  templateUrl: './crud-pagination.component.html',
  styleUrls: ['./crud-pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudPaginationComponent extends CrudComponent implements OnInit {

  private _entryCount: number | undefined = undefined;

  @Input()
  set entryCount(entryCount: number | undefined) {
    if (entryCount) {
      this._entryCount = entryCount;
      this.compute();
    }
  }

  get entryCount(): number {
    return this._entryCount;
  }

  mode: 'manual' | 'options' = 'options';

  pageCount: number = 0;
  pageOptions: ReadonlyArray<number> = [];

  usePagination: boolean = false;
  entriesPerPage: number = -1;
  entriesPerPageOptions: number[] = [5, 10, 20, -1];
  currentPageInList: number | undefined = undefined;

  constructor(private crudStateService: CrudStateService,
              private crudListService: CrudListService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  ngOnInit(): void {
    this.crudStateService.usePagination$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((usePagination) => this.usePagination = usePagination);

    this.crudStateService.entriesPerPage$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((entriesPerPage) => this.entriesPerPage = entriesPerPage);

    this.crudStateService.currentPageInList$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((currentPageInList) => this.currentPageInList = currentPageInList);
  }

  compute(): void {
    this.computePageCount(this.entryCount, this.entriesPerPage);
    this.computePageOptions(this.pageCount, this.currentPageInList);

    // We might be on a page which doesn't exist anymore. Let's fix that!
    setTimeout(() => {
      if (this.currentPageInList > this.pageCount) {
        this.updateCurrentPage(this.pageCount);
      }
    }, 0);

    this.cdr.detectChanges();
  }

  onlyAllowNumbers(event: KeyboardEvent): boolean {
    const key: string = event.key;
    if (!(key === '1' || key === '2' || key === '3' || key === '4'
      || key === '5' || key === '6' || key === '7' || key === '8'
      || key === '9' || key === '0' || key === 'Backspace')) {
      return false;
    }
  }

  setEntriesPerPageFromString(entriesPerPage: string): void {
    this.setEntriesPerPage(Number(entriesPerPage) || 1);
  }

  setEntriesPerPage(entriesPerPage: number): void {
    if (this.entriesPerPage !== entriesPerPage) {
      this.crudStateService.setEntriesPerPage(entriesPerPage);
      this.compute();
      this.crudListService.triggerReload();
    }
  }

  updateCurrentPage(page: number): void {
    this.crudStateService.setCurrentPageInList(page);
    this.compute();
    // TODO: Make this call optional. It should not be trigger on component-initialization!
    this.crudListService.triggerReload();
  }

  /**
   * @param entryCount The total number of entries available for display.
   * @param entriesPerPage The amount of entries which are to be displayed on a single page.
   */
  computePageCount(entryCount: number, entriesPerPage: number): void {
    // If entriesPerPage is set not set, all entries should be shown at once. => Only 1 page.
    if (entriesPerPage === -1) {
      this.pageCount = 1;
      return;
    }

    // Calculate a value representing the amount of available pages.
    this.pageCount = Math.ceil(entryCount / entriesPerPage);

    // We mathematically get a pageCount of 0 if entriesPerPage was 0.
    // But there should always be at least 1 page.
    if (this.pageCount === 0) {
      this.pageCount = 1;
    }
  }

  /**
   * Calculates the page indices which should / could be displayed to the user as an
   * array of page indices. May contain '-1' elements to indicate blanks/placeholders (1,2,3,4,-1,999).
   *
   * @param pageCount The number of available pages.
   * @param page The current page.
   */
  computePageOptions(pageCount: number, page: number): void {
    // Just return all available pages if there are not too many of them.
    if (pageCount <= 10) {
      this.pageOptions =
        range(1, pageCount, 1);
    } else if (page <= 5) {
      // Single ... at the right. Start of page spectrum.
      this.pageOptions =
        [1, 2, 3, 4, 5, 6, 7, -1, pageCount - 1, pageCount];
    } else if (page > 5 && page < pageCount - 4) {
      // With ... at the left and right. In the middle of the available pages.
      this.pageOptions =
        [1, 2, -1, page - 2, page - 1, page, page + 1, page + 2, -1, pageCount - 1, pageCount];
    } else if (page >= pageCount - 4) {
      // Single ... at the left. End of page spectrum.
      this.pageOptions =
        [1, 2, -1, pageCount - 6, pageCount - 5, pageCount - 4, pageCount - 3, pageCount - 2, pageCount - 1, pageCount];
    } else {
      // Error...
      this.pageOptions = [-1];
    }
  }

  getComponentName(): string {
    return 'pagination';
  }

}
