import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {faPlusCircle, faRedoAlt, faSearch} from '@fortawesome/pro-solid-svg-icons';
import {combineLatest, Subject} from 'rxjs';
import {first, flatMap, takeUntil, tap} from 'rxjs/operators';
import {CrudComponent} from '../crud-component';
import {CrudDataProviderService} from '../crud-data-provider/crud-data-provider.service';
import {CrudEventService} from '../crud-event.service';
import {CrudCondition} from '../crud-filter/crud-condition';
import {CrudFilterComponent} from '../crud-filter/crud-filter.component';
import {CrudLoggingService} from '../crud-logging.service';
import {CrudModalService} from '../crud-modal/crud-modal.service';
import {CrudRouterService} from '../crud-router.service';
import {CrudStateService} from '../crud-state.service';
import {CrudTableComponent} from '../crud-table/crud-table.component';
import {DataStructure, Entries, OrderByTypeSet} from '../types/crud-types';
import {CrudListService} from './crud-list.service';

@Component({
  selector: 'crud-list',
  templateUrl: './crud-list.component.html',
  styleUrls: ['./crud-list.component.scss'],
  providers: [CrudListService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudListComponent extends CrudComponent implements OnInit {

  @ViewChild(CrudTableComponent)
  tableComponent: CrudTableComponent;

  @ViewChild(CrudFilterComponent)
  filterComponent: CrudFilterComponent;

  dataStructure: DataStructure | undefined = undefined;

  entryCount: number | undefined = undefined;

  data: Entries = [];

  public readonly triggerReload$: Subject<void> = new Subject<void>();
  public readonly triggerFieldRendering$: Subject<void> = new Subject<void>();

  /**
   * Whether or not the user filter should be shown.
   */
  showFilter: boolean = false;

  /**
   * The user filter to show and manipulate.
   * Always contains the last value of the StateService's userFilter$ observable.
   */
  userFilter: CrudCondition = undefined;

  /**
   * Whether or not pagination is used.
   * Always contains the last value of the StateService's usePagination$ observable.
   */
  usePagination: boolean = false;

  faPlusCircle = faPlusCircle;
  faSearch = faSearch;
  faRedoAlt = faRedoAlt;

  constructor(private crudEventService: CrudEventService,
              private crudDataProviderService: CrudDataProviderService,
              private crudRouterService: CrudRouterService,
              public crudStateService: CrudStateService,
              private crudModalService: CrudModalService,
              private crudListService: CrudListService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    cdr.detach();
    crudListService.registerList(this);
  }

  private triggerReload(): void {
    this.logger.trace('reload triggered');
    this.triggerReload$.next();
  }

  ngOnInit(): void {
    // Be informed of structural changes. -> trigger reload
    this.crudDataProviderService.getStructure()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((dataStructure: DataStructure) => {
        // The first invocation of this lambda, providing the initial structure, must not trigger a reload.
        let shouldReload = false;
        if (this.dataStructure) {
          shouldReload = true;
        }
        this.logger.trace('structural change');
        this.dataStructure = dataStructure;
        this.cdr.detectChanges();
        if (shouldReload) {
          this.triggerReload();
        }
      });

    // Be informed about a changing user filter.
    // A change in the filter will most likely change the amount of data available to us.
    // Only relevant if pagination is used.
    // TODO: Only trigger a "count" reload after an entry was created or deleted?
    combineLatest([
      this.triggerReload$,
      this.crudStateService.userFilter$
    ])
      .pipe(
        takeUntil(this.unsubscribe$),
        tap(([, userFilter]) => this.userFilter = userFilter),
        flatMap(([_, userFilter]) => this.crudDataProviderService.getCount(userFilter))
      )
      .subscribe((count: number) => {
        this.entryCount = count;
        this.cdr.detectChanges();
      });

    //
    combineLatest([
      this.triggerReload$,
      this.crudStateService.usePagination$,
      this.crudStateService.currentPageInList$,
      this.crudStateService.entriesPerPage$,
      this.crudStateService.orderedBy$,
      this.crudStateService.userFilter$
    ])
      .pipe(
        takeUntil(this.unsubscribe$),
        tap(state => this.logger.debug('state change', state))
      )
      .subscribe(([_, usePagination, currentPageInList, entriesPerPage, orderedBy, userFilter]) => {
        this.usePagination = usePagination;
        this.fetchData(usePagination, entriesPerPage, currentPageInList, orderedBy, userFilter);
      });

    // Re-Render all field inside the table.
    this.triggerFieldRendering$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        const cells = this.tableComponent?.bodyComponent?.cells;
        if (cells) {
          for (const cell of cells) {
            cell.updateInjectedComponent();
          }
        }
      });

    // Load the first data!
    this.triggerReload$.next();

    this.crudEventService.keydownInActiveInstance$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event: KeyboardEvent) => this.handleKeyDown(event));
  }

  private fetchData(usePagination: boolean,
                    entriesPerPage: number,
                    currentPageInList: number,
                    orderedBy: OrderByTypeSet,
                    userFilter: CrudCondition): void {
    const observable = usePagination && entriesPerPage !== -1 ?
      this.crudDataProviderService.get(
        entriesPerPage, (currentPageInList - 1) * entriesPerPage, orderedBy, userFilter)
      : this.crudDataProviderService.getAll(orderedBy, userFilter);
    observable.pipe(first()).subscribe((data: Entries) => this.handleNewData(data));
  }

  private handleKeyDown(event: KeyboardEvent): void {
    // "Add".
    if (event.key === 'a' && !event.ctrlKey && !this.crudModalService.hasModals()) {
      this.crudStateService.unsetActiveEntry();
      this.crudRouterService.openCreateView();
    }

    // "Start searching".
    if (event.ctrlKey && event.key === 'f' && !this.crudModalService.hasModals()) {
      this.showFilter = true;
      this.cdr.detectChanges();
      event.preventDefault();
      event.stopPropagation();
    }
  }

  private handleNewData(data: Entries): void {
    this.logger.trace('handling new data', data);
    this.data = data;
    this.cdr.detectChanges();
    this.triggerFieldRendering$.next();
  }

  toggleFilter(): void {
    this.showFilter = !this.showFilter;
    this.cdr.detectChanges();
  }

  add(): void {
    this.crudStateService.unsetActiveEntry();
    this.crudRouterService.openCreateView();
  }

  restore(): void {
    this.crudDataProviderService.restore();
    this.triggerReload$.next();
  }

  getComponentName(): string {
    return 'list';
  }

}
