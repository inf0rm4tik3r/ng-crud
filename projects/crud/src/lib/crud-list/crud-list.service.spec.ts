import {TestBed} from '@angular/core/testing';

import {CrudListService} from './crud-list.service';

describe('CrudListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudListService = TestBed.get(CrudListService);
    expect(service).toBeTruthy();
  });
});
