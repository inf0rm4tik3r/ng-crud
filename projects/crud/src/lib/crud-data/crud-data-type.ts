export enum CrudDataType {

  number,
  int,
  float,
  string,
  text,
  date,
  time,
  datetime,
  timestamp

}
