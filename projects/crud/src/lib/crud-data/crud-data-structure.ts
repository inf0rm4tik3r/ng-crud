import {CrudDataTypeDef, CrudDataTypeSpec} from './crud-data-type-def';

export class CrudDataStructure {

  private readonly _fields: CrudDataTypeDef[];

  constructor(...fields: (CrudDataTypeDef | CrudDataTypeDef)[]) {
    this._fields = [];
    for (const elem of fields) {
      if (elem instanceof CrudDataTypeSpec) {
        elem.checkValidity();
        this._fields.push(elem);
      } else {
        this._fields.push(new CrudDataTypeSpec(
          elem.type,
          elem.name,
          elem.nullable,
          elem.defaultValue,
          elem.key,
          elem.autoIncrement,
          elem.constraints
        ));
      }
    }
  }

  get fields(): CrudDataTypeDef[] {
    return this._fields;
  }

}
