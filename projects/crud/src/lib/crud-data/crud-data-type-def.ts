import {CrudInstanceComponent} from '../crud-instance/crud-instance.component';
import {CrudDataTypeConstraints} from '../types/crud-types';
import {CrudDataType} from './crud-data-type';

export interface CrudDataTypeDef {
  type: CrudDataType;
  name: string;
  nullable: boolean;
  defaultValue: unknown;
  key: boolean;
  autoIncrement: boolean;
  constraints?: CrudDataTypeConstraints | undefined;
}

export class CrudDataTypeSpec implements CrudDataTypeDef {

  private _type: CrudDataType;
  private _name: string;
  private _nullable: boolean;
  private _defaultValue: unknown; // Must be compatible with the given type!
  private _key: boolean;
  private _autoIncrement: boolean;
  private _constraints?: CrudDataTypeConstraints | undefined;

  constructor(type: CrudDataType,
              name: string,
              nullable: boolean,
              defaultValue: unknown,
              key: boolean,
              autoIncrement: boolean,
              constraints: CrudDataTypeConstraints | undefined) {
    this._type = type;
    this._name = name;
    this._nullable = nullable;
    this._defaultValue = defaultValue;
    this._key = key;
    this._autoIncrement = autoIncrement;
    this._constraints = constraints;
    this.checkValidity();
  }

  checkValidity(): void {
    if (this.nullable === true && typeof this.defaultValue === typeof undefined) {
      CrudInstanceComponent.logger.error('Nullable field must have a default!', this);
    }
    if (this.autoIncrement === true && typeof this.defaultValue !== typeof undefined) {
      CrudInstanceComponent.logger.error('Autoincrement field must have no default!', this);
    }
  }

  get type(): CrudDataType {
    return this._type;
  }

  set type(value: CrudDataType) {
    this._type = value;
    this.checkValidity();
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
    this.checkValidity();
  }

  get nullable(): boolean {
    return this._nullable;
  }

  set nullable(value: boolean) {
    this._nullable = value;
    this.checkValidity();
  }

  get defaultValue(): unknown {
    return this._defaultValue;
  }

  set defaultValue(value: unknown) {
    this._defaultValue = value;
    this.checkValidity();
  }

  get key(): boolean {
    return this._key;
  }

  set key(value: boolean) {
    this._key = value;
    this.checkValidity();
  }

  get autoIncrement(): boolean {
    return this._autoIncrement;
  }

  set autoIncrement(value: boolean) {
    this._autoIncrement = value;
    this.checkValidity();
  }

  get constraints(): CrudDataTypeConstraints | undefined {
    return this._constraints;
  }

  set constraints(value: CrudDataTypeConstraints | undefined) {
    this._constraints = value;
    this.checkValidity();
  }

}
