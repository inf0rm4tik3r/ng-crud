import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudInstanceComponent} from './crud-instance.component';

describe('CrudInstanceComponent', () => {
  let component: CrudInstanceComponent;
  let fixture: ComponentFixture<CrudInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudInstanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
