import {Injectable} from '@angular/core';
import {first} from 'rxjs/operators';
import {v4 as uuid} from 'uuid';
import {CrudInstancesService} from '../crud-instances.service';
import {CrudLocalStorageService} from '../crud-local-storage.service';
import {CrudStateService} from '../crud-state.service';
import {CrudInstance} from '../types/crud-types';
import {CrudInstanceComponent} from './crud-instance.component';

@Injectable()
export class CrudInstanceService {

  private _id: string = '';
  private _instance: CrudInstanceComponent | undefined = undefined;

  constructor(private crudInstancesService: CrudInstancesService,
              private crudStateService: CrudStateService,
              private crudLocalStorageService: CrudLocalStorageService) {
  }

  get id(): string {
    return this._id;
  }

  set id(id: string | undefined) {
    if (id) {
      this._id = id;
    } else {
      this._id = uuid();
    }
    // TODO: remove this?
    this.crudLocalStorageService.updatePartial(this.id, (instance: CrudInstance) => {
      instance.id = this.id;
    });
  }

  get instance(): CrudInstanceComponent | undefined {
    return this._instance;
  }

  set instance(instanceComponent: CrudInstanceComponent | undefined) {
    this._instance = instanceComponent;
  }

  activate(): void {
    // Deactivate all other instances.
    for (const instance of this.crudInstancesService.get()) {
      if (instance !== this.instance) {
        instance.crudStateService.active$.pipe(first()).subscribe((active) => {
          if (active) {
            instance.crudStateService.setActive(false);
          }
        });
      }
    }
    // Activate this instance!
    this.instance.crudStateService.active$.pipe(first()).subscribe((active) => {
      if (!active) {
        this.crudStateService.setActive(true);
      }
    });

    /*
    const instances = this.crudLocalStorageService.load();
    for (const id of objectKeys(instances)) {
      instances[id].active = false;
    }
    instances[this.id].active = true;
    this.crudLocalStorageService.store(instances);
    */
  }

}
