import {DOCUMENT} from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {faHistory, faWindowMaximize, faWindowMinimize} from '@fortawesome/pro-regular-svg-icons';
import {takeUntil} from 'rxjs/operators';
import {CrudAlertService} from '../crud-alert.service';
import {CrudComponent} from '../crud-component';
import {CrudConfig} from '../crud-config';
import {CrudConfigService} from '../crud-config.service';
import {CrudDataProviderService} from '../crud-data-provider/crud-data-provider.service';
import {CrudEventService} from '../crud-event.service';
import {CrudFocusable} from '../crud-focusable';
import {CrudIdService} from '../crud-id.service';
import {CrudInjectionService} from '../crud-injection.service';
import {CrudInputService} from '../crud-input.service';
import {CrudInstancesService} from '../crud-instances.service';
import {CrudLicensingService} from '../crud-licensing/crud-licensing.service';
import {CrudLocalStorageService} from '../crud-local-storage.service';
import {CrudTranslatorService} from '../crud-localization/crud-translator.service';
import {CrudLocationService} from '../crud-location.service';
import {CrudLoggingService, LogLevel} from '../crud-logging.service';
import {CrudModalComponent} from '../crud-modal/crud-modal.component';
import {CrudModalService} from '../crud-modal/crud-modal.service';
import {CrudRouterService} from '../crud-router.service';
import {CrudScrollService} from '../crud-scroll.service';
import {CrudStateService} from '../crud-state.service';
import {CrudToast} from '../crud-toasts/crud-toast';
import {CrudToastService} from '../crud-toasts/crud-toast.service';
import {CrudUser} from '../crud-user/crud-user';
import {CrudUserService} from '../crud-user/crud-user.service';
import {NestedInstanceConfig} from '../types/crud-types';
import {CrudView} from '../types/crud-view';
import {CrudInstanceService} from './crud-instance-service';

/** @dynamic */
@Component({
  selector: 'crud-instance',
  templateUrl: './crud-instance.component.html',
  styleUrls: ['./crud-instance.component.scss'],
  providers: [
    CrudLoggingService, CrudLocalStorageService, CrudInstanceService, CrudEventService,
    CrudInputService, CrudRouterService, CrudLocationService, CrudConfigService, CrudStateService, CrudUserService,
    CrudDataProviderService, CrudAlertService, CrudModalService, CrudInjectionService,
    CrudTranslatorService, CrudLicensingService, CrudScrollService, CrudIdService
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudInstanceComponent extends CrudComponent implements OnInit, AfterViewInit, OnDestroy, CrudFocusable {

  static logger: CrudLoggingService = new CrudLoggingService().init().setLogLevel(LogLevel.TRACE);

  @Input()
  set title(title: string) {
    this.crudInputService.setTitle(title);
  }

  @Input()
  set theme(themeName: string) {
    this.crudInputService.theme = themeName;
  }

  @Input()
  set themeOverriders(themeOverriders: string | string[] | undefined) {
    this.crudInputService.themeOverriders = themeOverriders;
  }

  @Input()
  set config(config: CrudConfig) {
    this.crudInputService.config = config;
  }

  @Input()
  set nestedInstanceConfig(config: NestedInstanceConfig) {
    this.crudInputService.nestedInstanceConfig = config;
  }

  @Input()
  set currentUser(currentUser: CrudUser) {
    this.crudInputService.currentUser = currentUser;
  }

  @Input()
  set instanceId(instanceId: string | undefined) {
    // TODO: is id set if no id is given when <crud-instance> does not contain instanceId info?
    this.crudInstanceService.id = instanceId;
  }

  @Input()
  set parent(parent: CrudInstanceComponent | undefined) {
    this.crudInputService.parent = parent;
  }

  @ViewChild(CrudModalComponent)
  modalContainer: CrudModalComponent;

  @HostBinding('tabindex')
  tabindex = 0;

  /**
   * The title of this instance.
   * Always contains the last value of the InputService's title$ observable.
   */
  private _title: string = '';

  /**
   * Whether or not this instance is currently active.
   * Always contains the last value of the StateService's active$ observable.
   */
  private _active: boolean = false;

  /**
   * The current view.
   * Always contains the last value of the StateService's view$ observable.
   */
  private _view: CrudView = undefined;

  views = CrudView;

  /**
   * Whether or not this instance is licensed.
   * Always contains the last value of the InstanceService's licensed$ observable.
   */
  private _licensed: boolean = false;

  fullscreen: boolean = false;

  documentClickHandlerFunction;
  instanceClickHandlerFunction;
  keydownHandlerFunction;

  faWindowMaximize = faWindowMaximize;
  faWindowMinimize = faWindowMinimize;
  faHistory = faHistory;

  constructor(private crudInstancesService: CrudInstancesService,
              public crudInstanceService: CrudInstanceService,
              private crudEventService: CrudEventService,
              public crudInputService: CrudInputService,
              private crudRouterService: CrudRouterService,
              public crudConfigService: CrudConfigService,
              public crudStateService: CrudStateService,
              private crudUserService: CrudUserService,
              private crudDataProviderService: CrudDataProviderService,
              private crudModalService: CrudModalService,
              public crudLicensingService: CrudLicensingService,
              private crudToastService: CrudToastService,
              private elementRef: ElementRef,
              private zone: NgZone,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService,
              @Inject(DOCUMENT) private document: HTMLDocument) {
    super(cdr, logger);
    cdr.detach();

    // Early-initialize the logger (using a default name) to make it usable in the instance constructor.
    logger.init();

    crudInstancesService.register(this);
    crudInstanceService.instance = this;
    crudStateService.registerConfigService(crudConfigService);
    crudRouterService.registerInstance(this);

    this.instanceClickHandlerFunction = this.handleInstanceClick.bind(this);
    this.documentClickHandlerFunction = this.handleDocumentClick.bind(this);
    this.keydownHandlerFunction = this.handleKeydown.bind(this);
  }

  ngOnInit(): void {
    // Re-initialize the logger with the name of this instance as it is only determined in the constructor.
    this.logger.init(this.crudInstanceService.id);

    // First initialize the configuration. This will set the appropriate data provider.
    // console.log('initializing ', this, ' with configuration ', this.crudInputService.config);
    this.crudConfigService.initializeFrom(this.crudInputService.config);

    /* CONFIG and STATE are now initialized! */

    // The user specified the log level!
    this.logger.setLogLevel(this.crudConfigService.config.getLogLevel());

    // For now, we just want to start in the LIST view.
    this.crudStateService.setView(CrudView.LIST);

    // Then initialize the data provider facades.
    this.crudDataProviderService.initFacades();

    /* Listen for updates. */

    this.crudInputService.title$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((title) => {
        this._title = title;
        this.cdr.detectChanges();
      });

    this.crudStateService.active$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((active) => {
        this._active = active;
        this.cdr.detectChanges();
      });

    this.crudStateService.view$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((view: CrudView) => {
        this._view = view;
        this.cdr.detectChanges();
      });

    this.crudLicensingService.licensed$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((licensed: boolean) => {
        this._licensed = licensed;
        this.cdr.detectChanges();
      });

    // Keep a copy of the current fullscreen state to be able to use @HostBinding.
    this.crudStateService.fullscreen$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((fullscreen) => {
        this.fullscreen = fullscreen;
        this.cdr.detectChanges();
      });

    // Activate this instance after it was clicked.
    this.crudEventService.instanceClick$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event: MouseEvent) => {
        this.crudInstanceService.activate();
        // Stopping the propagation is necessary.
        // Otherwise a click inside a nested instance would propagate and active the topmost parent in the stack.
        event.stopPropagation();
      });

    // Register global event listeners.
    this.zone.runOutsideAngular(() => {
      this.elementRef.nativeElement.addEventListener('click', this.instanceClickHandlerFunction);
      this.document.addEventListener('click', this.documentClickHandlerFunction);
      this.document.addEventListener('keydown', this.keydownHandlerFunction);
    });

    this.crudToastService.add(new CrudToast('info', 'Instance', 'created', false));

    /*
    this.crudStateService.recentlyCreatedEntries$.pipe(takeUntil(this.unsubscribe$))
      .subscribe((_: LinkedList<Entry>) =>
        this.crudToastService.add(new CrudToast('info', 'Entry', 'created', false))
      );
    this.crudStateService.recentlyUpdatedEntries$.pipe(takeUntil(this.unsubscribe$))
      .subscribe((_: LinkedList<Entry>) =>
        this.crudToastService.add(new CrudToast('info', 'Entry', 'updated', false))
      );
    */
  }

  ngAfterViewInit(): void {
    this.crudModalService.setModalContainerForInjection(this.modalContainer);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();

    this.crudInstancesService.unregister(this);

    // Unregister the global event listeners.
    this.zone.runOutsideAngular(() => {
      this.elementRef.nativeElement.removeEventListener('click', this.instanceClickHandlerFunction);
      this.document.removeEventListener('click', this.documentClickHandlerFunction);
      this.document.removeEventListener('keydown', this.keydownHandlerFunction);
    });
  }

  handleInstanceClick(event: MouseEvent): void {
    // Event handler runs outside of angular!
    this.crudEventService.handleInstanceClick(event);
  }

  handleDocumentClick(event: MouseEvent): void {
    // Event handler runs outside of angular!
    this.crudEventService.handleDocumentClick(event);
  }

  handleKeydown(event: KeyboardEvent): void {
    // Event handler runs outside of angular!
    this.crudEventService.handleKeydown(event);
  }

  @HostBinding('class')
  get hostClasses(): string {
    return [
      this.crudInputService.theme + '-theme',
      this.themeOverriders ? [].concat(this.themeOverriders).join(' ') : ''
    ].join(' ');
  }

  @HostBinding('class.collapsible')
  get isCollapsible(): boolean {
    return this.crudConfigService.config.isCollapsible();
  }

  @HostBinding('class.fullscreen')
  get fullscreenClass(): boolean {
    return this.fullscreen;
  }

  toggleFullscreen(event: MouseEvent): void {
    this.crudStateService.toggleFullscreen();
    // Stopping the propagation is necessary to not toggle the collapsible header.
    event.preventDefault();
    event.stopPropagation();
    // We still want to register the click inside the instance!
    this.handleInstanceClick(event);
  }

  focus(): void {
    this.elementRef.nativeElement.focus();
  }

  blur(): void {
    this.elementRef.nativeElement.blur();
  }

  isFocused(): boolean {
    return this.elementRef.nativeElement === this.document.activeElement;
  }

  getNativeElement(): Element {
    return this.elementRef.nativeElement;
  }

  showHistory(event: MouseEvent): void {
    this.crudStateService.setView(CrudView.HISTORY);
    event.stopPropagation();
    // We still want to register the click inside the instance!
    this.handleInstanceClick(event);
  }

  // eslint-disable-next-line @typescript-eslint/adjacent-overload-signatures
  get title(): string {
    return this._title;
  }

  get active(): boolean {
    return this._active;
  }

  get view(): CrudView {
    return this._view;
  }

  get licensed(): boolean {
    return this._licensed;
  }

  detectChanges(): void {
    this.cdr.detectChanges();
  }

  getComponentName(): string {
    return 'instance';
  }

}
