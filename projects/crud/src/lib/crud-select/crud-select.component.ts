import {DOCUMENT} from '@angular/common';
import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../crud-component';
import {CrudEventService} from '../crud-event.service';
import {CrudEventFiltersService} from '../crud-event/crud-event-filters.service';
import {CrudFocusable} from '../crud-focusable';
import {CrudTranslatorService} from '../crud-localization/crud-translator.service';
import {CrudLoggingService} from '../crud-logging.service';
import {CrudSelectDropdownComponent} from './crud-select-dropdown/crud-select-dropdown.component';
import {CrudSelectSelectedComponent} from './crud-select-selected/crud-select-selected.component';
import {Choice, ChoiceBuilder, ChoicesGroup} from './state/choices-types';
import {ChoicesService} from './state/choices.service';
import {CrudSelectService, CrudSelectType} from './state/crud-select.service';

/** @dynamic */
@Component({
  selector: 'crud-select',
  templateUrl: './crud-select.component.html',
  styleUrls: ['./crud-select.component.scss'],
  providers: [
    CrudSelectService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CrudSelectComponent),
      multi: true
    },
    CrudEventFiltersService
  ]
})
export class CrudSelectComponent extends CrudComponent implements OnInit, CrudFocusable, ControlValueAccessor {

  @ViewChild(CrudSelectSelectedComponent)
  selectSelected: CrudSelectSelectedComponent;

  @ViewChild(CrudSelectDropdownComponent)
  selectDropdown: CrudSelectDropdownComponent;

  @Input()
  set type(type: 'select' | 'multiselect') {
    this.crudSelectService.setType(type);
  }

  @Input()
  set disabled(disabled: boolean) {
    this.crudSelectService.setDisabled(disabled);
  }

  @Input()
  private allowDeselect: boolean = false;

  @Input()
  set choices(choices: Choice[]) {
    this.crudSelectService.setChoicesGroups(this.choicesService.cloneChoicesGroups([
      {
        label: '',
        id: 1,
        choices
      }
    ]));
  }

  @Input()
  set choicesGroups(choicesGroups: ChoicesGroup[]) {
    this.crudSelectService.setChoicesGroups(this.choicesService.cloneChoicesGroups(choicesGroups));
  }

  @Output()
  open: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  close: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  selection: EventEmitter<Choice | Choice[] | undefined> = new EventEmitter<Choice | Choice[] | undefined>();

  @Output()
  selectionValues: EventEmitter<string> = new EventEmitter<string>();

  private _choicesGroups: ChoicesGroup[];
  _type: CrudSelectType = 'select';
  _disabled: boolean = false; // From CrudSelectService#disabled$
  menuOpen: boolean = false; // From CrudSelectService#menuOpen$

  translationPlaceholder: string = '';
  translationSearchPlaceholder: string = '';

  private _selected: Choice | Choice[] | undefined; // From CrudSelectService#selected$
  private _preselected: Choice | undefined; // From CrudSelectService#preselected$

  private _clickedInside: boolean = false;

  @HostBinding('tabindex')
  private tabindex = 0;

  constructor(private crudSelectService: CrudSelectService,
              private crudEventService: CrudEventService,
              private crudEventFiltersService: CrudEventFiltersService,
              private crudTranslatorService: CrudTranslatorService,
              private choicesService: ChoicesService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService,
              private elementRef: ElementRef,
              @Inject(DOCUMENT) private document: HTMLDocument) {
    super(cdr, logger);
    cdr.detach();
    this.translationPlaceholder = crudTranslatorService.translate('select.placeholder');
    this.translationSearchPlaceholder = crudTranslatorService.translate('select.searchPlaceholder');

    this.crudEventFiltersService.register({
      'keydown.esc': () => this.menuOpen,
      'keydown.tab': () => this.menuOpen,
      'keydown.Space': () => !this.menuOpen,
      'keydown.Enter': () => !this.menuOpen,
      'keydown.ArrowDown': () => !this.menuOpen
    });
  }

  @HostListener('click')
  onClickInside(): void {
    this._clickedInside = true;
  }

  @HostBinding('style.opacity')
  get opacity(): number {
    return this._disabled ? 0.5 : 1;
  }

  @HostListener('keydown.esc.filter.stop')
  @HostListener('keydown.tab.filter.stop')
  private onEscapeOrTab(): void {
    this.closeMenu();
  }

  @HostListener('keydown.Space.filter.stop.prevent')
  @HostListener('keydown.Enter.filter.stop.prevent')
  @HostListener('keydown.ArrowDown.filter.stop.prevent')
  private onSpaceEnterOrArrowDown(): void {
    this.openMenu();
  }

  ngOnInit(): void {
    this.crudEventService.documentClick$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((_) => {
        if (this.menuOpen) {
          // Clicked outside!
          this.closeMenu();
        }
        this._clickedInside = false;
      });

    this.crudSelectService.choicesGroups$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(choicesGroups => {
        const notSetBefore = !this._choicesGroups;
        this._choicesGroups = choicesGroups;
        if (notSetBefore) {
          this.searchAndSelectInitiallySelectedChoices();
        }
        this.cdr.detectChanges();
      });

    this.crudSelectService.type$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(selectType => {
        if (selectType === 'select') {
          this.switchToSelect();
        } else if (selectType === 'multiselect') {
          this.switchToMultiselect();
        } else {
          this.logger.warn('Unknown type!');
        }
      });

    this.crudSelectService.disabled$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(disabled => {
        this._disabled = disabled;
      });

    this.crudSelectService.preselected$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(preselected => {
        this._preselected = preselected;
        this.cdr.detectChanges();
      });

    this.crudSelectService.selected$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(([selected, isUserChange]) => {
        this._selected = selected;
        this.cdr.detectChanges();
        if (isUserChange) {
          this.selection.emit(this._selected);
        }
        this.propagateSelectedValueChanges(this.selectedValues);
      });

    this.crudSelectService.menuOpen$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(menuOpen => {
        this.menuOpen = menuOpen;
        this.cdr.detectChanges();
      });

    this.crudSelectService.menuShouldOpen$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.openMenu();
      });

    this.crudSelectService.menuShouldClose$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.closeMenu();
      });
  }

  focus(): void {
    this.elementRef.nativeElement.focus();
  }

  blur(): void {
    this.elementRef.nativeElement.blur();
  }

  isFocused(): boolean {
    return this.elementRef.nativeElement === this.document.activeElement;
  }

  getNativeElement(): Element {
    return this.elementRef.nativeElement;
  }

  openMenu(): void {
    if (!this.menuOpen) {
      this.crudSelectService.setMenuOpen();
      this.open.emit();
    }
  }

  closeMenu(): void {
    if (this.menuOpen) {
      // Opening the menu puts the focus on the search input.
      // On close, the focus on the select itself should be restored, as the user might still want to interact with it
      // or want to tab further down the focusable elements on the site.
      // If the menu is closed by pressing escape,
      // the search input is still focused and the focus can be restored safely.
      // If the menu is closed with a click outside the select menu and onto a focusable element, restoring focus
      // to the select would probably be against the users intention / will.
      if (!!this.selectDropdown.searchInput && !!this.selectDropdown.searchInput.nativeElement
        && !!this.document.activeElement
        && (this.document.activeElement === this.document.body
          || this.document.activeElement === this.document.body.parentElement
          || this.document.activeElement === this.selectDropdown.searchInput.nativeElement)) {
        this.focus();
      }
      this.crudSelectService.setMenuClosed();
      if (this.isSelect()) {
        this.crudSelectService.resetSearchString();
      }
      this.close.emit();
    }
  }

  toggleMenu(): void {
    // Do not let the user interact with the select if it is disabled.
    if (this._disabled) {
      return;
    }
    if (this.menuOpen) {
      this.closeMenu();
    } else {
      this.openMenu();
    }
  }

  select(choice: Choice): void {
    if (choice.selected) {
      return;
    }
    this.checkSelectedType();
    const updatedChoice = ChoiceBuilder.from(choice).selected(true).build();
    if (this._preselected?.value === updatedChoice.value) {
      this.crudSelectService.setPreselected(updatedChoice);
    }
    if (this.isSelect()) {
      // Deselect any previously selected element!
      if (this._selected) {
        this.crudSelectService.updateChoice(
          this._selected as Choice,
          ChoiceBuilder.from(this._selected as Choice).selected(false).build()
        );
      }
      this.crudSelectService.updateChoice(choice, updatedChoice);
      this.crudSelectService.setSelected(updatedChoice);
      // We should reset the search input when a choice is selected in single select mode.
      // The menu is not needed anymore and can be closed.
      // In multiselect mode, the menu should stay open and the search input must not be reset.
      this.closeMenu();
      this.crudSelectService.resetSearchString();
    } else if (this.isMultiselect()) {
      this.crudSelectService.updateChoice(choice, updatedChoice);
      this.crudSelectService.pushSelected(updatedChoice);
    } else {
      console.error('Unknown select mode. Cannot process selection.');
    }
  }

  deselect(choice: Choice): void {
    if (!choice.selected) {
      return;
    }
    this.checkSelectedType();
    const updatedChoice = ChoiceBuilder.from(choice).selected(false).build();
    if (this._preselected?.value === updatedChoice.value) {
      this.crudSelectService.setPreselected(updatedChoice);
    }
    if (this.isSelect()) {
      if (this.allowDeselect && (this._selected as Choice).value === choice.value) {
        this.crudSelectService.updateChoice(choice, updatedChoice);
        this.crudSelectService.setSelected(undefined);
        // We should reset the search input when a choice is selected in single select mode.
        // The menu is not needed anymore and can be closed.
        // In multiselect mode, the menu should stay open and the search input must not be reset.
        this.closeMenu();
        this.crudSelectService.resetSearchString();
      }
    } else if (this.isMultiselect()) {
      for (let i = 0; i < (this._selected as Choice[]).length; i++) {
        if (this._selected[i].value === choice.value) {
          this.crudSelectService.updateChoice(choice, updatedChoice);
          this.crudSelectService.removeSelectedAt(i);
        }
      }
    } else {
      console.error('Unknown select mode. Cannot process selection.');
    }
  }

  isSelect(): boolean {
    return this._type === 'select';
  }

  isMultiselect(): boolean {
    return this._type === 'multiselect';
  }

  private switchToSelect(): void {
    if (this.isSelect()) {
      return;
    }
    this._type = 'select';
    if (Array.isArray(this._selected)) {
      if (this._selected.length !== 0) {
        // Deselect all choices above the first one. Do not trigger change detection every single time, as setSelected does that later.
        for (let i = 1; i < this._selected.length; i++) {
          this.crudSelectService.updateChoice(this._selected[i], ChoiceBuilder.from(this._selected[i]).selected(false).build());
        }
        this.crudSelectService.setSelected(this._selected[0]);
      } else {
        this.crudSelectService.setSelected(undefined);
      }
    } else {
      this.logger.warn('Expected an array of selected choices but variable was not an array!');
      this.cdr.detectChanges();
    }
  }

  private switchToMultiselect(): void {
    if (this.isMultiselect()) {
      return;
    }
    this._type = 'multiselect';
    if (!this._selected) {
      this.crudSelectService.setSelected([]);
    } else if (!Array.isArray(this._selected)) {
      this.crudSelectService.setSelected([this._selected]);
    } else {
      this.logger.warn('Expected a single choice element but variable was an array.');
      this.cdr.detectChanges();
    }
  }

  private searchAndSelectInitiallySelectedChoices(): void {
    if (this.isSelect()) {
      this._choicesGroups.forEach(group => group.choices.forEach(choice => {
        if (choice.selected) {
          this.crudSelectService.setSelected(choice, false);
        }
      }));
    } else if (this.isMultiselect()) {
      const accumulated: Choice[] = [];
      this._choicesGroups.forEach(group => group.choices.forEach(choice => {
        if (choice.selected) {
          accumulated.push(choice);
        }
      }));
      this.crudSelectService.setSelected(accumulated, false);
    } else {
      this.logger.error('Unable to searchAndSelectInitiallySelectedChoices for select of type', this._type);
    }
  }

  private trySelectChoiceByValue(value: string): void {
    const choice: Choice | undefined = this.choicesService.find(value, this._choicesGroups);
    if (choice) {
      if (!choice.selected) {
        this.select(choice);
      }
    } else {
      // console.warn('Tried to select a choice by value. No choice was found for value: ', value);
    }
  }

  private checkSelectedType(): void {
    if (this.isSelect() && Array.isArray(this._selected)) {
      this.logger.warn('Type is "select" but _selected was an array. Correcting the mistake.');
      this.crudSelectService.setSelected(undefined);
    } else if (this.isMultiselect() && !Array.isArray(this._selected)) {
      this.logger.warn('Type is "multiselect" but _selected was not an array. Correcting the mistake.');
      this.crudSelectService.setSelected([]);
    }
  }

  get selected(): Choice | Choice[] | undefined {
    return this._selected;
  }

  get selectedValues(): string {
    if (!this._selected) {
      return '';
    }
    if (Array.isArray(this._selected)) {
      return this._selected.map(choice => choice.value).join(',');
    }
    return this._selected.value;
  }

  propagateSelectedValueChanges(selectedValues: string): void {
    this.selectionValues.emit(selectedValues);
    this.ngFormsPropagateChange(selectedValues);
  }

  /* ControlValueAccessor implementation */

  // The argument to this function is propagated to the form control value!
  ngFormsPropagateChange: (selectedValues: string) => void = (_: string) => {
    // Do nothing.
  };

  registerOnChange(fn: (selectedValues: string) => void): void {
    this.ngFormsPropagateChange = fn;
  }

  registerOnTouched(_: unknown): void {
    // Original parameter name: fn
    // On blur, etc...
    // We don't care right now.
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: unknown): void {
    if (typeof obj !== 'string') {
      console.error('Unable to write value to select. Not a string.', obj);
      return;
    }
    obj.split(',').map(part => part.trim()).forEach(value => {
      this.trySelectChoiceByValue(value);
    });
  }

  getComponentName(): string {
    return 'select';
  }

}
