import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudSelectDropdownComponent } from './crud-select-dropdown.component';

describe('CrudSelectDropdownComponent', () => {
  let component: CrudSelectDropdownComponent;
  let fixture: ComponentFixture<CrudSelectDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudSelectDropdownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudSelectDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
