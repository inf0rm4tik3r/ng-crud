import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../../crud-component';
import {CrudEventFiltersService} from '../../crud-event/crud-event-filters.service';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudSelectMenuComponent} from '../crud-select-menu/crud-select-menu.component';
import {Choice} from '../state/choices-types';
import {CrudSelectService} from '../state/crud-select.service';

@Component({
  selector: 'crud-select-dropdown',
  templateUrl: './crud-select-dropdown.component.html',
  styleUrls: ['./crud-select-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudSelectDropdownComponent extends CrudComponent implements OnInit {

  @ViewChild('searchInput')
  searchInput: ElementRef<HTMLInputElement> = undefined;

  @ViewChild(CrudSelectMenuComponent)
  menu: CrudSelectMenuComponent;

  @Input()
  translationSearchPlaceholder: string = '';

  @Output()
  select: EventEmitter<Choice> = new EventEmitter<Choice>();

  @Output()
  deselect: EventEmitter<Choice> = new EventEmitter<Choice>();

  searchString: string = '';
  preselected: Choice | undefined = undefined;

  constructor(private crudSelectService: CrudSelectService,
              private crudEventFiltersService: CrudEventFiltersService,
              public cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);

    crudEventFiltersService.register({
      'keydown.Enter': () => !!this.preselected
    });
  }

  ngOnInit(): void {
    this.crudSelectService.searchString$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(searchString => {
        this.searchString = searchString;
        this.cdr.detectChanges();
      });

    this.crudSelectService.preselected$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(preselected => {
        this.preselected = preselected;
        this.cdr.detectChanges();
      });

    this.crudSelectService.menuOpen$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        if (this.searchInput) {
          this.searchInput.nativeElement.focus();
        }
      });
  }

  @HostListener('keydown.ArrowUp.stop')
  onArrowUp(): void {
    this.menu.selectPreviousChoice();
  }

  @HostListener('keydown.ArrowDown.stop')
  onArrowDown(): void {
    this.menu.selectNextChoice();
  }

  @HostListener('keydown.Enter.filter.stop')
  onEnter(): void {
    if (this.preselected.selected === true) {
      this.deselect.emit(this.preselected);
    } else {
      this.select.emit(this.preselected);
    }
  }

  updateSearchString(newSearchString: string): void {
    this.crudSelectService.setSearchString(newSearchString);
  }

  getComponentName(): string {
    return 'select-dropdown';
  }

}
