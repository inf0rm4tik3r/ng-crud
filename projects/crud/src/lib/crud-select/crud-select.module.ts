import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CrudEventModule} from '../crud-event/crud-event.module';
import {CrudSelectDropdownComponent} from './crud-select-dropdown/crud-select-dropdown.component';
import {CrudSelectItemComponent} from './crud-select-item/crud-select-item.component';
import {CrudSelectMenuComponent} from './crud-select-menu/crud-select-menu.component';
import {CrudSelectSelectedComponent} from './crud-select-selected/crud-select-selected.component';
import {CrudSelectComponent} from './crud-select.component';

@NgModule({
  exports: [
    CrudSelectComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CrudEventModule
  ],
  declarations: [
    CrudSelectComponent,
    CrudSelectItemComponent,
    CrudSelectMenuComponent,
    CrudSelectDropdownComponent,
    CrudSelectSelectedComponent
  ]
})
export class CrudSelectModule {
}
