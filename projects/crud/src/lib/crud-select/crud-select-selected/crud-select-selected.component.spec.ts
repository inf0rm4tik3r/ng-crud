import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudSelectSelectedComponent } from './crud-select-selected.component';

describe('CrudSelectSelectedComponent', () => {
  let component: CrudSelectSelectedComponent;
  let fixture: ComponentFixture<CrudSelectSelectedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudSelectSelectedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudSelectSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
