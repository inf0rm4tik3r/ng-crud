import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../../crud-component';
import {CrudLoggingService} from '../../crud-logging.service';
import {Choice} from '../state/choices-types';
import {CrudSelectService, CrudSelectType} from '../state/crud-select.service';

@Component({
  selector: 'crud-select-selected',
  templateUrl: './crud-select-selected.component.html',
  styleUrls: ['./crud-select-selected.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudSelectSelectedComponent extends CrudComponent implements OnInit {

  @Input()
  type: CrudSelectType | undefined = undefined;

  @Input()
  @HostBinding('class.disabled')
  disabled: boolean | undefined = undefined;

  @Input()
  translationPlaceholder: string = '';

  @Output()
  toggleMenu: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  deselect: EventEmitter<Choice> = new EventEmitter<Choice>();

  selected: Choice | Choice[] | undefined = undefined; // From CrudSelectService#selected$

  // For use in template.
  array = Array;

  constructor(private crudSelectService: CrudSelectService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  ngOnInit(): void {
    this.crudSelectService.selected$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(([selected, _]) => {
        this.selected = selected;
        this.cdr.detectChanges();
      });
  }

  @HostBinding('class.type-select')
  private isTypeSelect(): boolean {
    return this.type === 'select';
  }

  @HostBinding('class.type-multiselect')
  private isTypeMultiselect(): boolean {
    return this.type === 'multiselect';
  }

  @HostListener('click')
  private click(): void {
    this.toggleMenu.emit();
  }

  labelClicked(event: Event, choice: Choice): void {
    event.stopPropagation();
    this.deselect.emit(choice);
  }

  getComponentName(): string {
    return 'select-selected';
  }

}
