export class ChoiceBuilder {
  private _value: string;
  private _label: string;
  private _selected?: boolean;

  value(value: string): ChoiceBuilder {
    this._value = value;
    return this;
  }

  label(label: string): ChoiceBuilder {
    this._label = label;
    return this;
  }

  selected(selected?: boolean): ChoiceBuilder {
    this._selected = selected;
    return this;
  }

  build(): Choice {
    return {
      value: this._value,
      label: this._label,
      selected: this._selected
    };
  }

  static from(choice: Choice): ChoiceBuilder {
    return new ChoiceBuilder()
      .value(choice.value)
      .label(choice.label)
      .selected(choice.selected);
  }
}

export interface Choice {
  value: string;
  label: string;
  selected?: boolean;
}

export interface ChoicesGroup {
  label: string;
  id: number;
  choices: Choice[];
}

export interface LocalizedChoices {
  [languageIdentifier: string]: Choice[];
}

export interface LocalizedChoicesGroups {
  [languageIdentifier: string]: ChoicesGroup[];
}
