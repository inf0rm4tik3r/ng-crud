import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {Choice, ChoicesGroup} from './choices-types';

export type CrudSelectType = 'select' | 'multiselect';

@Injectable()
export class CrudSelectService {

  private readonly _choicesGroups: BehaviorSubject<ChoicesGroup[]> = new BehaviorSubject<ChoicesGroup[]>([]);
  public readonly choicesGroups$: Observable<ChoicesGroup[]> = this._choicesGroups.asObservable();

  private readonly _type: BehaviorSubject<CrudSelectType> = new BehaviorSubject<CrudSelectType>('select');
  public readonly type$: Observable<CrudSelectType> = this._type.asObservable();

  private readonly _disabled: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public readonly disabled$: Observable<boolean> = this._disabled.asObservable();

  private readonly _searchString: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public readonly searchString$: Observable<string> = this._searchString.asObservable();

  private readonly _preselected: BehaviorSubject<Choice | undefined> = new BehaviorSubject<Choice | undefined>(undefined);
  public readonly preselected$: Observable<Choice | undefined> = this._preselected.asObservable();

  private readonly _selected: BehaviorSubject<[Choice | Choice[] | undefined, boolean]>
    = new BehaviorSubject<[Choice | Choice[] | undefined, boolean]>([undefined, false]);
  public readonly selected$: Observable<[Choice | Choice[] | undefined, boolean]> = this._selected.asObservable();

  private readonly _menuOpen: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public readonly menuOpen$: Observable<boolean> = this._menuOpen.asObservable();

  private readonly _menuShouldOpen: Subject<void> = new Subject<void>();
  public readonly menuShouldOpen$: Observable<void> = this._menuShouldOpen.asObservable();

  private readonly _menuShouldClose: Subject<void> = new Subject<void>();
  public readonly menuShouldClose$: Observable<void> = this._menuShouldClose.asObservable();

  setChoicesGroups(choicesGroups: ChoicesGroup[]): void {
    this._choicesGroups.next(choicesGroups);
  }

  updateChoice(choice: Choice, newChoice: Choice): void {
    const choicesGroups = this._choicesGroups.getValue();
    for (let g = 0; g < choicesGroups.length; g++) {
      for (let c = 0; c < choicesGroups[g].choices.length; c++) {
        const current = choicesGroups[g].choices[c];
        if (current.value === choice.value) {
          choicesGroups[g].choices[c] = newChoice;
          this._choicesGroups.next(choicesGroups);
          return;
        }
      }
    }
    throw new Error('Could not update choice. Choice was not found!');
  }

  setType(type: CrudSelectType): void {
    this._type.next(type);
  }

  resetSearchString(): void {
    this.setSearchString('');
  }

  setSearchString(searchString: string): void {
    this._searchString.next(searchString);
  }

  setPreselected(choice: Choice | undefined): void {
    this._preselected.next(choice);
  }

  setSelected(selected: Choice | Choice[] | undefined, isUserChange: boolean = true): void {
    this._selected.next([selected, isUserChange]);
  }

  pushSelected(selected: Choice, isUserChange: boolean = true): void {
    const array = this._selected.getValue() as Choice[];
    array.push(selected);
    this._selected.next([array, isUserChange]);
  }

  removeSelectedAt(index: number, amount: number = 1, isUserChange: boolean = true): void {
    const array = this._selected.getValue() as Choice[];
    array.splice(index, amount);
    this._selected.next([array, isUserChange]);
  }

  setDisabled(disabled: boolean): void {
    this._disabled.next(disabled);
    if (disabled && this._menuOpen.getValue()) {
      this._menuShouldClose.next();
    }
  }

  setMenuOpen(): void {
    this._menuOpen.next(true);
  }

  setMenuClosed(): void {
    this._menuOpen.next(false);
  }

}
