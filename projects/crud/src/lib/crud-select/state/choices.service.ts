import {Injectable} from '@angular/core';
import {Choice, ChoicesGroup} from './choices-types';

@Injectable({
  providedIn: 'root'
})
export class ChoicesService {

  find(value: string, groups: ChoicesGroup[]): Choice | undefined {
    for (const group of groups) {
      for (const choice of group.choices) {
        if (choice.value === value) {
          return choice;
        }
      }
    }
    return undefined;
  }

  cloneChoicesGroups(groups: ChoicesGroup[]): ChoicesGroup[] {
    const clonedGroups: ChoicesGroup[] = [];
    for (const group of groups) {
      clonedGroups.push({
        label: group.label,
        id: group.id,
        choices: this.cloneChoices(group.choices)
      });
    }
    return clonedGroups;
  }

  cloneChoices(choices: Choice[]): Choice[] {
    const clonedChoices: Choice[] = [];
    for (const choice of choices) {
      clonedChoices.push({
        value: choice.value,
        label: choice.label,
        selected: choice.selected
      });
    }
    return clonedChoices;
  }

  filtered(choicesGroups: ChoicesGroup[], filter: string | undefined): ChoicesGroup[] {
    return !filter || filter.length === 0
      ? choicesGroups
      : choicesGroups
        // Create copies of the original choices groups...
        .map((choicesGroup: ChoicesGroup) => {
          return {
            id: choicesGroup.id,
            label: choicesGroup.label,
            // in which unnecessary choices are filtered out.
            choices: choicesGroup.choices.filter((choice) =>
              choice.label.toLowerCase().includes(filter.toLowerCase()))
          };
        })
        // Every empty group must not be displayed.
        .filter((choicesGroup) => choicesGroup.choices.length > 0);
  }

  calculatePreviousChoice(groups: ChoicesGroup[], current: Choice | undefined): Choice {
    if (!current) {
      return ChoicesService.firstChoice(groups);
    }
    let selectNext = false;
    for (let g = groups.length - 1; g >= 0; g--) {
      const group = groups[g];
      for (let c = group.choices.length - 1; c >= 0; c--) {
        const choice = group.choices[c];
        if (selectNext) {
          return choice;
        }
        if (choice.value === current.value) {
          selectNext = true;
        }
      }
    }
    // If currently selected could not be found in options...
    if (!selectNext) {
      // console.error('Could not select previous choice. Currently selected was not found and no choices are present!');
      return ChoicesService.firstChoice(groups);
    }
    // Wrap around to last choice.
    const lastGroup = groups[groups.length - 1];
    return lastGroup.choices[lastGroup.choices.length - 1];
  }

  calculateNextChoice(groups: ChoicesGroup[], current: Choice | undefined): Choice {
    if (!current) {
      return ChoicesService.firstChoice(groups);
    }
    let selectNext = false;
    for (const group of groups) {
      for (const choice of group.choices) {
        if (selectNext) {
          return choice;
        }
        if (choice.value === current.value) {
          selectNext = true;
        }
      }
    }
    if (!selectNext) {
      // console.error('Could not select next choice. Currently selected was not found and no choices are present!!');
      return ChoicesService.firstChoice(groups);
    }
    // Wrap around to first choice.
    return groups[0].choices[0];
  }

  private static firstChoice(groups: ChoicesGroup[]): Choice | undefined {
    if (groups && groups.length > 0) {
      if (groups[0] && groups[0].choices && groups[0].choices.length > 0) {
        return groups[0].choices[0];
      }
    }
    return undefined;
  }

}
