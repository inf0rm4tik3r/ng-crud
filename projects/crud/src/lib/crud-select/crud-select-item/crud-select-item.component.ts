import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, HostBinding, HostListener, Input, Output} from '@angular/core';
import {CrudComponent} from '../../crud-component';
import {CrudLoggingService} from '../../crud-logging.service';
import {Choice} from '../state/choices-types';

@Component({
  selector: 'crud-select-item',
  templateUrl: './crud-select-item.component.html',
  styleUrls: ['./crud-select-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudSelectItemComponent extends CrudComponent {

  @Input()
  choice: Choice;

  @Input()
  @HostBinding('class.pre-selected')
  isPreselected: boolean = false;

  @Input()
  translationItemSelected: string = '';

  @Output()
  select: EventEmitter<Choice> = new EventEmitter<Choice>();

  @Output()
  deselect: EventEmitter<Choice> = new EventEmitter<Choice>();

  constructor(protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
  }

  @HostBinding('class.selected')
  get selected(): boolean {
    return this.choice.selected;
  }

  @HostListener('click')
  selectOrDeselect(): void {
    if (this.choice.selected === true) {
      this.deselect.emit(this.choice);
    } else {
      this.select.emit(this.choice);
    }
  }

  getComponentName(): string {
    return 'select-item';
  }

}
