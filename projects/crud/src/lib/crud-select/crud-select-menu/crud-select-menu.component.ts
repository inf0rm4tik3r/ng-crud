import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {v4 as uuid} from 'uuid';
import {CrudComponent} from '../../crud-component';
import {CrudTranslatorService} from '../../crud-localization/crud-translator.service';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudScrollService} from '../../crud-scroll.service';
import {Choice, ChoicesGroup} from '../state/choices-types';
import {ChoicesService} from '../state/choices.service';
import {CrudSelectService} from '../state/crud-select.service';

@Component({
  selector: 'crud-select-menu',
  templateUrl: './crud-select-menu.component.html',
  styleUrls: ['./crud-select-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudSelectMenuComponent extends CrudComponent implements OnInit {

  private _choicesGroups: ChoicesGroup[];
  private _filteredChoicesGroups: ChoicesGroup[];

  private _filter: string | undefined = undefined;

  @Input()
  set filter(filter: string | undefined) {
    this._filter = filter;
    this._filteredChoicesGroups = this.choicesService.filtered(this._choicesGroups, this._filter);
    this.cdr.detectChanges();
  }

  _preselected: Choice | undefined = undefined;

  @Input()
  set preselected(preselected: Choice | undefined) {
    this._preselected = preselected;
    this.scrollToChoice(preselected);
  }

  @Output()
  select: EventEmitter<Choice> = new EventEmitter<Choice>();

  @Output()
  deselect: EventEmitter<Choice> = new EventEmitter<Choice>();

  seed: string = uuid();

  translationNoSearchResults: string = '';
  translationItemSelected: string = '';

  constructor(private elementRef: ElementRef,
              private choicesService: ChoicesService,
              private crudSelectService: CrudSelectService,
              private crudScrollService: CrudScrollService,
              private crudTranslatorService: CrudTranslatorService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    this.translationNoSearchResults = this.crudTranslatorService.translate('select.noSearchResults');
    this.translationItemSelected = this.crudTranslatorService.translate('selectItem.selected');
  }

  ngOnInit(): void {
    this.crudSelectService.choicesGroups$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(choicesGroups => {
        this._choicesGroups = choicesGroups;
        this._filteredChoicesGroups = this.choicesService.filtered(this._choicesGroups, this._filter);
        this.cdr.detectChanges();
      });
  }

  selectPreviousChoice(): void {
    this.crudSelectService.setPreselected(this.choicesService.calculatePreviousChoice(this.filteredChoicesGroups, this._preselected));
  }

  selectNextChoice(): void {
    this.crudSelectService.setPreselected(this.choicesService.calculateNextChoice(this.filteredChoicesGroups, this._preselected));
  }

  private scrollToChoice(choice: Choice | undefined): void {
    if (choice && this.elementRef && this.elementRef.nativeElement) {
      this.crudScrollService.scrollToElementInContainer(
        this.elementRef.nativeElement, this.seed + '-' + choice.value, 'middle'
      );
    }
  }

  get filteredChoicesGroups(): ChoicesGroup[] {
    return this._filteredChoicesGroups;
  }

  getComponentName(): string {
    return 'select-menu';
  }

}
