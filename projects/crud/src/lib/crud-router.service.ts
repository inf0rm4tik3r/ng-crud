import {Injectable} from '@angular/core';
import {CrudInstanceComponent} from './crud-instance/crud-instance.component';
import {CrudLocationService} from './crud-location.service';
import {CrudStateService} from './crud-state.service';
import {CrudView} from './types/crud-view';

@Injectable()
export class CrudRouterService {

  private instance: CrudInstanceComponent;

  constructor(private crudLocationService: CrudLocationService,
              private crudStateService: CrudStateService) {
  }

  registerInstance(instance: CrudInstanceComponent): void {
    this.instance = instance;
  }

  openListView(): void {
    this.crudStateService.setView(CrudView.LIST);
  }

  openCreateView(): void {
    this.crudStateService.setView(CrudView.CREATE);
  }

  openViewView(): void {
    this.crudStateService.setView(CrudView.VIEW);
  }

  openEditView(): void {
    this.crudStateService.setView(CrudView.EDIT);
  }

}
