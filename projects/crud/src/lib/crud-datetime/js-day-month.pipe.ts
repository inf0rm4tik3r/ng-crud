import {Pipe, PipeTransform} from '@angular/core';
import {CurrentUser} from '../user/current-user.service';

@Pipe({
  name: 'JsDayMonth',
  pure: true
})
export class JsDayMonthPipe implements PipeTransform {

  constructor(private currentUser: CurrentUser) {
  }

  transform(date: Date, args?: any): string {
    if (!date) {
      return '';
    }
    return date.toLocaleDateString(
      this.currentUser.locale,
      {
        day: '2-digit',
        month: 'short'
      }
    );
  }

}
