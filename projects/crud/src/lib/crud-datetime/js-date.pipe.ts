import {Pipe, PipeTransform} from '@angular/core';
import {CrudUser} from '../crud-user/crud-user';

@Pipe({
  name: 'JsDate',
  pure: true
})
export class JsDatePipe implements PipeTransform {

  constructor(private crudUser: CrudUser) {
  }

  transform(date: Date, args?: any): string {
    if (!date) {
      return '';
    }
    return date.toLocaleDateString(
      this.crudUser.locale,
      {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
      }
    );
  }

}
