import { TestBed } from '@angular/core/testing';

import { CrudIdService } from './crud-id.service';

describe('IdService', () => {
  let service: CrudIdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudIdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
