import {Injectable} from '@angular/core';
import {CrudAlert} from './crud-alert/crud-alert';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class CrudAlertService {

  private readonly _alerts: BehaviorSubject<CrudAlert[]> = new BehaviorSubject<CrudAlert[]>([]);
  public readonly alerts$: Observable<CrudAlert[]> = this._alerts.asObservable();

  pushAlert(alert: CrudAlert): void {
    this._alerts.getValue().push(alert);
    this._alerts.next(this._alerts.getValue());
    if (alert.automaticallyClosing) {
      setTimeout(() => {
        this.removeAlert(alert);
      }, alert.automaticCloseDelay);
    }
  }

  removeAlert(alert: CrudAlert): void {
    this._alerts.getValue().forEach((current: CrudAlert, index: number) => {
      if (current === alert) {
        this._alerts.getValue().splice(index, 1);
        this._alerts.next(this._alerts.getValue());
        return;
      }
    });
  }

}
