import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[crudComponentContainer]'
})
export class CrudContainerDirective {

  constructor(public viewContainerRef: ViewContainerRef) {
  }

}
