import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudBtnWrapperComponent } from './crud-btn-wrapper.component';

describe('CrudBtnWrapperComponent', () => {
  let component: CrudBtnWrapperComponent;
  let fixture: ComponentFixture<CrudBtnWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudBtnWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudBtnWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
