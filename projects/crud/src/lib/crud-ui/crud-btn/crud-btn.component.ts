import {ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, Output} from '@angular/core';
import {IconProp} from '@fortawesome/fontawesome-svg-core';

@Component({
  selector: 'crud-btn',
  templateUrl: './crud-btn.component.html',
  styleUrls: ['./crud-btn.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudBtnComponent {

  @Input()
  type: 'default' | 'primary' | 'secondary' | 'info' | 'success' | 'warn' | 'danger' = 'primary';

  @Input()
  small: boolean = false;

  @Input()
  disabled: boolean = false;

  @Input()
  active: boolean = false;

  @Input()
  icon: IconProp | undefined = undefined;

  @Output()
  action: EventEmitter<void> = new EventEmitter<void>();

  @HostBinding('tabindex')
  tabindex = 0;

  @HostBinding('class.type-default')
  get isTypeDefault(): boolean {
    return this.type === 'default';
  }

  @HostBinding('class.type-primary')
  get isTypePrimary(): boolean {
    return this.type === 'primary';
  }

  @HostBinding('class.type-secondary')
  get isTypeSecondary(): boolean {
    return this.type === 'secondary';
  }

  @HostBinding('class.type-info')
  get isTypeInfo(): boolean {
    return this.type === 'info';
  }

  @HostBinding('class.type-success')
  get isTypeSuccess(): boolean {
    return this.type === 'success';
  }

  @HostBinding('class.type-warn')
  get isTypeWarn(): boolean {
    return this.type === 'warn';
  }

  @HostBinding('class.type-danger')
  get isTypeDanger(): boolean {
    return this.type === 'danger';
  }

  @HostBinding('class.small')
  get isSmall(): boolean {
    return this.small;
  }

  @HostBinding('class.disabled')
  get isDisabled(): boolean {
    return this.disabled;
  }

  @HostBinding('class.active')
  get isActive(): boolean {
    return this.active;
  }

  @HostListener('click')
  onKlick(): void {
    this.action.emit();
  }

  @HostListener('keydown.enter.stop')
  onEnter(): void {
    this.action.emit();
  }

}
