import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudBtnGroupComponent } from './crud-btn-group.component';

describe('CrudBtnGroupComponent', () => {
  let component: CrudBtnGroupComponent;
  let fixture: ComponentFixture<CrudBtnGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudBtnGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudBtnGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
