import { TestBed } from '@angular/core/testing';

import { CrudRouterService } from './crud-router.service';

describe('CrudRouterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudRouterService = TestBed.get(CrudRouterService);
    expect(service).toBeTruthy();
  });
});
