import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../../crud-component';
import {CrudLoggingService} from '../../crud-logging.service';
import {ItemToHeaderAndBodyCommunicationService} from '../services/item-to-header-and-body-communication.service';
import {ListToItemCommunicationService} from '../services/list-to-item-communication.service';

@Component({
  selector: 'crud-collapsible-item',
  templateUrl: './crud-collapsible-item.component.html',
  styleUrls: ['./crud-collapsible-item.component.scss'],
  providers: [ItemToHeaderAndBodyCommunicationService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudCollapsibleItemComponent extends CrudComponent implements OnInit {

  @Input()
  expanded: boolean = false;

  @Input()
  collapsible: boolean = true;

  @Output()
  stateChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private listToItemCom: ListToItemCommunicationService,
              private itemToHeaderAndBodyCom: ItemToHeaderAndBodyCommunicationService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    cdr.detach();
  }

  ngOnInit(): void {
    this.listToItemCom.addItem(this);

    this.listToItemCom.onLevelShouldBePropagated$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(parentIsOdd => this.propagateLevelAdjustment(parentIsOdd));

    // Propagates the current expanded state (after initialization).
    this.itemToHeaderAndBodyCom.itemStateChanged(this);

    this.itemToHeaderAndBodyCom.headerClick$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => this.toggle());
  }

  toggle(): void {
    if (this.collapsible) {
      this.setExpanded(!this.expanded);
    }
  }

  setExpanded(newState: boolean, shouldNotifyParent: boolean = true): void {
    if (this.collapsible) {
      this.expanded = newState;
      this.cdr.detectChanges();

      this.itemToHeaderAndBodyCom.itemStateChanged(this);
      if (shouldNotifyParent) {
        this.listToItemCom.itemStateChange(this);
      }
      this.stateChange.emit(newState);
    }
  }

  propagateLevelAdjustment(parentIsOdd: boolean): void {
    this.itemToHeaderAndBodyCom.propagateLevelAdjustment(parentIsOdd);
  }

  getComponentName(): string {
    return 'collapsible-item';
  }

}
