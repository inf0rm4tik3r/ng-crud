import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ListToItemCommunicationService} from '../services/list-to-item-communication.service';

import {CrudCollapsibleItemComponent} from './crud-collapsible-item.component';

describe('CollapsibleItemComponent', () => {
  let cut: CrudCollapsibleItemComponent;
  let fixture: ComponentFixture<CrudCollapsibleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CrudCollapsibleItemComponent],
      providers: [ListToItemCommunicationService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudCollapsibleItemComponent);
    cut = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(cut).toBeTruthy();
  });

  it('should switch expanded state on toggle: false => true', () => {
    spyOn(cut, 'setExpanded').and.stub();
    cut.expanded = false;
    cut.toggle();
    expect(cut.setExpanded).toHaveBeenCalledWith(true);
  });

  it('should switch expanded state on toggle: true => false', () => {
    spyOn(cut, 'setExpanded').and.stub();
    cut.expanded = true;
    cut.toggle();
    expect(cut.setExpanded).toHaveBeenCalledWith(false);
  });

  it('should set expanded state ', () => {
    spyOn(cut, 'setExpanded').and.stub();
    cut.expanded = false;
    cut.toggle();
    expect(cut.setExpanded).toHaveBeenCalledWith(true);
  });

  it('should emit event on change of the expanded state', () => {
    spyOn(cut.stateChange, 'emit').and.stub();
    cut.setExpanded(true);
    expect(cut.stateChange.emit).toHaveBeenCalledWith(true);
  });

});
