import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ItemToHeaderAndBodyCommunicationService} from '../services/item-to-header-and-body-communication.service';

import {CrudCollapsibleHeaderComponent} from './crud-collapsible-header.component';

describe('CollapsibleHeaderComponent', () => {
  let cut: CrudCollapsibleHeaderComponent;
  let fixture: ComponentFixture<CrudCollapsibleHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CrudCollapsibleHeaderComponent],
      providers: [ItemToHeaderAndBodyCommunicationService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudCollapsibleHeaderComponent);
    cut = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(cut).toBeTruthy();
  });

});
