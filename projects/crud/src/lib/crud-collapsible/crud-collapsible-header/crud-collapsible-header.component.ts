import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, Input} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../../crud-component';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudCollapsibleItemComponent} from '../crud-collapsible-item/crud-collapsible-item.component';
import {ItemToHeaderAndBodyCommunicationService} from '../services/item-to-header-and-body-communication.service';

@Component({
  selector: 'crud-collapsible-header',
  templateUrl: './crud-collapsible-header.component.html',
  styleUrls: ['./crud-collapsible-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudCollapsibleHeaderComponent extends CrudComponent {

  @Input()
  title: string = undefined;

  @Input()
  showHTML: string = undefined;

  itemIsExpanded = false;

  constructor(private itemToHeaderAndBodyCom: ItemToHeaderAndBodyCommunicationService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    cdr.detach();

    this.itemToHeaderAndBodyCom.itemStateChange$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((item: CrudCollapsibleItemComponent) => {
        this.itemIsExpanded = item.expanded;
        cdr.detectChanges();
      });
  }

  @HostListener('click')
  click(): void {
    this.itemToHeaderAndBodyCom.headerClicked(this);
  }

  getComponentName(): string {
    return 'collapsible-header';
  }

}
