import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {CrudCollapsibleItemComponent} from '../crud-collapsible-item/crud-collapsible-item.component';

@Injectable()
export class ListToItemCommunicationService {

  private _items: CrudCollapsibleItemComponent[] = [];

  // Observable sources
  private itemAddSource = new Subject<CrudCollapsibleItemComponent>();
  private itemStateChangeSource = new Subject<CrudCollapsibleItemComponent>();
  private levelPropagationSource = new Subject<boolean>();

  // Observable streams
  onItemAdd$: Observable<CrudCollapsibleItemComponent>  = this.itemAddSource.asObservable();
  onItemStateChange$: Observable<CrudCollapsibleItemComponent>  = this.itemStateChangeSource.asObservable();
  onLevelShouldBePropagated$: Observable<boolean> = this.levelPropagationSource.asObservable();

  // Service message commands
  addItem(item: CrudCollapsibleItemComponent): void {
    this._items.push(item);
    this.itemAddSource.next(item);
  }

  itemStateChange(item: CrudCollapsibleItemComponent): void {
    this.itemStateChangeSource.next(item);
  }

  propagateLevel(parentIsOdd: boolean): void {
    this.levelPropagationSource.next(parentIsOdd);
  }

  get items(): CrudCollapsibleItemComponent[] {
    return this._items;
  }

}
