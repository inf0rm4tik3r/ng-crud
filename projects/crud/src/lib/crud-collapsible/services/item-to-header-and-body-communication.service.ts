import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {CrudCollapsibleHeaderComponent} from '../crud-collapsible-header/crud-collapsible-header.component';
import {CrudCollapsibleItemComponent} from '../crud-collapsible-item/crud-collapsible-item.component';

@Injectable()
export class ItemToHeaderAndBodyCommunicationService {

  private _headerClick = new Subject<CrudCollapsibleHeaderComponent>();
  private _itemState = new Subject<CrudCollapsibleItemComponent>();
  private _levelAdjustment = new Subject<boolean>();

  headerClick$: Observable<CrudCollapsibleHeaderComponent> = this._headerClick.asObservable();
  itemStateChange$: Observable<CrudCollapsibleItemComponent> = this._itemState.asObservable();
  levelShouldBeAdjusted$: Observable<boolean> = this._levelAdjustment.asObservable();

  headerClicked(header: CrudCollapsibleHeaderComponent): void {
    this._headerClick.next(header);
  }

  itemStateChanged(item: CrudCollapsibleItemComponent): void {
    this._itemState.next(item);
  }

  propagateLevelAdjustment(parentIsOdd: boolean): void {
    this._levelAdjustment.next(parentIsOdd);
  }

}
