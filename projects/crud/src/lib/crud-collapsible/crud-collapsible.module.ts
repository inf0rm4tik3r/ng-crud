import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CrudCollapsibleBodyComponent} from './crud-collapsible-body/crud-collapsible-body.component';
import {CrudCollapsibleHeaderComponent} from './crud-collapsible-header/crud-collapsible-header.component';
import {CrudCollapsibleItemComponent} from './crud-collapsible-item/crud-collapsible-item.component';
import {CrudCollapsibleListComponent} from './crud-collapsible-list/crud-collapsible-list.component';
import {CrudCollapsibleAnimationsService} from './services/collapsible-animation.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CrudCollapsibleListComponent,
    CrudCollapsibleItemComponent,
    CrudCollapsibleHeaderComponent,
    CrudCollapsibleBodyComponent
  ],
  exports: [
    CrudCollapsibleListComponent,
    CrudCollapsibleItemComponent,
    CrudCollapsibleHeaderComponent,
    CrudCollapsibleBodyComponent
  ],
  providers: [
    CrudCollapsibleAnimationsService
  ]
})
export class CrudCollapsibleModule {
}
