import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostBinding, Injector, Input} from '@angular/core';
import {CrudComponent} from '../../crud-component';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudCollapsibleItemComponent} from '../crud-collapsible-item/crud-collapsible-item.component';
import {LevelAdjustmentService} from '../services/level-adjustment.service';
import {ListToItemCommunicationService} from '../services/list-to-item-communication.service';

@Component({
  selector: 'crud-collapsible-list',
  templateUrl: './crud-collapsible-list.component.html',
  styleUrls: ['./crud-collapsible-list.component.scss'],
  providers: [ListToItemCommunicationService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudCollapsibleListComponent extends CrudComponent {

  public static readonly TYPE_EXPANDABLE = 'expandable';
  public static readonly TYPE_ACCORDION = 'accordion';
  public static readonly FALLBACK_TYPE = CrudCollapsibleListComponent.TYPE_ACCORDION;

  @Input()
  type: 'expandable' | 'accordion' = CrudCollapsibleListComponent.FALLBACK_TYPE;

  @HostBinding('class.odd-level')
  oddLevel = true;

  @HostBinding('class.even-level')
  evenLevel = false;

  constructor(private listToItemCom: ListToItemCommunicationService,
              // The injector is used, as the LevelAdjustmentService might not be present.
              private injector: Injector,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    cdr.detach();

    this.checkType();

    // If the parent component provides a LevelAdjustmentService instance, this list should listen to push events.
    try {
      injector.get(LevelAdjustmentService).onLevelShouldBeAdjusted$.subscribe(parentIsOdd => this.propagateLevel(parentIsOdd));
    } catch (exception) {
      // This list component does not have a parent which provides a LevelAdjustmentService instance. Nothing to do here..
    }

    // Whenever a new item is added to this list (through asynchronous operations for example), trigger a propagation cycle.
    this.listToItemCom.onItemAdd$.subscribe(() => this.propagateLevel(this.oddLevel));

    // If an item changes its state, we might want to collapse all its siblings if the item got opened and this is an accordion.
    this.listToItemCom.onItemStateChange$.subscribe((itemComponentWithChangedState: CrudCollapsibleItemComponent) => {
      if (itemComponentWithChangedState.expanded && this.type === CrudCollapsibleListComponent.TYPE_ACCORDION) {
        this.collapseAllSiblingsOf(itemComponentWithChangedState);
      }
    });
  }

  private checkType(): void {
    if (this.type !== CrudCollapsibleListComponent.TYPE_EXPANDABLE
      && this.type !== CrudCollapsibleListComponent.TYPE_ACCORDION) {
      this.logger.error('Unknown type specified: "' + this.type + '".');
      this.logger.info('Falling back to type: "' + CrudCollapsibleListComponent.FALLBACK_TYPE + '".');
      this.type = CrudCollapsibleListComponent.FALLBACK_TYPE;
      this.cdr.detectChanges();
    }
  }

  private collapseAllSiblingsOf(targetItem: CrudCollapsibleItemComponent): void {
    this.listToItemCom
      .items
      .filter((item) => item !== targetItem && item.expanded)
      .forEach((item) => item.setExpanded(false, false /* important! */));
  }

  private propagateLevel(parentIsOdd: boolean): void {
    this.oddLevel = !parentIsOdd;
    this.evenLevel = parentIsOdd;
    this.cdr.detectChanges();
    this.listToItemCom.propagateLevel(this.oddLevel);
  }

  getComponentName(): string {
    return 'collapsible-list';
  }

}
