import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudCollapsibleListComponent} from './crud-collapsible-list.component';

describe('CollapsibleListComponent', () => {
  let component: CrudCollapsibleListComponent;
  let fixture: ComponentFixture<CrudCollapsibleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudCollapsibleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudCollapsibleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
