import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostBinding} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {CrudComponent} from '../../crud-component';
import {CrudLoggingService} from '../../crud-logging.service';
import {CrudCollapsibleItemComponent} from '../crud-collapsible-item/crud-collapsible-item.component';

import {CrudCollapsibleAnimationsService} from '../services/collapsible-animation.service';
import {ItemToHeaderAndBodyCommunicationService} from '../services/item-to-header-and-body-communication.service';
import {LevelAdjustmentService} from '../services/level-adjustment.service';

@Component({
  selector: 'crud-collapsible-body',
  templateUrl: './crud-collapsible-body.component.html',
  styleUrls: ['./crud-collapsible-body.component.scss'],
  animations: CrudCollapsibleAnimationsService.collapsibleBodyAnimations('collapsibleBodyState'),
  providers: [LevelAdjustmentService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudCollapsibleBodyComponent extends CrudComponent {

  @HostBinding('@collapsibleBodyState')
  expandedState: string;

  constructor(private itemToHeaderAndBodyCom: ItemToHeaderAndBodyCommunicationService,
              private levelAdjustmentService: LevelAdjustmentService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    cdr.detach();

    this.itemToHeaderAndBodyCom.itemStateChange$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((item: CrudCollapsibleItemComponent) => {
      this.expandedState = item.expanded.toString();
      this.cdr.detectChanges();
    });

    this.itemToHeaderAndBodyCom.levelShouldBeAdjusted$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((parentIsOdd: boolean) => {
      this.levelAdjustmentService.adjustLevels(parentIsOdd);
    });
  }

  getComponentName(): string {
    return 'collapsible-body';
  }

}
