import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Observable, of} from 'rxjs';
import {CrudCollapsibleItemComponent} from '../crud-collapsible-item/crud-collapsible-item.component';
import {ItemToHeaderAndBodyCommunicationService} from '../services/item-to-header-and-body-communication.service';
import {LevelAdjustmentService} from '../services/level-adjustment.service';

import {CrudCollapsibleBodyComponent} from './crud-collapsible-body.component';

class MockCollapsibleItemComponent {
  expanded = false;
}

const itemMock = new MockCollapsibleItemComponent();

class MockItemToHeaderAndBodyCommunicationService {
  onItemStateChange$: Observable<CrudCollapsibleItemComponent> = of(itemMock);
  onLevelShouldBeAdjusted$: Observable<boolean> = of(false);
}

const itemToHeaderAndBodyComMock = new MockItemToHeaderAndBodyCommunicationService();

describe('CollapsibleBodyComponent', () => {
  let cut: CrudCollapsibleBodyComponent;
  let fixture: ComponentFixture<CrudCollapsibleBodyComponent>;
  let itemToHeaderAndBodyCom: ItemToHeaderAndBodyCommunicationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule],
      declarations: [CrudCollapsibleBodyComponent],
      providers: [
        {provide: ItemToHeaderAndBodyCommunicationService, useValue: itemToHeaderAndBodyComMock},
        LevelAdjustmentService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    spyOn(itemToHeaderAndBodyComMock.onItemStateChange$, 'subscribe').and.callThrough();
    fixture = TestBed.createComponent(CrudCollapsibleBodyComponent);
    cut = fixture.componentInstance;
    itemToHeaderAndBodyCom = TestBed.inject(ItemToHeaderAndBodyCommunicationService);
    console.log(itemToHeaderAndBodyCom);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(cut).toBeTruthy();
  });

});
