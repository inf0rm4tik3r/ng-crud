import {CrudCollapsibleModule} from './collapsible.module';

describe('CollapsibleModule', () => {
  let collapsibleModule: CrudCollapsibleModule;

  beforeEach(() => {
    collapsibleModule = new CrudCollapsibleModule();
  });

  it('should create an instance', () => {
    expect(collapsibleModule).toBeTruthy();
  });
});
