import { TestBed } from '@angular/core/testing';

import { CrudEventService } from './crud-event.service';

describe('CrudEventService', () => {
  let service: CrudEventService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudEventService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
