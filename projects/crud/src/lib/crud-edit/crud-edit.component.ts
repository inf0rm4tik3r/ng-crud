import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {first, switchMap, takeUntil, tap} from 'rxjs/operators';
import {CrudAlertService} from '../crud-alert.service';
import {CrudAlert} from '../crud-alert/crud-alert';
import {CrudComponent} from '../crud-component';
import {CrudDataProviderService} from '../crud-data-provider/crud-data-provider.service';
import {CrudDeleteModalComponent} from '../crud-delete-modal/crud-delete-modal.component';
import {CrudEventService} from '../crud-event.service';
import {CrudFieldsComponent} from '../crud-fields/crud-fields.component';
import {CrudLoggingService} from '../crud-logging.service';
import {CrudModalService} from '../crud-modal/crud-modal.service';
import {CrudModifiedModalComponent} from '../crud-modified-modal/crud-modified-modal.component';
import {CrudRouterService} from '../crud-router.service';
import {CrudStateService} from '../crud-state.service';
import {Entry, EntryKey, EntryUpdate} from '../types/crud-types';

@Component({
  selector: 'crud-edit',
  templateUrl: './crud-edit.component.html',
  styleUrls: ['./crud-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudEditComponent extends CrudComponent implements OnInit {

  // TODO: remove, place logic into fields.
  @ViewChild(CrudFieldsComponent)
  fields: CrudFieldsComponent;

  triggerReload$: Subject<EntryKey> = new Subject<EntryKey>();

  loading: boolean = true;
  entry: Entry | undefined = undefined;

  ongoingSave: boolean = false;
  unsavedChangesWarningInitialized: boolean = false;

  constructor(private crudEventService: CrudEventService,
              private crudDataProviderService: CrudDataProviderService,
              private crudRouterService: CrudRouterService,
              private crudStateService: CrudStateService,
              private crudAlertService: CrudAlertService,
              private crudModalService: CrudModalService,
              protected cdr: ChangeDetectorRef,
              protected logger: CrudLoggingService) {
    super(cdr, logger);
    cdr.detach();
  }

  ngOnInit(): void {
    this.triggerReload$
      .pipe(
        takeUntil(this.unsubscribe$),
        tap((entryKey: EntryKey) => {
          this.logger.trace('loading entry', entryKey);
          this.loading = true;
          this.cdr.detectChanges();
        }),
        switchMap((entryKey: EntryKey) =>
          this.crudDataProviderService.getOne(entryKey)
        )
      ).subscribe(
      (entry: Entry | undefined) => {
        this.logger.trace('Loaded entry', entry);
        this.entry = entry;
        this.loading = false;
        this.cdr.detectChanges();
      },
      () => {
        this.loading = false;
        this.cdr.detectChanges();
      },
      () => {
        this.loading = false;
        this.cdr.detectChanges();
      }
    );

    this.crudStateService.activeEntryKey$
      .pipe(
        takeUntil(this.unsubscribe$),
        tap((activeEntryKey: EntryKey | undefined) => {
          if (activeEntryKey) {
            this.triggerReload$.next(activeEntryKey);
          } else {
            this.crudRouterService.openListView();
          }
        })
      )
      .subscribe();

    // Handle keydown events if the instance is active.
    this.crudEventService.keydownInActiveInstance$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((event: KeyboardEvent) => this.handleKeyDown(event));
  }

  private handleKeyDown(event: KeyboardEvent): void {
    // "Back".
    if (event.key === 'l' && !this.crudModalService.hasModals() && !this.fields.isAnyFieldFocused()) {
      this.toListView();
    }

    // "Save": Whether or not an input has focus is irrelevant when saving the entry.
    if (event.key === 's' && event.ctrlKey && !this.crudModalService.hasModals()) {
      this.save();
      event.preventDefault();
    }
  }

  private _save(then?: (() => void) | undefined): void {
    // TODO: Just update field of the original entry with data from the field inputs.
    // TODO: fields.getUpdate() to just get modified data.
    const update: EntryUpdate = this.fields.getInput();
    this.logger.trace('Saving update', update);
    this.ongoingSave = true;
    this.cdr.detectChanges();

    const key = this.crudDataProviderService.getKey(this.entry);

    this.crudDataProviderService.updateOne(key, update)
      .pipe(first())
      .subscribe(() => {
        this.fields.unsavedChanges = false;
        this.ongoingSave = false;
        this.cdr.detectChanges();
        this.triggerReload$.next(key);
        if (then) {
          then.apply(this, []);
        }
      });
  }

  private issueSuccessfullySavedAlert(): void {
    this.crudAlertService.pushAlert(new CrudAlert('success', 'Saved', 'The entry was saved!'));
  }

  save(): void {
    this._save(() => {
      this.issueSuccessfullySavedAlert();
    });
  }

  saveAndReturn(): void {
    this._save(() => {
      this.crudStateService.unsetActiveEntry();
      this.crudRouterService.openListView();
      this.issueSuccessfullySavedAlert();
    });
  }

  saveAndNew(): void {
    this._save(() => {
      this.crudStateService.unsetActiveEntry();
      this.crudRouterService.openCreateView();
      this.issueSuccessfullySavedAlert();
    });
  }

  onDeleteClick(): void {
    this.crudModalService.init(
      CrudDeleteModalComponent,
      {
        key: this.crudDataProviderService.getKey(this.entry)
      },
      {
        cancelClicked: () => {
          this.crudModalService.destroy();
        },
        deleteClicked: () => {
          this.crudDataProviderService.delete(this.crudDataProviderService.getKey(this.entry))
            .pipe(first())
            .subscribe(() => {
              this.crudStateService.unsetActiveEntry();
              this.crudRouterService.openListView();
              this.crudModalService.destroy();
            });
        }
      });
  }

  toListView(): void {
    if (!!this.entry && this.fields.unsavedChanges) {
      if (!this.unsavedChangesWarningInitialized) {
        this.unsavedChangesWarningInitialized = true;
        this.crudModalService.init(CrudModifiedModalComponent, {}, {
          cancelClicked: () => {
            this.crudModalService.destroy();
            this.unsavedChangesWarningInitialized = false;
          },
          leaveClicked: () => {
            this.crudStateService.unsetActiveEntry();
            this.crudRouterService.openListView();
            this.crudModalService.destroy();
            this.unsavedChangesWarningInitialized = false;
          }
        });
      }
    } else {
      this.crudStateService.unsetActiveEntry();
      this.crudRouterService.openListView();
    }
  }

  getComponentName(): string {
    return 'edit';
  }

}
