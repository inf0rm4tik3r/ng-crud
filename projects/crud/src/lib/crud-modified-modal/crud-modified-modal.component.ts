import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'crud-modified-modal',
  templateUrl: './crud-modified-modal.component.html',
  styleUrls: ['./crud-modified-modal.component.scss']
})
export class CrudModifiedModalComponent {

  @Output()
  cancelClicked: EventEmitter<void> = new EventEmitter();

  @Output()
  leaveClicked: EventEmitter<void> = new EventEmitter();

}
