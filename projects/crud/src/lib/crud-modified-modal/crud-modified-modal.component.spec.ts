import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrudModifiedModalComponent} from './crud-modified-modal.component';

describe('CrudModifiedModalComponent', () => {
  let component: CrudModifiedModalComponent;
  let fixture: ComponentFixture<CrudModifiedModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudModifiedModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudModifiedModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
