import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CrudLicensingService {

  private _licensed: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  licensed$: Observable<boolean> = this._licensed.asObservable();

  // TODO: Architecture and implementation.
  /*
  Das CRUD Framework wird mit einem Lizenzschlüssel deployed.
  Das Framework schickt den Schlüssel an einen Lizenzserver (max 1 Mal pro Tag).
  Der Lizenzserver überprüft, ob der Lizenzschlüssel auf die Absenderdomain registriert ist und gültig ist.
  Und antwortet mit Gültigkeitsinformation (gültig: boolean, gültigBis: datetime(utc)).
  Das Frontend speichert die CRUD-Lizenz-Gültigkeitsinformation im Local-Store des Browsers.
  Der CrudLicenseService übernimmt diese Aufgabe.
  Der Lizenzserver wird nicht angesprochen, wenn die Umgebung "localhost" ist.
  */

}
