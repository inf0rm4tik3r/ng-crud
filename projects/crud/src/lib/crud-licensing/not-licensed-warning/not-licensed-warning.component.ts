import {Component} from '@angular/core';
import {CrudTranslatorService} from '../../crud-localization/crud-translator.service';

@Component({
  selector: 'crud-not-licensed-warning',
  templateUrl: './not-licensed-warning.component.html',
  styleUrls: ['./not-licensed-warning.component.scss']
})
export class NotLicensedWarningComponent {

  constructor(private crudTranslatorService: CrudTranslatorService) {
  }

  __(key: string): string {
    return this.crudTranslatorService.translate(key);
  }

}
