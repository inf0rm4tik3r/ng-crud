import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NotLicensedWarningComponent} from './not-licensed-warning.component';

describe('NotLicensedWarningComponent', () => {
  let component: NotLicensedWarningComponent;
  let fixture: ComponentFixture<NotLicensedWarningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotLicensedWarningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotLicensedWarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
