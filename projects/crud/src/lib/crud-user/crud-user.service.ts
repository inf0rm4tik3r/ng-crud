import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {CrudUser} from './crud-user';

@Injectable()
export class CrudUserService {

  private _currentUser: Subject<CrudUser> = new BehaviorSubject<CrudUser>(new CrudUser('en', 'male'));
  private currentUser$: Observable<CrudUser> = this._currentUser.asObservable();

  setCurrentUser(currentUser: CrudUser): void {
    this._currentUser.next(currentUser);
  }

}
