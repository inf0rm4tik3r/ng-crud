export class CrudUser {

  public static readonly DEFAULT_USER = new CrudUser('en', 'diverse');

  private _locale: string;
  private _gender: string;

  constructor(locale: string, gender: string) {
    this._locale = locale;
    this._gender = gender;
  }

  get locale(): string {
    return this._locale;
  }

  set locale(value: string) {
    this._locale = value;
  }

  get gender(): string {
    return this._gender;
  }

  set gender(value: string) {
    this._gender = value;
  }

}
