import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  EmbeddedViewRef,
  Injectable,
  Injector,
  Type,
  ViewContainerRef
} from '@angular/core';

export interface ComponentConfig {
  inputs: { [key: string]: unknown };
  outputs: { [key: string]: unknown };
}

@Injectable()
export class CrudInjectionService {

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private applicationRef: ApplicationRef,
              private injector: Injector) {
  }

  private static attachConfig(componentRef: ComponentRef<unknown>, config: ComponentConfig) {
    // Pass inputs by value.
    for (const key of Object.keys(config.inputs)) {
      /*
      // Currently does not work when extending another component.
      if (!componentRef.instance[key]) {
        throw new Error('Input "' + key + '" is not present on component "' + componentRef.componentType + '"');
      }
      */
      componentRef.instance[key] = config.inputs[key];
    }
    // Subscribe to outputs.
    for (const key of Object.keys(config.outputs)) {
      const output = componentRef.instance[key];
      if (!output) {
        throw new Error('Output "' + key + '" is not present on component "' + componentRef.componentType + '"');
      }
      output.subscribe(config.outputs[key]);
    }
  }

  private static htmlOfComponentRef(componentRef: ComponentRef<unknown>): HTMLElement {
    // Get DOM element from component.
    return (componentRef.hostView as EmbeddedViewRef<unknown>).rootNodes[0] as HTMLElement;
  }

  public appendComponentToHtmlElement(parent: HTMLElement,
                                      component: Type<unknown>,
                                      componentConfig?: ComponentConfig): ComponentRef<unknown> {
    const componentRef = this.createFreeComponent(component, componentConfig);
    const htmlElement = CrudInjectionService.htmlOfComponentRef(componentRef);
    parent.appendChild(htmlElement);
    return componentRef;
  }

  public appendComponentToViewContainer(viewContainerRef: ViewContainerRef,
                                        component: Type<unknown>,
                                        componentConfig?: ComponentConfig): ComponentRef<unknown> {
    return this.createViewContainerComponent(viewContainerRef, component, componentConfig);
  }

  private createFreeComponent(component: Type<unknown>, componentConfig?: ComponentConfig): ComponentRef<unknown> {
    // Create a component reference from the component
    const componentRef: ComponentRef<unknown> = this.componentFactoryResolver
      .resolveComponentFactory(component)
      .create(this.injector);

    // Attach the config to the child (inputs and outputs).
    CrudInjectionService.attachConfig(componentRef, componentConfig);

    // Attach component to the applicationRef so that it's inside the ng component tree.
    this.applicationRef.attachView(componentRef.hostView);

    return componentRef;
  }

  private createViewContainerComponent(viewContainerRef: ViewContainerRef,
                                       component: Type<unknown>,
                                       componentConfig?: ComponentConfig) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    const componentRef = viewContainerRef.createComponent(componentFactory);

    // Attach the config to the child (inputs and outputs).
    CrudInjectionService.attachConfig(componentRef, componentConfig);

    return componentRef;
  }

  public removeComponentFromApplicationRef(componentRef: ComponentRef<unknown>): void {
    this.applicationRef.detachView(componentRef.hostView);
    componentRef.destroy();
  }

}
