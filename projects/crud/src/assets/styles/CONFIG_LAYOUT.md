## Layout of the $config argument passed to producers.

````
colors: (
    primary_color_light: <color>,
    secondary_color_light: <color>,
    info_color_light: <color>,
    success_color_light: <color>,
    warning_color_light: <color>,
    danger_color_light: <color>
)
````
