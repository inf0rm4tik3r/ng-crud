import {Component} from '@angular/core';
import {
  CrudCondition,
  CrudConfig,
  CrudDataProvider,
  CrudLocalDataProvider,
  CrudTestData,
  CrudToast,
  CrudToastService,
  Entry,
  EntryKey,
  LogLevel
} from 'crud';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  lightTheme = 'light';
  darkTheme = 'dark';
  colorblindTheme = 'colorblind';
  theme = this.darkTheme;

  mainDataProvider: CrudDataProvider = new CrudLocalDataProvider(
    () => CrudTestData.testDataStructure,
    () => CrudTestData.testData,
    'main-data-provider',
    0
  ).setLogLevel(LogLevel.TRACE);

  relationDataProvider: CrudDataProvider = new CrudLocalDataProvider(
    () => CrudTestData.relationDataStructure,
    () => CrudTestData.relationData,
    'relation-data-provider',
    0
  ).setLogLevel(LogLevel.TRACE);

  nestedDataProvider: CrudDataProvider = new CrudLocalDataProvider(
    () => CrudTestData.nestedDataStructure,
    () => CrudTestData.nestedData,
    'nested-data-provider',
    0
  ).setLogLevel(LogLevel.TRACE);

  config: CrudConfig = this.buildConfig();
  relationConfig: CrudConfig = this.buildRelationConfig();

  constructor(private crudToastService: CrudToastService) {
  }

  buildConfig(): CrudConfig {
    const crud = new CrudConfig();
    crud.logLevel(LogLevel.TRACE);
    crud.dataProvider(this.mainDataProvider);
    // crud.fetchMode('prefetch');
    crud.fetchMode('lazy');
    crud.label('id', '#');
    crud.label('name', 'Name');
    crud.label('value', 'Value');
    crud.label('color', 'Color');
    crud.label('relation_id', 'Ref');

    // crud.align('value', 'right');
    // crud.transform('value', (value: number) => value.toFixed(2));

    crud.usePagination(true);
    crud.entriesPerPage(5);
    // TODO: Set entriesPerPage mode to 'buttons'(?). Set specific options.
    crud.allowOrderBy(['id', 'name', 'value', 'color', 'relation_id'], true);
    crud.orderBy('name', 'asc');
    crud.keyVisualizer((key: EntryKey | undefined) => {
      return key ? (key.id as string) : '';
    });
    crud.entryVisualizer((entry: Entry | undefined): string => {
      return entry ? (entry.name as string) : '';
    });
    crud.fieldType('id', 'int');
    // crud.fieldType('value', 'test');
    // crud.fieldRenderer('test', TestFieldComponent);
    crud.inlineEdit(['name', 'value', 'color', 'relation_id'], true);
    crud.filterMode('normal');
    crud.allowFilterDefault(true);
    crud.allowFilter('color', false);
    crud.userFilter(new CrudCondition().where('name', 'CONTAINS', 'b'));
    /*
      .where('name', 'CONTAINS', '', 'nameContains')
      .where('value', '<', 7, 'valueLessThen20');
     */
    crud.relation(
      'relation_id', // My field. Starting point of the relation.
      this.relationDataProvider, // Provider for the relation data.
      'id', // The field in the relation data which is used to connect entries to our entry.
      '{name}', // The fields of the relation data which are displayed on our side.
      new CrudCondition(),
      {name: 'asc'}
      // TODO: select or multiselect.
    );
    crud.fieldType('relation_id', 'relation');
    crud.fieldOptions('relation_id', ['foo', 'bar']);

    crud.nestedInstance(
      'comments', // New field name under which the data is shown.
      'id',
      'ref_id',
      new CrudConfig()
        .logLevel(LogLevel.TRACE)
        .collapsible(true)
        .dataProvider(this.nestedDataProvider)
        .keyVisualizer((key: EntryKey | undefined) => {
          return key ? (key.id as string) : '';
        })
        .entryVisualizer((entry: Entry | undefined): string => {
          return entry ? (entry.name as string) : '';
        })
    );

    return crud;
  }

  buildRelationConfig(): CrudConfig {
    const crud = new CrudConfig();
    crud.logLevel(LogLevel.TRACE);
    crud.dataProvider(this.relationDataProvider);
    // crud.fetchMode('prefetch');
    crud.fetchMode('lazy');
    crud.label('id', '#');
    crud.label('name', 'Name');
    crud.usePagination(true);
    crud.entriesPerPage(5);
    crud.allowOrderBy(['id', 'name'], true);
    crud.orderBy('id', 'asc');
    crud.keyVisualizer((key: EntryKey | undefined) => {
      return key ? (key.id as string) : '';
    });
    crud.entryVisualizer((entry: Entry | undefined): string => {
      return entry ? (entry.name as string) : '';
    });
    crud.inlineEdit(['name', 'value'], true);
    return crud;
  }

  createToast(): void {
    this.crudToastService.add(new CrudToast('success', 'Test', 'message', false));
  }

}
