# CRUD

This project contains the CRUD library and a demo application using it.

## Building the library.

Execute one of the following commands to build the library:

    ng build crud
    ng build crud --watch
    ng build --prod crud
    ng build --prod crud --watch

Use the "--prod" flag to build the "View Engine" version of the library.<br>
Without it, the build targets the new Ivy engine and will then be incompatible with applications not using Ivy.<br>
It is recommended to build with the --prod flag. Angular can compile a View engine library to Ivy, so no performance is
lost.

Use the "--watch" flag when actively developing the library.<br>
Changes in the source files will then lead to fast incremental compilation.<br>
A running test application will see the changes made to the library immediately.

## Starting the demo application.

Execute

    ng serve

to start the demo application that consumes the CRUD library.

The demo application can be used as a blueprint for new applications.

## Exporting the library.

...tbd

## Creating a new application.

...tbd
