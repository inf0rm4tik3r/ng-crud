module.exports = {
  root: true,
  env: {
    node: false
  },
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint'
  ],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  rules: {
    // We only want strings defined by single quotes in our codebase.
    'quotes': [
      2,
      'single', {
        'avoidEscape': true
      }
    ],
    // Unused variables and arguments are allowed if they are named "_" or "ignored".
    '@typescript-eslint/no-unused-vars': [
      1,
      {
        'varsIgnorePattern': '_|ignored',
        'argsIgnorePattern': '_|ignored'
      }
    ],
    // Specifying inferrable types, for example in property declaration/definition -> "name: string = 'foo';",
    // will not raise a warning.
    // We want to encourage developers to give explicit type information in class properties and parameters.
    '@typescript-eslint/no-inferrable-types': [
      1,
      {
        ignoreProperties: true,
        ignoreParameters: true
      }
    ]
  }
};
